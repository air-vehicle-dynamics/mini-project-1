"quadrotor_Linear_Autopilot" is an implmenentation of the linear quadrotor controller to the three trim conditions calculated for the quadrotor.

"quadrotor_Differential_Flatness" is an implmementation of the differential flatness algorithm to the Polynomial Trajectory and Dubins trajectory.

User must comment/uncomment lines to get the correct trajectory, gains, and initial conditions for each case.