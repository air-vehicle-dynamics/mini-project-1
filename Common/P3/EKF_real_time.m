function [x_hat, P_hat] = EKF_real_time(state_curr,state_prev,u_curr,u_prev,...
    measurement_curr,measurement_prev,wind,P_minus_prev,Q, R, dt)
%
% INPUT
%   f -> jacobian of state model f(x, u, 0)
%   h -> jacobian of measurement model h(x, u, 0)
%
% OUTPUT
%   x_hat -> estimated state
%   P_hat -> estimated covariance
%
% STATE
% x_hat = [Pt; Vt; psi; theta; phi; psi bias; theta bias; phi bias; accel bias;
% rate bias; airspeed bias]
%
% u = [ACCEL; GYRO; GRAV];


% Check to see if measurement has updated at all (1 if equal)
% measurement_update_check = isequal(measurement_prev, measurement_curr);

% if measurement_update_check == 1 % no new measurement
%     % update x and p to the best of the filter's ability
%     [x_hat, P_hat] = predictive_update(state_prev, state_curr, u_prev, u_curr, P_minus_prev, Q, dt);
% elseif measurement_update_check == 0 % new measurement
%     % update x and p to best of sensor's ability
%     [x_hat_minus, P_hat_minus] = predictive_update(state_prev, state_curr, u_prev, u_curr, P_minus_prev, Q, dt);
%     [x_hat, P_hat] = measurement_update(x_hat_minus, measurement_curr, u_curr, wind, P_hat_minus, R);
% else 
%     fprintf('ERROR: measurement_update_check');
% end
        
    

%% Predictive and Measurement Updates

    % grab data
    ACCEL_1 = u_prev(1:3); GYRO_1 = u_prev(4:6); g_1 = u_prev(7:9);
    ACCEL_2 = u_curr(1:3); GYRO_2 = u_curr(4:6); g_2 = u_curr(7:9);

    bias_accel_1 = state_prev(13:15); bias_gyro_1 = state_prev(16:18);
    bias_accel_2 = state_curr(13:15); bias_gyro_2 = state_curr(16:18);

    % predict the next step using rk4 integration
    u1	= [ACCEL_1 - bias_accel_1;
           GYRO_1  - bias_gyro_1;
           g_1];  

    u2 = [ACCEL_2 - bias_accel_2;
          GYRO_2  - bias_gyro_2;
           g_2];

    u12 = 0.5 * (u1 + u2);	    

    k1 = dt*f_xun(state_curr,           u1);
    k2 = dt*f_xun(state_curr + 0.5*k1, u12);
    k3 = dt*f_xun(state_curr + 0.5*k2, u12);
    k4 = dt*f_xun(state_curr + k3,      u2);

    x_hat_minus = state_curr + ((1/6)*k1 + (1/3)*k2 + (1/3)*k3 + (1/6)*k4);

    % recalculate covariance coefficient matrices
    [A,B2] = predict_coeff_comp(state_prev,u_prev,dt);

    % Update P matrix
    P_minus = A*P_minus_prev*A' + B2*Q*B2';
    
    % measurement model
    C = measure_coeff_comp(x_hat_minus,u_curr,wind);
    
    % Kalman gain
    Kn = P_minus*C' / (C*P_minus*C' + R);
    
    % update predictive estimate with new measurement
    zm = measurement_curr;
    
    rho = atmosphere(-x_hat_minus(3));
    
    x_hat = x_hat_minus + Kn * (zm - h_xn(x_hat_minus, rho, wind));
    
    P_hat = (eye(19) - Kn * C) * P_minus;


%% Helper Functions

% get state derivative from state and predictive measurements
function xdot = f_xun(x, u)
        ps = x(7);
        th = x(8);
        ph = x(9);
        
        Hinv_fu = (1 / cos(th) ) * [0 sin(ph) cos(ph); ...
		0 cos(ph)*cos(th) -sin(ph)*cos(th); ...
		cos(th) sin(ph)*sin(th) cos(ph)*sin(th)];
        
        R_phi = [1 0 0; 0 cos(ph) sin(ph); 0 -sin(ph) cos(ph)];
        R_th  = [cos(th) 0 -sin(th); 0 1 0; sin(th) 0 cos(th)];
        R_psi = [cos(ps) sin(ps) 0; -sin(ps) cos(ps) 0; 0 0 1];

        R_tb = R_phi * R_th * R_psi;
        R_bt_fu = transpose(R_tb);
    
        xdot = [x(4:6); R_bt_fu*u(1:3) + u(7:9); Hinv_fu*u(4:6); zeros(10,1)];
end

% get estimated states from measurement
function measurement = h_xn(x, rho, wind)
    
    Vx = x(4);
    Vy = x(5);
    Vz = x(6);

    Deltap = (rho/2)*((Vx - wind(1))^2 + (Vy - wind(2))^2 + (Vz - wind(3))^2);

    measurement = [x(1:9) + [zeros(6,1); x(10:12)]; Deltap + x(19)];
end

% get A and B2 from state, predictive measurements
function [A,B2] = predict_coeff_comp(x,u,dt)
        % see symbolic_calc.m for matlab derivation of this linearized
        % model
        
        % state
        Px = x(1);
        Py = x(2);
        Pz = x(3);
        Vx = x(4);
        Vy = x(5);
        Vz = x(6);
        ps = x(7);
        th = x(8);
        ph = x(9);
        bps = x(10);
        bth = x(11);
        bph = x(12);
        bax = x(13);
        bay = x(14);
        baz = x(15);
        bwx = x(16);
        bwy = x(17);
        bwz = x(18);
        bair = x(19);
        
        % predictive measurements
        abx = u(1);
        aby = u(2);
        abz = u(3);
        wbx = u(4);
        wby = u(5);
        wbz = u(6);
        gx  = u(7);
        gy  = u(8);
        gz  = u(9);
        
        % noise
        nax = 0;
        nay = 0;
        naz = 0;
        nwx = 0;
        nwy = 0;
        nwz = 0;
        nair = 0;
        
        A = eye(19) + dt *...         
        [0, 0, 0, 1, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 1, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 1,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0, (cos(ph)*cos(ps) + sin(ph)*sin(ps)*sin(th))*(bay - aby + nay) - (cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th))*(baz - abz + naz) + cos(th)*sin(ps)*(bax - abx + nax), cos(ps)*sin(th)*(bax - abx + nax) - cos(ph)*cos(ps)*cos(th)*(baz - abz + naz) - cos(ps)*cos(th)*sin(ph)*(bay - aby + nay), - (sin(ph)*sin(ps) + cos(ph)*cos(ps)*sin(th))*(bay - aby + nay) - (cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th))*(baz - abz + naz), 0, 0, 0, -cos(ps)*cos(th),   cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th), - sin(ph)*sin(ps) - cos(ph)*cos(ps)*sin(th),  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0, (cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th))*(bay - aby + nay) - (sin(ph)*sin(ps) + cos(ph)*cos(ps)*sin(th))*(baz - abz + naz) - cos(ps)*cos(th)*(bax - abx + nax), sin(ps)*sin(th)*(bax - abx + nax) - cos(ph)*cos(th)*sin(ps)*(baz - abz + naz) - cos(th)*sin(ph)*sin(ps)*(bay - aby + nay),   (cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th))*(bay - aby + nay) + (cos(ph)*cos(ps) + sin(ph)*sin(ps)*sin(th))*(baz - abz + naz), 0, 0, 0, -cos(th)*sin(ps), - cos(ph)*cos(ps) - sin(ph)*sin(ps)*sin(th),   cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th),  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                         cos(th)*(bax - abx + nax) + cos(ph)*sin(th)*(baz - abz + naz) + sin(ph)*sin(th)*(bay - aby + nay),                                                           cos(th)*sin(ph)*(baz - abz + naz) - cos(ph)*cos(th)*(bay - aby + nay), 0, 0, 0,          sin(th),                            -cos(th)*sin(ph),                            -cos(ph)*cos(th),  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                           - (cos(ph)*sin(th)*(bwz + nwz - wbz))/cos(th)^2 - (sin(ph)*sin(th)*(bwy + nwy - wby))/cos(th)^2,                                                       (sin(ph)*(bwz + nwz - wbz))/cos(th) - (cos(ph)*(bwy + nwy - wby))/cos(th), 0, 0, 0,                0,                                           0,                                           0,  0,           -sin(ph)/cos(th),           -cos(ph)/cos(th), 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                           cos(ph)*(bwz + nwz - wbz) + sin(ph)*(bwy + nwy - wby), 0, 0, 0,                0,                                           0,                                           0,  0,                   -cos(ph),                    sin(ph), 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                            -(bwz*cos(ph) + nwz*cos(ph) + bwy*sin(ph) - wbz*cos(ph) + nwy*sin(ph) - wby*sin(ph))/cos(th)^2,                                       (sin(ph)*sin(th)*(bwz + nwz - wbz))/cos(th) - (cos(ph)*sin(th)*(bwy + nwy - wby))/cos(th), 0, 0, 0,                0,                                           0,                                           0, -1, -(sin(ph)*sin(th))/cos(th), -(cos(ph)*sin(th))/cos(th), 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0];
 
 
           
         B2 = dt* ...
           [                0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
             -cos(ps)*cos(th),   cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th), - sin(ph)*sin(ps) - cos(ph)*cos(ps)*sin(th),  0,                          0,                          0, 0;
             -cos(th)*sin(ps), - cos(ph)*cos(ps) - sin(ph)*sin(ps)*sin(th),   cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th),  0,                          0,                          0, 0;
                      sin(th),                            -cos(th)*sin(ph),                            -cos(ph)*cos(th),  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,           -sin(ph)/cos(th),           -cos(ph)/cos(th), 0;
                            0,                                           0,                                           0,  0,                   -cos(ph),                    sin(ph), 0;
                            0,                                           0,                                           0, -1, -(sin(ph)*sin(th))/cos(th), -(cos(ph)*sin(th))/cos(th), 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0];
                        
            
 
end


function [C] = measure_coeff_comp(x,u,wind)
        % see symbolic_calc.m for matlab derivation of this linearized
        % model
        
        % state
        Px = x(1);
        Py = x(2);
        Pz = x(3);
        Vx = x(4);
        Vy = x(5);
        Vz = x(6);
        ps = x(7);
        th = x(8);
        ph = x(9);
        bps = x(10);
        bth = x(11);
        bph = x(12);
        bax = x(13);
        bay = x(14);
        baz = x(15);
        bwx = x(16);
        bwy = x(17);
        bwz = x(18);
        bair = x(19);
        
        % predictive measurements
        abx = u(1);
        aby = u(2);
        abz = u(3);
        wbx = u(4);
        wby = u(5);
        wbz = u(6);
        gx  = u(7);
        gy  = u(8);
        gz  = u(9);
        
        % noise
        nax = 0;
        nay = 0;
        naz = 0;
        nwx = 0;
        nwy = 0;
        nwz = 0;
        nair = 0;
        
        % wind
        windx = wind(1);
        windy = wind(2);
        windz = wind(3);
        
        rho = atmosphere(-Pz);
        
        C = ...
            [ 1, 0, 0,                0,                0,                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 1, 0,                0,                0,                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 1,                0,                0,                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0,                1,                0,                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0,                0,                1,                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0,                0,                0,                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0,                0,                0,                0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0,                0,                0,                0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0,                0,                0,                0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0, rho*(Vx - windx), rho*(Vy - windy), rho*(Vz - windz), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
end









end

