
% this takes the paths generated in Path_Generation/generate_all_paths.m
% and handles the circle/straight line tracking

clear all; close all; clc;
dbstop if error

%% Initial Conditions for paths

p1 = [100; 100; -100];
p2 = [1000; 1000; -100];
psi1 = 90*pi/180;
psi2 = 0*pi/180;
Rmin = 150;
Vstar = 30;


run('Path_Generation/generate_all_paths.m')

%% Initial Conditions for Aircraft
P_0 = [100; 0];

%% Main Loop

% For each potential path
for i = 1:length(path_structs)
    
    current_path = path_structs(i);
    
    % eliminate errors
    if current_path.path1 == 0 
        continue
    elseif current_path.path3 == 0
        continue
    end
    
   
    [p1_type,p2_type,p3_type,CW1,CW2,CW3] = get_path_details(current_path);
    
    
    % Initialize for this run
    x = P_0;
    
    npts = 1000;
    
    % follow the first circle
    x_true1 = follow_circle(current_path.path1,P_0,current_path.c1,Rmin,CW1,Vstar,npts);
    
    % follow the second circle or line
    if CW2 == 0
        [heading_ref, line_start, line_stop] = define_straight_line(current_path.path1,current_path.path3);
        x_true2 = follow_straight_line(line_start,line_stop, x_true1(:,end), Vstar, heading_ref, npts);
        path_full = [current_path.path1, current_path.path3];
    elseif CW2 ~= 0
        x_true2 = follow_circle(current_path.path2,x_true1(:,end),current_path.c2,Rmin,CW2,Vstar,npts);
        path_full = [current_path.path1, current_path.path2, current_path.path3];
    else
        fprintf('ERROR: Undefined path 2 type %s', current_path.casenum)
    end
        
    % follow the third circle
    x_true3 = follow_circle(current_path.path3,x_true2(:,end),current_path.c3,Rmin,CW3,Vstar,npts);


    x_full = [x_true1, x_true2, x_true3];

    
    plot_paths(x_full, path_full, current_path.casenum)
    
end



function [heading_ref, line_start, line_stop] = define_straight_line(prev_circle_path,next_circle_path)

path1_finish = prev_circle_path(:,end);
path3_start  = next_circle_path(:,1);

line_vector = path3_start - path1_finish;
heading_ref = atan2(line_vector(2),line_vector(1));
line_start  = path1_finish;
line_stop   = path3_start;

end


function [p1_type,p2_type,p3_type,CW1,CW2,CW3] = get_path_details(path)

    p1_type = path.casenum(1);
    p2_type = path.casenum(2);
    p3_type = path.casenum(3);
    
    CW1 = direction(p1_type);
    CW2 = direction(p2_type);
    CW3 = direction(p3_type);

end

function [CW] = direction(pathtype)

if pathtype == 'R' 
    CW = 1;
elseif pathtype == 'L'
    CW = -1;
else 
    CW = 0;
end

end

function plot_paths(x_true, path, dubincase)

figure
hold on
plot(x_true(1,:),x_true(2,:),'Displayname','True Path')
plot(  path(1,:),  path(2,:),'Displayname','Desired Path')
legend
hold off
title(dubincase)


end
