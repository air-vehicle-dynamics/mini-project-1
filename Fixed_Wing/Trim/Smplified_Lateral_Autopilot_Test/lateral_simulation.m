clc;
clear all;
close all;


global chiref Kichi Kpchi Kdphi Kpphi Kpbeta Kdbeta ulat deltaastar deltarstar vstar pstar rstar phistar Rstar Vastar gammastar

unique_identifier_string = 'tc02';
mkdir(unique_identifier_string);

Vastar = 30;
g = 9.81;
phistar = 33.75*pi/180;
gammastar = 0*pi/180;
Rstar = 150;

t_out = 0:0.01:10;

ulat = [0;0];
deltaastar = -0.836*pi/180;
deltarstar = 1.080*pi/180;
chiref = 10*pi/180;
Kichi = 3.69;
Kpchi = 5.37;
Kdphi = 0.046;
Kpphi = 1.33;
Kpbeta = 2;
Kdbeta = 0.11;
vstar = 0.516;
pstar = -0.011;
rstar = 0.166;

xlat0 = zeros(6,1);

xlat0(1,1) = 0.5;
xlat0(2,1) = 0.3;
xlat0(3,1) = -0.02;
xlat0(4,1) = 0;
xlat0(5,1) = 5*pi/180;
xlat0(6,1) = 0;


% Define output matrix
x_store = zeros(length(t_out),length(xlat0));
x_store(1,:) = xlat0';
x_prev = xlat0;
u_store = zeros(length(t_out),2);
chiref_store = zeros(length(t_out),1);
chiref_store(1) = Vastar*cos(gammastar)*t_out(1)/Rstar;
for the_step = 2:length(t_out)
   
    chiref = Vastar*cos(gammastar)*t_out(the_step)/Rstar;
    chiref_store(the_step) = chiref;
    if mod(the_step, 10) == 0    
        fprintf('Step %i of %i\n\n',the_step, length(t_out))
    end
    
    t_prev = t_out(the_step - 1);
    t_current = t_out(the_step);

    the_output = ode45(@lateral_model, [t_prev t_current], x_prev);
    
    x_store(the_step,:) = the_output.y(:,end)';
    x_prev = the_output.y(:,end);
    u_store(the_step,:) = ulat;

end

%% Plotting

subplot(4,1,1)
plot(t_out, x_store(:,1),'linewidth',2)
ylabel('v (rad/s)')
hold on
plot(t_out, x_store(:,1) + vstar, 'r--', 'linewidth',2)
legend('\Delta v','v','location','east')
subplot(4,1,2)
plot(t_out, x_store(:,2),'linewidth',2)
ylabel('p (rad/s)')
hold on
plot(t_out, x_store(:,2) + pstar, 'r--', 'linewidth',2)
legend('\Delta v','v','location','east')
subplot(4,1,3)
plot(t_out, x_store(:,3),'linewidth',2)
hold on
ylabel('r (rad/s)')
plot(t_out, x_store(:,3) + rstar, 'r--', 'linewidth',2)
legend('\Delta r','r','location','east')
subplot(4,1,4)
plot(t_out, x_store(:,5)*180/pi,'linewidth',2)
ylabel('\chi (deg)')
hold on
plot(t_out, chiref_store*180/pi, 'r--','linewidth',2)
legend('\chi','\chi_{ref}','location','east')
xlabel('t(s)')

print(strcat(unique_identifier_string,'/States'),'-dpng')

figure;

subplot(3,1,1)
plot(t_out, x_store(:,4)*180/pi,'linewidth',2)
hold on
plot(t_out, x_store(:,6)*180/pi,'r--','linewidth',2)
ylabel('\Delta \phi (deg)')
legend('\Delta \phi','\Delta \phi_c','location','east')
subplot(3,1,2)
plot(t_out, u_store(:,1)*180/pi,'linewidth',2)
ylabel('\delta_a (deg)')
hold on
plot(t_out, (u_store(:,1) + deltaastar)*180/pi, 'r--', 'linewidth', 2)
legend('\Delta \delta_a','\delta_a','location','east')
subplot(3,1,3)
plot(t_out, u_store(:,2)*180/pi,'linewidth',2)
ylabel('\delta_r (deg)')
hold on
plot(t_out, (u_store(:,2) + deltarstar)*180/pi, 'r--', 'linewidth', 2)
legend('\Delta \delta_r','\delta_r','location','east')
xlabel('t (s)')

print(strcat(unique_identifier_string,'/Controls'),'-dpng')