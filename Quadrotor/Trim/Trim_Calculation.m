clc;
clear all;
close all;

global J m K_F K_M K_m W g L rho CD0_S
global GAMMA GAMMA1 GAMMA2 GAMMA3 GAMMA4 GAMMA5 GAMMA6 GAMMA7 GAMMA8

rho = 1.225;
W = [0;0;0];
g = 9.81;
m = 0.5;
J = [2.32E-3 0 0; 0 2.32E-3 0; 0 0 4E-3];
K_M = 1.5E-9;
K_F = 6.11E-8;
K_m = 20;
L = 0.175;
CD0_S = 0.000;


% Define Trim Condition
Vastar = 10;
Rstar = 20;
gammastardeg = 0;
gammastar = gammastardeg*pi/180;

% Define Initial Guess

phi0 = 0; %atan2(Vastar^2,g*Rstar);
%alpha0 = 10*pi/180;
alpha0 = 0*pi/180;
beta0 = 0*pi/180;


solver_options = optimoptions('fsolve', 'Display', 'iter', ...
	'Algorithm', 'levenberg-marquardt', 'FunctionTolerance', 1e-15, 'StepTolerance', 1E-15);

x = fsolve((@(x) Trim_Xdot_To_Minimize(x, Vastar, Rstar, gammastar)), [alpha0; beta0; phi0], solver_options);

alphastar = x(1);
betastar = x(2);
phistar = x(3);

psistar = 0;
ustar = Vastar*cos(alphastar)*cos(betastar);
vstar = Vastar*sin(betastar);
wstar = Vastar*sin(alphastar)*cos(betastar);
thetastar = alphastar + gammastar;
pstar = -Vastar*sin(thetastar)/Rstar;
qstar = Vastar*sin(phistar)*cos(thetastar)/Rstar;
rstar = Vastar*cos(phistar)*cos(thetastar)/Rstar;


J_x = J(1,1);
J_y = J(2,2);
J_z = J(3,3);

control_RHS = zeros(4,1);
control_RHS(1,1) = (m/K_F)*(qstar*ustar - pstar*vstar + g*cos(thetastar)*cos(phistar) - (1/m)*(sin(alphastar)*cos(betastar)*CD0_S*0.5*rho*Vastar^2));
control_RHS(2,1) = (J_x/(K_F*L))*GAMMA2*qstar*rstar;
control_RHS(3,1) = -(J_y/(K_F*L))*GAMMA5*pstar*rstar;
control_RHS(4,1) = -(J_z/K_M)*GAMMA7*pstar*qstar;
control_LHS = [1 1 1 1; 0 1 0 -1; -1 0 1 0; 1 -1 1 -1];

omegastarsq = control_LHS\control_RHS;

omega1star = sqrt(omegastarsq(1,1));
omega2star = sqrt(omegastarsq(2,1));
omega3star = sqrt(omegastarsq(3,1));
omega4star = sqrt(omegastarsq(4,1));

uvwstar = [ustar; vstar; wstar]

pqrstar = [pstar; qstar; rstar]

psi_theta_phi_star_deg = [psistar; thetastar; phistar]*180/pi

alpha_beta_star_deg = [alphastar; betastar]*180/pi

omegastar = sqrt(omegastarsq)
