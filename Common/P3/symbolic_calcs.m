

syms Vx Vy Vz abx aby abz bax bay baz na g hinv wbx wby wbz bwx bwy bwz nw gz
syms Px Py Pz air bair bps bth bph npx npy npz nvx nvy nvz nph nth nps
syms nax nay naz nwx nwy nwz nair windx windy windz rho

syms ps th ph

assume(ps, 'real')
assume(th, 'real')
assume(ph, 'real')

%% dynamical model

Hinv = (1 / cos(th) ) * [0 sin(ph) cos(ph); ...
    0 cos(ph)*cos(th) -sin(ph)*cos(th); ...
    cos(th) sin(ph)*sin(th) cos(ph)*sin(th)];


R_phi = [1 0 0; 0 cos(ph) sin(ph); 0 -sin(ph) cos(ph)];
R_th  = [cos(th) 0 -sin(th); 0 1 0; sin(th) 0 cos(th)];
R_psi = [cos(ps) sin(ps) 0; -sin(ps) cos(ps) 0; 0 0 1];

R_tb = R_phi * R_th * R_psi;
Rbt = transpose(R_tb);

% state variables
P = [Px; Py; Pz];     % position
V = [Vx; Vy; Vz];     % velocity
ab = [abx; aby; abz]; % accelerometer readings
wb = [wbx; wby; wbz]; % rate gyro readings

% biases
ba = [bax; bay; baz]; % accelerometer bias
bw = [bwx; bwy; bwz]; % rate gyro bias
g = [0; 0; gz];       % gravityyy

% noises
np = [npx; npy; npz]; % position noise
nv = [nvx; nvy; nvz]; % velocity noise
na = [nax; nay; naz]; % accelerometer noise
nw = [nwx; nwy; nwz]; % rate gyro noise

% other
wind = [windx; windy; windz];

n = [na; nw];   % full noise matrix
x = [P; V; ps; th; ph; bps; bth; bph; ba; bw; bair]; % full state
% since bair is actually read in as deltaP, we must convert to airspeed

f = [V; 
     Rbt*(ab - ba - na) + g;
     Hinv*(wb - bw - nw);
     zeros(10,1)];

dfdx = simplify(jacobian(f,x))
dfdn = simplify(jacobian(f,n))


%% measurement model

h = [P + np; 
     V + nv; 
     ps + bps + nps;  
     th + bth + nth; 
     ph + bph + nph;
     (rho/2) * norm((V - wind))^2 + bair + nair]; % velocity to pressure

C = simplify(jacobian(h,x)) 

