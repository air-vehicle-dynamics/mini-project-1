function demo_quadrotor_tracking()

close all

%----- Inertia
mass	= 0.5; % kg
moi_J	= diag([2.32E-3 2.32E-3 4E-3]); % kg.m2
g		= 9.81;
K_M = 1.5E-9;
K_F = 6.11E-8;
K_m = 20;
L = 0.175;

unique_identifier_string = 'linear_trimcond03';
mkdir(unique_identifier_string);

omega1star = 4746.16017;
omega2star = 4746.99719;
omega3star = 4746.16017;
omega4star = 4745.32300;

ustar = K_F*[1 1 1 1; 0 L 0 -L; -L 0 L 0; K_M/K_F -K_M/K_F K_M/K_F -K_M/K_F]*[omega1star^2; omega2star^2; omega3star^2; omega4star^2];

phistar  = 27.00721129079*pi/180;
thetastar = 0*pi/180;
pstar = 0;
qstar = 0.22705131945;
rstar = 0.44547468877;

% phistar = 0;
% thetastar = 0;
% pstar = 0;
% qstar = 0;
% rstar = 0;

n_pts	= 5001;
time_pts= linspace(0, 50, n_pts);	% s

%----- Initial conditions
posn0	= [21; -2; -105];
yaw0	= 98*pi/180;
vel0	= 10.6*[cos(yaw0); sin(yaw0); 0];
ptc0	= thetastar;
roll0	= phistar;
e_quat0	= euler2quat([yaw0; ptc0; roll0]);
omga0	= 1.025*[pstar; qstar; rstar];


%----- Reference
V_star	= 10;	% m/s
R_star	= 20;	% m


% Circle
px_ref	= R_star * cos( (V_star/R_star)* time_pts );
py_ref	= R_star * sin( (V_star/R_star)* time_pts );
pz_ref	= -100*ones(1, n_pts);
px_ref_dot = -V_star * sin( (V_star/R_star)* time_pts );
py_ref_dot = V_star * cos( (V_star/R_star)* time_pts );
pz_ref_dot = 0*ones(1,n_pts);
px_ref_2dot = -(V_star^2 / R_star) * cos( (V_star/R_star)* time_pts );
py_ref_2dot = -(V_star^2 / R_star) * sin( (V_star/R_star)* time_pts );
pz_ref_2dot = 0*ones(1,n_pts);
px_ref_3dot = (V_star^3 / (R_star^2)) * sin( (V_star/R_star)* time_pts );
py_ref_3dot = -(V_star^3 / (R_star^2)) * cos( (V_star/R_star)* time_pts );
pz_ref_3dot = 0*ones(1,n_pts);
yaw_ref	= (V_star/R_star)* time_pts + pi/2;
yaw_ref_dot = (V_star/R_star)*ones(1,n_pts);


% Straight Line Path

% px_ref = V_star*time_pts;
% py_ref = 0*ones(1,n_pts);
% pz_ref	= -100*ones(1, n_pts);
% px_ref_dot = V_star*ones(1,n_pts);
% py_ref_dot = 0*ones(1,n_pts);
% pz_ref_dot = 0*ones(1,n_pts);
% px_ref_2dot = 0*ones(1,n_pts);
% py_ref_2dot = 0*ones(1,n_pts);
% pz_ref_2dot = 0*ones(1,n_pts);
% px_ref_3dot = 0*ones(1,n_pts);
% py_ref_3dot = 0*ones(1,n_pts);
% pz_ref_3dot = 0*ones(1,n_pts);
% yaw_ref = 0*ones(1,n_pts);
% yaw_ref_dot = 0*ones(1,n_pts);


% Hover

% px_ref = 0*ones(1,n_pts);
% py_ref = 0*ones(1,n_pts);
% pz_ref	= -100*ones(1, n_pts);
% px_ref_dot = 0*ones(1,n_pts);
% py_ref_dot = 0*ones(1,n_pts);
% pz_ref_dot = 0*ones(1,n_pts);
% px_ref_2dot = 0*ones(1,n_pts);
% py_ref_2dot = 0*ones(1,n_pts);
% pz_ref_2dot = 0*ones(1,n_pts);
% px_ref_3dot = 0*ones(1,n_pts);
% py_ref_3dot = 0*ones(1,n_pts);
% pz_ref_3dot = 0*ones(1,n_pts);
% yaw_ref = 0*ones(1,n_pts);
% yaw_ref_dot = 0*ones(1,n_pts);



pos_ref = [px_ref; py_ref; pz_ref];
pos_ref_dot = [px_ref_dot; py_ref_dot; pz_ref_dot];
pos_ref_2dot = [px_ref_2dot; py_ref_2dot; pz_ref_2dot];
pos_ref_3dot = [px_ref_3dot; py_ref_3dot; pz_ref_3dot];



x		= [posn0; vel0; yaw0; ptc0; roll0; omga0; e_quat0];
x_true	= zeros(16, n_pts);
u_true	= zeros(4, n_pts);

x_true(:, 1)	= x;
for m1 = 2:n_pts
    t = time_pts(m1);
	dt		= time_pts(m1) - time_pts(m1 - 1);
	m1
    
%     gains for hover
%     K_pos_p = 0.3*[1; 1; 20];
%     K_pos_d = 0.3*[1; 1; 5];
%     K_roll_p =0.3; K_roll_d = 0.05;
%     K_ptc_p = 0.3; K_ptc_d = 0.05;
%     K_yaw_p = 0.3; K_yaw_d = 0.05;
    


%     gains for straight line
%     K_pos_p = 0.3*[1; 1; 20];
%     K_pos_d = 0.3*[1; 1; 5];
%     K_roll_p =0.3; K_roll_d = 0.05;
%     K_ptc_p = 0.3; K_ptc_d = 0.05;
%     K_yaw_p = 0.3; K_yaw_d = 0.05;

%     gains for circle
    K_pos_p = 0.5*[1; 1; 200];
    K_pos_d = 0.5*[1; 1; 50];
    K_roll_p =0.3; K_roll_d = 0.05;
    K_ptc_p = 0.3; K_ptc_d = 0.05;
    K_yaw_p = 0.3; K_yaw_d = 0.05;

    pos_tilde_2dot = [0;0;0];
    for m2 = 1:3
        pos_tilde_2dot(m2) = pos_ref_2dot(m2, m1-1) + ...
            K_pos_d(m2)*(pos_ref_dot(m2, m1-1) - x(m2+3)) + ...
            K_pos_p(m2)*(pos_ref(m2, m1-1) - x(m2));
    end
    
    roll_ptc_tilde = (1 / (g * cos(phistar)))*([-cos(yaw_ref(m1-1)) -sin(yaw_ref(m1-1)); ...
        -sin(yaw_ref(m1-1)) cos(yaw_ref(m1-1))] \ pos_tilde_2dot(1:2));

    input_u1 = mass* (g/cos(phistar) - pos_tilde_2dot(3)); 
    input_u2 = [K_roll_p*(roll_ptc_tilde(2) - (x(9))) + K_roll_d*( 0 - (x(10)) ); ...
        K_ptc_p*( roll_ptc_tilde(1) - (x(8)) ) + K_ptc_d*( 0 - (x(11)) ); ...
        K_yaw_p*( yaw_ref(m1-1) - x(7) ) + K_yaw_d*( yaw_ref_dot(m1-1) - x(12) )];
    
    input_u2 = input_u2 + ustar(2:4);
    
    u_t = [input_u1; input_u2];
    u_t_dt = u_t;
    
	%u_t		= [mass*g; 0; 0; 0];	% Hover
	%u_t_dt	= [mass*g; 0; 0; 0];	% Hover
	u_t_half= u_t;
	
	k1	= dt*quadrotor_dynamics(x,				u_t);
	k2	= dt*quadrotor_dynamics((x + 0.5*k1),	u_t_half);
	k3	= dt*quadrotor_dynamics((x + 0.5*k2),	u_t_half);
	k4	= dt*quadrotor_dynamics((x + k3),		u_t_dt);
	
	x	= x + (1/6)*k1 + (1/3)*k2 + (1/3)*k3 + (1/6)*k4;
	x_true(:, m1)	= x;
	u_true(:, m1)	= u_t;
end

figure('Name', 'Position');
plot3(x_true(1, :), x_true(2, :), -x_true(3, :), 'LineWidth', 2', 'Marker', '.', 'MarkerSize', 10); axis equal; grid on; hold on;
plot3(px_ref, py_ref, -pz_ref, 'LineWidth', 2', 'Marker', '.', 'MarkerSize', 10);
print(strcat(unique_identifier_string,'/Position3D'),'-dpng')

figure('Name', 'Position Coordinates');
subplot(311); plot(time_pts, x_true(1, :), time_pts, px_ref, 'LineWidth', 2); xlabel('Time (s)'); ylabel('p_x (m)'); grid on;
subplot(312); plot(time_pts, x_true(2, :), time_pts, py_ref, 'LineWidth', 2); xlabel('Time (s)'); ylabel('p_y (m)'); grid on;
subplot(313); plot(time_pts, -x_true(3, :), time_pts, -pz_ref, 'LineWidth', 2); xlabel('Time (s)'); ylabel('-p_z (m)'); grid on;
print(strcat(unique_identifier_string,'/Position'),'-dpng')

figure('Name', 'Velocity');
subplot(311); plot(time_pts, x_true(4, :), 'LineWidth', 2); xlabel('Time (s)'); ylabel('v_x (m/s)'); grid on;
subplot(312); plot(time_pts, x_true(5, :), 'LineWidth', 2); xlabel('Time (s)'); ylabel('v_y (m/s)'); grid on;
subplot(313); plot(time_pts, x_true(6, :), 'LineWidth', 2); xlabel('Time (s)'); ylabel('-v_z (m/s)'); grid on;
print(strcat(unique_identifier_string,'/Velocity'),'-dpng')

figure('Name', 'Euler angles');
subplot(311); plot(time_pts, wrapTo180(x_true(7, :)*180/pi), time_pts, wrapTo180(yaw_ref*180/pi), 'LineWidth', 2); xlabel('Time (s)'); ylabel('\psi (deg)'); grid on;
subplot(312); plot(time_pts, x_true(8, :)*180/pi, 'LineWidth', 2); xlabel('Time (s)'); ylabel('\theta (deg)'); grid on;
subplot(313); plot(time_pts, x_true(9, :)*180/pi, 'LineWidth', 2); xlabel('Time (s)'); ylabel('\phi (deg)'); grid on;
print(strcat(unique_identifier_string,'/Euler'),'-dpng')

figure('Name', 'Angular velocity');
subplot(311); plot(time_pts, x_true(10, :)*180/pi, 'LineWidth', 2); xlabel('Time (s)'); ylabel('p (deg/s)'); grid on;
subplot(312); plot(time_pts, x_true(11, :)*180/pi, 'LineWidth', 2); xlabel('Time (s)'); ylabel('q (deg/s)'); grid on;
subplot(313); plot(time_pts, x_true(12, :)*180/pi, 'LineWidth', 2); xlabel('Time (s)'); ylabel('r (deg/s)'); grid on;
print(strcat(unique_identifier_string,'/Angular'),'-dpng')

figure('Name', 'Controls');
subplot(411); plot(time_pts, u_true(1, :), 'Linewidth', 2); xlabel('Time (s)'); ylabel('u_1 (N)'); grid on; 
subplot(412); plot(time_pts, u_true(2, :), 'Linewidth', 2); xlabel('Time (s)'); ylabel('u_2 (Nm)'); grid on; 
subplot(413); plot(time_pts, u_true(3, :), 'Linewidth', 2); xlabel('Time (s)'); ylabel('u_3 (Nm)'); grid on; 
subplot(414); plot(time_pts, u_true(4, :), 'Linewidth', 2); xlabel('Time (s)'); ylabel('u_4 (Nm)'); grid on; 
print(strcat(unique_identifier_string,'/Controls'),'-dpng')

	function x_dot_ = quadrotor_dynamics(x_, u_)
		vel		= x_(4:6);
		yaw		= x_(7);
		ptc_	= x_(8);
		roll_	= x_(9);
		omga_	= x_(10:12);
		e_quat	= x_(13:16)';
		
		Rbt_		= [...
			cos(ptc_)*cos(yaw)	sin(roll_)*sin(ptc_)*cos(yaw) - cos(roll_)*sin(yaw)	cos(roll_)*sin(ptc_)*cos(yaw) + sin(roll_)*sin(yaw); ...
			cos(ptc_)*sin(yaw)	sin(roll_)*sin(ptc_)*sin(yaw) + cos(roll_)*cos(yaw)	cos(roll_)*sin(ptc_)*sin(yaw) - sin(roll_)*cos(yaw); ...
			-sin(ptc_)			sin(roll_)*cos(ptc_)									cos(roll_)*cos(ptc_)];
		Fb		= [0; 0; -u_(1)];
		Mb		= u_(2:4);
		
		posn_dot	= vel;
		vel_dot_e	= Rbt_*Fb/mass + [0; 0; g];
		e1			= quatmultiply(e_quat, (quatmultiply([0;Fb]', quatconj(e_quat))) )';
		vel_dot_q	= (1/mass) * e1(2:4) + [0; 0; g];
		
		%disp(norm( vel_dot_e - vel_dot_q));
		
		euler_dot	= [...
			-sin(ptc_)			0			1;
			sin(roll_)*cos(ptc_)	cos(roll_)	0;
			cos(roll_)*cos(ptc_)	-sin(roll_)	0] \ omga_;
		e_quat_dot	= 0.5*quatmultiply(e_quat, [0; omga_]');
		omga_dot	= moi_J \ (Mb - cross(omga_, moi_J*omga_));
		
		x_dot_		= [posn_dot; vel_dot_q; euler_dot; omga_dot; e_quat_dot'];
	end

	function quat_ = euler2quat(euler_angles_)
		e_psi_	= euler_angles_(1);
		e_thta_	= euler_angles_(2);
		e_phi_	= euler_angles_(3);
		quat_	=	[ ...
			cos(e_psi_/2)*cos(e_thta_/2)*cos(e_phi_/2) + sin(e_psi_/2)*sin(e_thta_/2)*sin(e_phi_/2); ...
			cos(e_psi_/2)*cos(e_thta_/2)*sin(e_phi_/2) - sin(e_psi_/2)*sin(e_thta_/2)*cos(e_phi_/2); ...
			cos(e_psi_/2)*sin(e_thta_/2)*cos(e_phi_/2) + sin(e_psi_/2)*cos(e_thta_/2)*sin(e_phi_/2); ...
			sin(e_psi_/2)*cos(e_thta_/2)*cos(e_phi_/2) - cos(e_psi_/2)*sin(e_thta_/2)*sin(e_phi_/2)];

	end
end
