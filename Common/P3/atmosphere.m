function rho = atmosphere(alt)
    % alt in m
    % this may be moved to its own function later
    h = alt;
    a1 = -6.5e-3; % K/m
    gr = 9.81; % m/s/s
    R_gas = 287; % J/(kg*K)

    T_sl = 288.16; % K
    rho_sl = 1.225; % kg/m^3

    if h <= 11000
        T = T_sl + a1 * h;
        rho = rho_sl * (T/T_sl)^(-1 - gr/(a1*R_gas));
    elseif h > 11000
        T11 = 216.66; % K
        rho11 = 0.3642; % kg / m^3
        rho = rho11*exp(-gr*(h - 11000)/(R_gas*T11));
    end
end