



cases = cell(8,1);
cases{1} = 'RSR';
cases{2} = 'RSL';
cases{3} = 'LSR';
cases{4} = 'LSL';
cases{5} = 'RLR';
cases{6} = 'RLR2';
cases{7} = 'LRL';
cases{8} = 'LRL2';


for thecase = 1:length(cases)
    the_case = cases{thecase};
    if the_case(2) == 'S'
        % Get the first circle and last circle (will get line params later)
        [path1,path2,c1,c2] = DubinsCSC_gen(p1, p2, psi1, psi2, Rmin, the_case, 0);
        path.casenum = the_case;
        path.path1   = path1;
        path.path2   = NaN;
        path.path3   = path2;
        path.c1      = c1;
        path.c2      = NaN;
        path.c3      = c2;
        
    else
        % Get all three circle paths
        [path1,path2,path3,c1,c2,c3] = DubinsCCC_gen(p1, p2, psi1, psi2, Rmin, the_case, 0);
        path.casenum = the_case;
        path.path1   = path1;
        path.path2   = path2;
        path.path3   = path3;
        path.c1      = c1;
        path.c2      = c2;
        path.c3      = c3;
    end
    
    path_structs(thecase) = path;
    
end