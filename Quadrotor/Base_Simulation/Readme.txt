"Base Simulation" is a simulation of the full nonlinear equations of motion for the quadrotor.  

User must supply control inputs and initial conditions.