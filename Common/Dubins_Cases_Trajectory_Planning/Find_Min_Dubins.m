clc;
clear all;
close all;

unique_identifier_string = 'delete';
mkdir(strcat(unique_identifier_string))

p1 = [500; 500; -100];
p2 = [750; 750; -100];
psi1 = 0*pi/180;
psi2 = 90*pi/180;
Rmin = 20;
Vstar = 10;

cases = cell(8,1);
cases{1} = 'RSR';
cases{2} = 'RSL';
cases{3} = 'LSR';
cases{4} = 'LSL';
cases{5} = 'RLR';
cases{6} = 'RLR2';
cases{7} = 'LRL';
cases{8} = 'LRL2';

path_lengths = zeros(8,1);

for thecase = 1:length(cases)
    figure;
    the_case = cases{thecase};
    if the_case(2) == 'S'
        the_path_length = DubinsCSC(p1, p2, psi1, psi2, Rmin, the_case, 1);
    else
        the_path_length = DubinsCCC(p1, p2, psi1, psi2, Rmin, the_case, 1);
    end
    path_lengths(thecase) = the_path_length;
    if the_path_length < 1E9
        print(strcat(unique_identifier_string,'/',unique_identifier_string,'_',the_case,'_Dubins_Path'),'-dpng')
    end
end

[themin, whichmin] = min(path_lengths);
mintime = themin/Vstar;
fprintf('The minimum length is %.1f meters, achieved by the %s path in %.1f seconds\n',themin, cases{whichmin}, mintime)
theminprintout = sprintf('The minimum length is %.1f meters, achieved by the %s path in %.1f seconds\n',themin, cases{whichmin}, mintime);


save(strcat(unique_identifier_string,'/',unique_identifier_string,'_Dubins_Data'))
