function the_J = Trim_Xdot_To_Minimize(xstar, Vastar, Rstar, gammastar)


global J m K_F K_M K_m W g L rho CD0_S
global GAMMA GAMMA1 GAMMA2 GAMMA3 GAMMA4 GAMMA5 GAMMA6 GAMMA7 GAMMA8

alphastar = xstar(1);
betastar = xstar(2);
phistar = xstar(3);

J_x = J(1,1);
J_y = J(2,2);
J_z = J(3,3);

xdotstar = zeros(10,1);

xdotstar(1,1) = -Vastar*sin(gammastar);
xdotstar(2,1) = 0;
xdotstar(3,1) = 0;
xdotstar(4,1) = 0;
xdotstar(5,1) = 0;
xdotstar(6,1) = 0;
xdotstar(7,1) = Vastar*(cos(gammastar))/Rstar;
xdotstar(8,1) = 0;
xdotstar(9,1) = 0;
xdotstar(10,1) = 0;

psistar = 0;
ustar = Vastar*cos(alphastar)*cos(betastar);
vstar = Vastar*sin(betastar);
wstar = Vastar*sin(alphastar)*cos(betastar);
thetastar = alphastar + gammastar;
pstar = -Vastar*sin(thetastar)/Rstar;
qstar = Vastar*sin(phistar)*cos(thetastar)/Rstar;
rstar = Vastar*cos(phistar)*cos(thetastar)/Rstar;

Jx = J(1,1);
Jy = J(2,2);
Jz = J(3,3);
Jxz = J(3,1);
GAMMA = Jx*Jz - Jxz^2;
GAMMA1 = (Jxz*(Jx - Jy + Jz))/GAMMA;
GAMMA2 = (Jz*(Jz-Jy)+Jxz^2)/GAMMA;
GAMMA3 = Jz/GAMMA;
GAMMA4 = Jxz/GAMMA;
GAMMA5 = (Jz-Jx)/Jy;
GAMMA6 = Jxz/Jy;
GAMMA7 = ((Jx-Jy)*Jx+Jxz^2)/GAMMA;
GAMMA8 = Jx/GAMMA;

control_RHS = zeros(4,1);
control_RHS(1,1) = (m/K_F)*(qstar*ustar - pstar*vstar + g*cos(thetastar)*cos(phistar) - (1/m)*(sin(alphastar)*cos(betastar)*CD0_S*0.5*rho*Vastar^2));
control_RHS(2,1) = (J_x/(K_F*L))*GAMMA2*qstar*rstar;
control_RHS(3,1) = -(J_y/(K_F*L))*GAMMA5*pstar*rstar;
control_RHS(4,1) = -(J_z/K_M)*GAMMA7*pstar*qstar;
control_LHS = [1 1 1 1; 0 1 0 -1; -1 0 1 0; 1 -1 1 -1];

omegastarsq = control_LHS\control_RHS;

omega1star = sqrt(omegastarsq(1,1));
omega2star = sqrt(omegastarsq(2,1));
omega3star = sqrt(omegastarsq(3,1));
omega4star = sqrt(omegastarsq(4,1));

f_x_u = zeros(10,1);

%f_x_u(1,1) = (cos(thetastar)*cos(psistar))*ustar + (sin(phistar)*sin(thetastar)*cos(psistar) - cos(phistar)*sin(psistar))*vstar...
%    +(cos(phistar)*sin(thetastar)*cos(psistar) + sin(phistar)*sin(psistar))*wstar;
%f_x_u(2,1) = (cos(thetastar)*sin(psistar))*ustar + (sin(phistar)*sin(thetastar)*sin(psistar) + cos(phistar)*cos(psistar))*vstar...
%    +(cos(phistar)*sin(thetastar)*sin(psistar) - sin(phistar)*cos(psistar))*wstar;

R_b_w = [cos(betastar)*cos(alphastar) sin(betastar) cos(betastar)*sin(alphastar);
    -sin(betastar)*cos(alphastar) cos(betastar) -sin(betastar)*sin(alphastar);
    -sin(alphastar) 0 cos(alphastar)];
F_d_b = R_b_w'*[-CD0_S*0.5*rho*Vastar^2; 0; 0];

f_x_u(1,1) = -ustar*sin(thetastar) + vstar*sin(phistar)*cos(thetastar) + wstar*cos(phistar)*cos(thetastar);
f_x_u(2,1) = rstar*vstar - qstar*wstar - g*sin(thetastar) + (1/m)*F_d_b(1);
f_x_u(3,1) = pstar*wstar - rstar*ustar + g*cos(thetastar)*sin(phistar) + (1/m)*F_d_b(2);
f_x_u(4,1) = qstar*ustar - pstar*vstar + g*cos(thetastar)*cos(phistar)...
    - (K_F/m)*(omega1star^2 + omega2star^2 + omega3star^2 + omega4star^2) + (1/m)*F_d_b(3);
f_x_u(5,1) = pstar + qstar*sin(phistar)*tan(thetastar) + rstar*cos(phistar)*tan(thetastar);
f_x_u(6,1) = qstar*cos(phistar) - rstar*sin(phistar);
f_x_u(7,1) = (qstar*sin(phistar) + rstar*cos(phistar))/cos(thetastar);
f_x_u(8,1) = -GAMMA2*qstar*rstar + K_F*L*(omega2star^2 - omega4star^2)/Jx;
f_x_u(9,1) = GAMMA5*pstar*rstar + K_F*L*(omega3star^2 - omega1star^2)/Jy;
f_x_u(10,1) = GAMMA7*pstar*qstar...
    + K_F*(K_M/K_F)*(omega1star^2 - omega2star^2 + omega3star^2 - omega4star^2)/Jz;

the_J = xdotstar - f_x_u;

end

