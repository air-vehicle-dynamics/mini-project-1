function the_J = Trim_Xdot_To_Minimize(x, Vastar, Rstar, gammastar)

global J m W X Y Z g L M N GAMMA GAMMA1 GAMMA2 GAMMA3 GAMMA4 GAMMA5 GAMMA6 GAMMA7 GAMMA8
global S b c S_prop rho K_motor K_TP K_omega e_oswald Aspect
global C_L_0 C_D_0 C_m_0 C_L_alpha C_D_alpha C_m_alpha C_L_q C_D_q C_m_q C_L_deltae C_D_deltae C_m_deltae C_prop 
global M_Stall alpha_0 epsilon C_D_p
global C_Y_0 C_l_0 C_n_0 C_Y_beta C_l_beta C_n_beta C_Y_p C_l_p C_n_p C_Y_r C_l_r C_n_r 
global C_Y_deltaa C_l_deltaa C_n_deltaa C_Y_deltar C_l_deltar C_n_deltar

alphastar = x(1);
betastar = x(2);
phistar = x(3);

xdotstar = zeros(10,1);

xdotstar(1,1) = -Vastar*sin(gammastar);
xdotstar(2,1) = 0;
xdotstar(3,1) = 0;
xdotstar(4,1) = 0;
xdotstar(5,1) = 0;
xdotstar(6,1) = 0;
xdotstar(7,1) = Vastar*(cos(gammastar))/Rstar;
xdotstar(8,1) = 0;
xdotstar(9,1) = 0;
xdotstar(10,1) = 0;

psistar = 0;
ustar = Vastar*cos(alphastar)*cos(betastar);
vstar = Vastar*sin(betastar);
wstar = Vastar*sin(alphastar)*cos(betastar);
thetastar = alphastar + gammastar;
pstar = -Vastar*sin(thetastar)/Rstar;
qstar = Vastar*sin(phistar)*cos(thetastar)/Rstar;
rstar = Vastar*cos(phistar)*cos(thetastar)/Rstar;

C_L = C_L_0 + C_L_alpha*alphastar;
C_D = C_D_p + C_L.^2/(pi*Aspect*e_oswald);
C_X = -C_D*cos(alphastar) + C_L*sin(alphastar);
C_X_q = -C_D_q*cos(alphastar) + C_L_q*sin(alphastar);
C_X_deltae = -C_D_deltae*cos(alphastar) + C_L_deltae*sin(alphastar);
C_Z = -C_D*sin(alphastar)-C_L*cos(alphastar);
C_Z_q = -C_D_q*sin(alphastar)-C_L_q*cos(alphastar);
C_Z_deltae = -C_D_deltae*sin(alphastar) - C_L_deltae*cos(alphastar);

deltaestar = (((-J(3,1)*(pstar^2-rstar^2)+(J(1,1)-J(3,3))*pstar*rstar)/(0.5*rho*Vastar^2*c*S))-C_m_0...
    - C_m_alpha*alphastar - C_m_q*c*qstar/(2*Vastar))/(C_m_deltae);

deltapstar = sqrt((2*m*(-rstar*vstar+qstar*wstar + g*sin(thetastar))...
    -rho*Vastar^2*S*(C_X + C_X_q*c*qstar/(2*Vastar) + C_X_deltae*deltaestar))/(rho*S_prop*C_prop*K_motor^2)...
    + Vastar^2/(K_motor^2));

Jxz = -J(3,1);
Jx = J(1,1);
Jy = J(2,2);
Jz = J(3,3);
GAMMA = Jx*Jz - Jxz^2;
GAMMA1 = (Jxz*(Jx - Jy + Jz))/GAMMA;
GAMMA2 = (Jz*(Jz-Jy)+Jxz^2)/GAMMA;
GAMMA3 = Jz/GAMMA;
GAMMA4 = Jxz/GAMMA;
GAMMA5 = (Jz-Jx)/Jy;
GAMMA6 = Jxz/Jy;
GAMMA7 = ((Jx-Jy)*Jx+Jxz^2)/GAMMA;
GAMMA8 = Jx/GAMMA;

C_p_0 = GAMMA3*C_l_0 + GAMMA4*C_n_0;
C_p_beta = GAMMA3*C_l_beta + GAMMA4*C_n_beta;
C_p_p = GAMMA3*C_l_p + GAMMA4*C_n_p;
C_p_r = GAMMA3*C_l_r + GAMMA4*C_n_r;
C_p_deltaa = GAMMA3*C_l_deltaa + GAMMA4*C_n_deltaa;
C_p_deltar = GAMMA3*C_l_deltar + GAMMA4*C_n_deltar;
C_r_0 = GAMMA4*C_l_0 + GAMMA8*C_n_0;
C_r_beta = GAMMA4*C_l_beta + GAMMA8*C_n_beta;
C_r_p = GAMMA4*C_l_p + GAMMA8*C_n_p;
C_r_r = GAMMA4*C_l_r + GAMMA8*C_n_r;
C_r_deltaa = GAMMA4*C_l_deltaa + GAMMA8*C_n_deltaa;
C_r_deltar = GAMMA4*C_l_deltar + GAMMA8*C_n_deltar;

deltaa_deltar_vector = zeros(2,1);
deltaa_deltar_vector(1,1) = (-GAMMA1*pstar*qstar + GAMMA2*qstar*rstar)/(0.5*rho*Vastar^2*S*b) - C_p_0 - C_p_beta*betastar...
    - C_p_p*b*pstar/(2*Vastar) - C_p_r*b*rstar/(2*Vastar);
deltaa_deltar_vector(2,1) = (-GAMMA7*pstar*qstar + GAMMA1*qstar*rstar)/(0.5*rho*Vastar^2*S*b) - C_r_0 - C_r_beta*betastar...
    - C_r_p*b*pstar/(2*Vastar) - C_r_r*b*rstar/(2*Vastar);

deltaa_deltar = [C_p_deltaa C_p_deltar; C_r_deltaa C_r_deltar]\deltaa_deltar_vector;
deltaastar = deltaa_deltar(1,1);
deltarstar = deltaa_deltar(2,1);

f_x_u = zeros(10,1);

%f_x_u(1,1) = (cos(thetastar)*cos(psistar))*ustar + (sin(phistar)*sin(thetastar)*cos(psistar) - cos(phistar)*sin(psistar))*vstar...
%    +(cos(phistar)*sin(thetastar)*cos(psistar) + sin(phistar)*sin(psistar))*wstar;
%f_x_u(2,1) = (cos(thetastar)*sin(psistar))*ustar + (sin(phistar)*sin(thetastar)*sin(psistar) + cos(phistar)*cos(psistar))*vstar...
%    +(cos(phistar)*sin(thetastar)*sin(psistar) - sin(phistar)*cos(psistar))*wstar;
f_x_u(1,1) = -ustar*sin(thetastar) + vstar*sin(phistar)*cos(thetastar) + wstar*cos(phistar)*cos(thetastar);
f_x_u(2,1) = rstar*vstar - qstar*wstar - g*sin(thetastar) + (rho*Vastar^2*S/(2*m))*(C_X + C_X_q*c*qstar/(2*Vastar)...
    + C_X_deltae*deltaestar) + (rho*S_prop*C_prop/(2*m))*((K_motor*deltapstar)^2 - Vastar^2);
f_x_u(3,1) = pstar*wstar - rstar*ustar + g*cos(thetastar)*sin(phistar) + (rho*Vastar^2*S/(2*m))*(C_Y_0 + C_Y_beta*betastar...
    + C_Y_p*b*pstar/(2*Vastar) + C_Y_r*b*rstar/(2*Vastar) + C_Y_deltaa*deltaastar + C_Y_deltar*deltarstar);
f_x_u(4,1) = qstar*ustar - pstar*vstar + g*cos(thetastar)*cos(phistar) + (rho*Vastar^2*S/(2*m))*(C_Z...
    + C_Z_q*c*qstar/(2*Vastar) + C_Z_deltae*deltaestar);
f_x_u(5,1) = pstar + qstar*sin(phistar)*tan(thetastar) + rstar*cos(phistar)*tan(thetastar);
f_x_u(6,1) = qstar*cos(phistar) - rstar*sin(phistar);
f_x_u(7,1) = (qstar*sin(phistar) + rstar*cos(phistar))/cos(thetastar);
f_x_u(8,1) = GAMMA1*pstar*qstar - GAMMA2*qstar*rstar + 0.5*rho*Vastar^2*S*b*(C_p_0 + C_p_beta*betastar + C_p_p*b*pstar/(2*Vastar)...
    + C_p_r*b*rstar/(2*Vastar) + C_p_deltaa*deltaastar + C_p_deltar*deltarstar);
f_x_u(9,1) = GAMMA5*pstar*rstar - GAMMA6*(pstar^2-rstar^2) + (rho*Vastar^2*S*c/(2*Jy))*(C_m_0 + C_m_alpha*alphastar...
    + C_m_q*c*qstar/(2*Vastar) + C_m_deltae*deltaestar);
f_x_u(10,1) = GAMMA7*pstar*qstar - GAMMA1*qstar*rstar + 0.5*rho*Vastar^2*S*b*(C_r_0 + C_r_beta*betastar + C_r_p*b*pstar/(2*Vastar)...
    + C_r_r*b*rstar/(2*Vastar) + C_r_deltaa*deltaastar + C_r_deltar*deltarstar);

the_J = norm(xdotstar - f_x_u)^2;
the_J = xdotstar - f_x_u;
end

