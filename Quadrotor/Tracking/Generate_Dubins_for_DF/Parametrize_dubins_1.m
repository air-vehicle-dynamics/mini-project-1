clc;
clear all;
close all;

the_case = 2;

if the_case == 1
    t1 = linspace(0,1.60533,1000);
    x1 = 20*sin(0.5*t1);
    y1 = 20 + 20*cos(0.5*t1 - pi);
    z1 = -100*ones(1,length(t1));

    t2 = linspace(1.60533, 69.4286, 1000);
    x2 = 14.3842 + 6.94793*(t2 - 1.60533);
    y2 = 6.1042 + 7.192095*(t2 - 1.60533);
    z2 = -100*ones(1,length(t2));

    t3 = linspace(69.42863, 71.03396, 1000);
    x3 = 500 + 20*sin(0.5*(t3 - 71.03396));
    y3 = 480 + 20*cos(0.5*(t3 - 71.03396));
    z3 = -100*ones(1,length(t3));

    t_all = [t1 t2 t3];
    x_all = [x1 x2 x3];
    y_all = [y1 y2 y3];
    z_all = [z1 z2 z3];

    plot(y_all, x_all, 'linewidth', 2)
    axis equal
    
end

if the_case == 2
    t1 = linspace(0,1.570796,1000);
    x1 = 500 + 20*sin(0.5*t1);
    y1 = 520 + 20*cos(0.5*t1 - pi);
    z1 = -100*ones(1,length(t1));
    
    t2 = linspace(1.570796, 34.139916, 1000);
    x2 = 514.142 + 7.07107*(t2 - 1.570796);
    y2 = 505.858 + 7.07107*(t2 - 1.570796);
    z2 = -100*ones(1,length(t2));
    
    t3 = linspace(34.139916, 35.6685, 1000);
    x3 = 730 + 20*cos(0.5*(t3 - 35.6685));
    y3 = 750 + 20*sin(0.5*(t3 - 35.6685));
    z3 = -100*ones(1,length(t3));
end

x_all = [x1 x2 x3];
y_all = [y1 y2 y3];
plot(y_all, x_all, 'linewidth', 2)
axis equal
xlabel('y')
ylabel('x')
print(strcat('Dubins_path_',num2str(the_case)),'-dpng')