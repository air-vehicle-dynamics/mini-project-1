function [x_true] = follow_straight_line(P_start,P_stop,x_start,V_star,heading_ref,npts)


% tstart = 0;
% tend   = 30;
% time_pts = linspace(tstart,tend,npts);


heading_infty = pi/4;   % Reference heading at infinity
K_heading     = 0.05;   % Heading referene gain
tol = [1; 1];
i_tol = 1e5;

dt = 0.01;

Rtp = [cos(heading_ref) sin(heading_ref); -sin(heading_ref) cos(heading_ref)];

x		= [x_start];
% x_true	= zeros(2, npts);
% u_true	= zeros(1, npts);

x_true(:, 1)	= x;

i = 2;
while i > 0
    
    e_posn_p = Rtp*( x_true(:,i-1) - P_start);
    
	u_t = heading_ref - heading_infty*(2/pi)*atan(K_heading*e_posn_p(2)); % *** CALCULATE THE COMMANDED HEADING 
	
    u_t_dt	= u_t;
	u_t_half= 0.5*(u_t + u_t_dt);
	
	k1	= dt*aircraft_kinematics(x,				u_t);
	k2	= dt*aircraft_kinematics((x + 0.5*k1),	u_t_half);
	k3	= dt*aircraft_kinematics((x + 0.5*k2),	u_t_half);
	k4	= dt*aircraft_kinematics((x + k3),		u_t_dt);
	
	x	= x + (1/6)*k1 + (1/3)*k2 + (1/3)*k3 + (1/6)*k4;
	x_true(:,i)	= x;
	u_true(:,i)	= u_t;
    
    
    closeness = P_stop - x;
    
    if closeness(1) <= tol(1)
        break
    elseif closeness(2) <= tol(2)
        break
    elseif i >= i_tol 
        break
    end
    
    i = i + 1;
     
    
end


function x_dot_ = aircraft_kinematics(x_, u_)
		
% 		u_ = max( min(u_, (V_star / R_min)), (V_star / R_min));
		
		
		heading_chi	= u_; %x_(3);
		x_dot_		= [V_star*cos(heading_chi); V_star*sin(heading_chi)]; %; u_];
end



end