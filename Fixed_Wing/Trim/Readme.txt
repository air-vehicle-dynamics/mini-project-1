"Trim_Calculation" provides the user with the trim state, A and B matricies, A matrix eigenvalues, gains, and LQR control K matrix for a given trim condition.

"Lateral_Autopilot_Playground" is a simulink model to test the lateral controller.

"Simplified_Longitudinal_Autopilot_Test" is where output of the longitudinal LQR controller will be saved.