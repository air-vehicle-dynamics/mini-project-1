function [x_hat] =  EKF_ac(time,sensor_data,predict_sensor_data,wind,rate_gyro,accel,pressure,dig_comp,gps)

% [Pt; Vt; psi; theta; phi; psi bias; theta bias; phi bias; accel bias;
% rate bias; airspeed bias]

% select sensor inputs
gyro_Hz     = rate_gyro.read_rate;
accel_Hz    = accel.read_rate;
pressure_Hz = pressure.read_rate;
comp_Hz     = dig_comp.read_rate;
gps_Hz      = gps.read_rate;

GPS_pos  = sensor_data(1:3,:);
GPS_vel  = sensor_data(4:6,:);
MAG      = sensor_data(7:9,:);
AIRSPEED = sensor_data(10,:);
ACCEL    = predict_sensor_data(1:3,:);
GYRO     = predict_sensor_data(4:6,:);
g        = predict_sensor_data(7:9,:);

% initial state estimate is whatever noisy input we can read directly, zero
% for the rest (biases)
x_hat(:,1) = [GPS_pos(:,1); GPS_vel(:,1); MAG(:,1); zeros(10,1)];

Pt(:,1)          = x_hat(1:3,1);
Vt(:,1)          = x_hat(4:6,1);
psi(1)           = x_hat(7,1);
theta(1)         = x_hat(8,1);
phi(1)           = x_hat(9,1);
bias_psi(1)      = x_hat(10,1);
bias_theta(1)    = x_hat(11,1);
bias_phi(1)      = x_hat(12,1);
bias_accel(:,1)  = x_hat(13:15,1);
bias_gyro(:,1)   = x_hat(16:18,1);
bias_airspeed(1) = x_hat(19,1);

% Formulate the measurement and predictive noise covariance matrices
GPS_posn_dev = blkdiag(gps.noise_stdev_pos_NE^2, gps.noise_stdev_pos_NE^2, gps.noise_stdev_pos_Z^2);
GPS_vel_dev  = blkdiag(gps.noise_stdev_vel_NE^2, gps.noise_stdev_vel_NE^2, gps.noise_stdev_vel_Z^2);
MAG_dev      = dig_comp.noise_stdev^2 * eye(3);
AIRSPEED_dev = pressure.noise_stdev^2;
ACCEL_dev    = accel.noise_stdev^2 * eye(3);
GYRO_dev     = rate_gyro.noise_stdev^2 * eye(3);

% initialize P as R and some zeros
R = blkdiag(GPS_posn_dev, GPS_vel_dev, MAG_dev, AIRSPEED_dev); % observation noise covariance
Q = blkdiag(ACCEL_dev, GYRO_dev, 0);                           % process noise covariance
P = blkdiag(R, eye(9));  


for i = 2:length(time)
    i
    dt = time(i) - time(i-1);
    t = time(i);
    
    % check to see if new sensor readings have come in
%     if mod(t, 1/GPS_Hz) == 0
%         
%     end
    
    % organize variables
    Pt(:,i)          = x_hat(1:3,  i-1);
    Vt(:,i)          = x_hat(4:6,  i-1);
    psi(i)           = x_hat(7,    i-1);
    theta(i)         = x_hat(8,    i-1);
    phi(i)           = x_hat(9,    i-1);
    bias_psi(i)      = x_hat(10,   i-1);
    bias_theta(i)    = x_hat(11,   i-1);
    bias_phi(i)      = x_hat(12,   i-1);
    bias_accel(:,i)  = x_hat(13:15,i-1);
    bias_gyro(:,i)   = x_hat(16:18,i-1);
    bias_airspeed(i) = x_hat(19,   i-1);
    
    
    
    %% Predictive Update

     % predict the next step using rk4 integration
     u1	= [ACCEL(:,i - 1) - bias_accel(:,i - 1);
           GYRO(:,i - 1)  - bias_gyro(:,i - 1);
           g(:,i - 1)];  
         
     u2 = [ACCEL(:,i) - bias_accel(:,i);
           GYRO(:,i)  - bias_gyro(:,i);
           g(:,i)];
       
	 u12 = 0.5 * (u1 + u2);	    
     
     k1 = dt*f_xun(x_hat(:,i-1),           u1);
     k2 = dt*f_xun(x_hat(:,i-1) + 0.5*k1, u12);
     k3 = dt*f_xun(x_hat(:,i-1) + 0.5*k2, u12);
     k4 = dt*f_xun(x_hat(:,i-1) + k3,      u2);

     % this keeps blowing up the altitude
	 x_hat_minus = x_hat(:,i-1) + ((1/6)*k1 + (1/3)*k2 + (1/3)*k3 + (1/6)*k4);
    
     % recalculate covariance coefficient matrices
     [A,B2] = predict_coeff_comp(x_hat(:,i-1),predict_sensor_data(:,i-1),dt);
     
     % Update P matrix

     P_minus = A*P*A' + B2*Q*B2';
     
    %% Measurement Update
     
    % measurement model
    C = measure_coeff_comp(x_hat_minus,predict_sensor_data(:,i),wind);
    
    % Kalman gain
    Kn = P_minus*C' / (C*P_minus*C' + R);
    
    % update predictive estimate with new measurement
    zm = sensor_data(:,i); % current measurement, schedule with read rates later
    
    rho = atmosphere(-x_hat_minus(3));
    
    x_hat(:,i) = x_hat_minus + Kn * (zm - h_xn(x_hat_minus, rho, wind));
    
    P = (eye(19) - Kn * C) * P_minus;
      
           
end
        
    
    function xdot = f_xun(x, u)
        
        ps = x(7);
        th = x(8);
        ph = x(9);
        
        Hinv_fu = (1 / cos(th) ) * [0 sin(ph) cos(ph); ...
		0 cos(ph)*cos(th) -sin(ph)*cos(th); ...
		cos(th) sin(ph)*sin(th) cos(ph)*sin(th)];
        
        R_phi = [1 0 0; 0 cos(ph) sin(ph); 0 -sin(ph) cos(ph)];
        R_th  = [cos(th) 0 -sin(th); 0 1 0; sin(th) 0 cos(th)];
        R_psi = [cos(ps) sin(ps) 0; -sin(ps) cos(ps) 0; 0 0 1];

        R_tb = R_phi * R_th * R_psi;
        R_bt_fu = transpose(R_tb);
    
        xdot = [x(4:6); R_bt_fu*u(1:3) + u(7:9); Hinv_fu*u(4:6); zeros(10,1)];
    end

    function measurement = h_xn(x, rho, wind)
        
        Vx = x(4);
        Vy = x(5);
        Vz = x(6);
        
        Deltap = (rho/2)*((Vx - wind(1))^2 + (Vy - wind(2))^2 + (Vz - wind(3))^2);
        
        measurement = [x(1:9) + [zeros(6,1); x(10:12)]; Deltap + x(19)];
    end

        

    function [A,B2] = predict_coeff_comp(x,u,dt)
        % see symbolic_calc.m for matlab derivation of this linearized
        % model
        
        % state
        Px = x(1);
        Py = x(2);
        Pz = x(3);
        Vx = x(4);
        Vy = x(5);
        Vz = x(6);
        ps = x(7);
        th = x(8);
        ph = x(9);
        bps = x(10);
        bth = x(11);
        bph = x(12);
        bax = x(13);
        bay = x(14);
        baz = x(15);
        bwx = x(16);
        bwy = x(17);
        bwz = x(18);
        bair = x(19);
        
        % predictive measurements
        abx = u(1);
        aby = u(2);
        abz = u(3);
        wbx = u(4);
        wby = u(5);
        wbz = u(6);
        gx  = u(7);
        gy  = u(8);
        gz  = u(9);
        
        % noise
        nax = 0;
        nay = 0;
        naz = 0;
        nwx = 0;
        nwy = 0;
        nwz = 0;
        nair = 0;
        
        A = eye(19) + dt *...         
        [0, 0, 0, 1, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 1, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 1,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0, (cos(ph)*cos(ps) + sin(ph)*sin(ps)*sin(th))*(bay - aby + nay) - (cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th))*(baz - abz + naz) + cos(th)*sin(ps)*(bax - abx + nax), cos(ps)*sin(th)*(bax - abx + nax) - cos(ph)*cos(ps)*cos(th)*(baz - abz + naz) - cos(ps)*cos(th)*sin(ph)*(bay - aby + nay), - (sin(ph)*sin(ps) + cos(ph)*cos(ps)*sin(th))*(bay - aby + nay) - (cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th))*(baz - abz + naz), 0, 0, 0, -cos(ps)*cos(th),   cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th), - sin(ph)*sin(ps) - cos(ph)*cos(ps)*sin(th),  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0, (cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th))*(bay - aby + nay) - (sin(ph)*sin(ps) + cos(ph)*cos(ps)*sin(th))*(baz - abz + naz) - cos(ps)*cos(th)*(bax - abx + nax), sin(ps)*sin(th)*(bax - abx + nax) - cos(ph)*cos(th)*sin(ps)*(baz - abz + naz) - cos(th)*sin(ph)*sin(ps)*(bay - aby + nay),   (cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th))*(bay - aby + nay) + (cos(ph)*cos(ps) + sin(ph)*sin(ps)*sin(th))*(baz - abz + naz), 0, 0, 0, -cos(th)*sin(ps), - cos(ph)*cos(ps) - sin(ph)*sin(ps)*sin(th),   cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th),  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                         cos(th)*(bax - abx + nax) + cos(ph)*sin(th)*(baz - abz + naz) + sin(ph)*sin(th)*(bay - aby + nay),                                                           cos(th)*sin(ph)*(baz - abz + naz) - cos(ph)*cos(th)*(bay - aby + nay), 0, 0, 0,          sin(th),                            -cos(th)*sin(ph),                            -cos(ph)*cos(th),  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                           - (cos(ph)*sin(th)*(bwz + nwz - wbz))/cos(th)^2 - (sin(ph)*sin(th)*(bwy + nwy - wby))/cos(th)^2,                                                       (sin(ph)*(bwz + nwz - wbz))/cos(th) - (cos(ph)*(bwy + nwy - wby))/cos(th), 0, 0, 0,                0,                                           0,                                           0,  0,           -sin(ph)/cos(th),           -cos(ph)/cos(th), 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                           cos(ph)*(bwz + nwz - wbz) + sin(ph)*(bwy + nwy - wby), 0, 0, 0,                0,                                           0,                                           0,  0,                   -cos(ph),                    sin(ph), 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                            -(bwz*cos(ph) + nwz*cos(ph) + bwy*sin(ph) - wbz*cos(ph) + nwy*sin(ph) - wby*sin(ph))/cos(th)^2,                                       (sin(ph)*sin(th)*(bwz + nwz - wbz))/cos(th) - (cos(ph)*sin(th)*(bwy + nwy - wby))/cos(th), 0, 0, 0,                0,                                           0,                                           0, -1, -(sin(ph)*sin(th))/cos(th), -(cos(ph)*sin(th))/cos(th), 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0;
         0, 0, 0, 0, 0, 0,                                                                                                                                                                 0,                                                                                                                         0,                                                                                                                               0, 0, 0, 0,                0,                                           0,                                           0,  0,                          0,                          0, 0];
 
 
           
         B2 = dt* ...
           [                0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
             -cos(ps)*cos(th),   cos(ph)*sin(ps) - cos(ps)*sin(ph)*sin(th), - sin(ph)*sin(ps) - cos(ph)*cos(ps)*sin(th),  0,                          0,                          0, 0;
             -cos(th)*sin(ps), - cos(ph)*cos(ps) - sin(ph)*sin(ps)*sin(th),   cos(ps)*sin(ph) - cos(ph)*sin(ps)*sin(th),  0,                          0,                          0, 0;
                      sin(th),                            -cos(th)*sin(ph),                            -cos(ph)*cos(th),  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,           -sin(ph)/cos(th),           -cos(ph)/cos(th), 0;
                            0,                                           0,                                           0,  0,                   -cos(ph),                    sin(ph), 0;
                            0,                                           0,                                           0, -1, -(sin(ph)*sin(th))/cos(th), -(cos(ph)*sin(th))/cos(th), 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0;
                            0,                                           0,                                           0,  0,                          0,                          0, 0];
                        
            
 
    end

    function [C] = measure_coeff_comp(x,u,wind)
        % see symbolic_calc.m for matlab derivation of this linearized
        % model
        
        % state
        Px = x(1);
        Py = x(2);
        Pz = x(3);
        Vx = x(4);
        Vy = x(5);
        Vz = x(6);
        ps = x(7);
        th = x(8);
        ph = x(9);
        bps = x(10);
        bth = x(11);
        bph = x(12);
        bax = x(13);
        bay = x(14);
        baz = x(15);
        bwx = x(16);
        bwy = x(17);
        bwz = x(18);
        bair = x(19);
        
        % predictive measurements
        abx = u(1);
        aby = u(2);
        abz = u(3);
        wbx = u(4);
        wby = u(5);
        wbz = u(6);
        gx  = u(7);
        gy  = u(8);
        gz  = u(9);
        
        % noise
        nax = 0;
        nay = 0;
        naz = 0;
        nwx = 0;
        nwy = 0;
        nwz = 0;
        nair = 0;
        
        % wind
        windx = wind(1);
        windy = wind(2);
        windz = wind(3);
        
        rho = atmosphere(-Pz);
        
        C = ...
            [ 1, 0, 0,                0,                0,                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 1, 0,                0,                0,                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 1,                0,                0,                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0,                1,                0,                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0,                0,                1,                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0,                0,                0,                1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0,                0,                0,                0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0,                0,                0,                0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0,                0,                0,                0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0;
              0, 0, 0, rho*(Vx - windx), rho*(Vy - windy), rho*(Vz - windz), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
         
    end

end

