classdef Model
    % a class dedicated to the model 
    
    properties
        % put process model here (line by line) for x_dot = f(x,u)
        % keep them in order
        p_dot
        q_dot
        r_dot
        u_dot
        v_dot
        w_dot
        etc
    end
    
    properties(Constant) % add more as needed
        g = 9.81; % gravitational constant
        m = 10;   % aircraft mass (fat)
    end
    
 
    methods 
        
        % create the state of noninear equations
        function state = create_state(obj)
            num_properties = length(properties(obj));
            
            state = [];
            
            for i = 1:num_properties
               state = [obj.p_dot;
                        obj.q_dot
                        obj.r_dot]; % will have to fill in the rest
            end
        end
        
        % get the linearized form of the state relationship (jacobian)
        function J = linearize(obj,symbolic_vars)
            
            % create the state
            state_nonlinear = create_state(obj);
            
            % get jacobian of the state
            J = jacobian(state_nonlinear, symbolic_vars);
        end
    end
end
    