clc;
clear all;
close all;

global J m W g GAMMA GAMMA1 GAMMA2 GAMMA3 GAMMA4 GAMMA5 GAMMA6 GAMMA7 GAMMA8
global S b c S_prop rho K_motor K_TP K_omega e_oswald Aspect
global C_L_0 C_D_0 C_m_0 C_L_alpha C_D_alpha C_m_alpha C_L_q C_D_q C_m_q C_L_deltae C_D_deltae C_m_deltae C_prop 
global M_Stall alpha_0 epsilon C_D_p
global C_Y_0 C_l_0 C_n_0 C_Y_beta C_l_beta C_n_beta C_Y_p C_l_p C_n_p C_Y_r C_l_r C_n_r 
global C_Y_deltaa C_l_deltaa C_n_deltaa C_Y_deltar C_l_deltar C_n_deltar

unique_identifier_string = 'Simplified_Lontigudinal_Autopilot_Test/delete';
mkdir(unique_identifier_string);

W = [0;0;0];
g = 9.81;
rho = 1.225;

Define_Aerosonde_Parameters;


%% Define Trim Condition
Vastar = 30;
Rstar = 150;
gammastardeg = 3;
gammastar = gammastardeg*pi/180;

%% Define Initial Guess

phi0 = atan2(Vastar^2,g*Rstar);
%alpha0 = 10*pi/180;
alpha0 = ((2*m*g)/(rho*Vastar^2*S*cos(phi0)) - C_L_0)*(1/C_L_alpha);
beta0 = 0*pi/180;

 
%% Use solver

solver_options = optimoptions('fsolve', 'Display', 'iter', ...
	'Algorithm', 'levenberg-marquardt', 'FunctionTolerance', 1e-10);

x = fsolve((@(x) Trim_Xdot_To_Minimize(x, Vastar, Rstar, gammastar)), [alpha0; beta0; phi0], solver_options);

%% Postprocess solver

alphastar = x(1);
betastar = x(2);
phistar = x(3);

psistar = 0;
ustar = Vastar*cos(alphastar)*cos(betastar);
vstar = Vastar*sin(betastar);
wstar = Vastar*sin(alphastar)*cos(betastar);
thetastar = alphastar + gammastar;
pstar = -Vastar*sin(thetastar)/Rstar;
qstar = Vastar*sin(phistar)*cos(thetastar)/Rstar;
rstar = Vastar*cos(phistar)*cos(thetastar)/Rstar;

C_L = C_L_0 + C_L_alpha*alphastar;
C_D = C_D_p+C_L.^2/(pi*Aspect*e_oswald);
C_X = -C_D*cos(alphastar) + C_L*sin(alphastar);
C_X_alpha = -C_D_alpha*cos(alphastar) + C_L_alpha*sin(alphastar);
C_X_q = -C_D_q*cos(alphastar) + C_L_q*sin(alphastar);
C_X_deltae = -C_D_deltae*cos(alphastar) + C_L_deltae*sin(alphastar);
C_Z = -C_D*sin(alphastar)-C_L*cos(alphastar);
C_Z_alpha = -C_D_alpha*sin(alphastar)-C_L_alpha*cos(alphastar);
C_Z_q = -C_D_q*sin(alphastar)-C_L_q*cos(alphastar);
C_Z_deltae = -C_D_deltae*sin(alphastar) - C_L_deltae*cos(alphastar);

deltaestar = (((-J(3,1)*(pstar^2-rstar^2)+(J(1,1)-J(3,3))*pstar*rstar)/(0.5*rho*Vastar^2*c*S))-C_m_0...
    - C_m_alpha*alphastar - C_m_q*c*qstar/(2*Vastar))/(C_m_deltae);

deltapstar = sqrt((2*m*(-rstar*vstar+qstar*wstar + g*sin(thetastar))...
    -rho*Vastar^2*S*(C_X + C_X_q*c*qstar/(2*Vastar) + C_X_deltae*deltaestar))/(rho*S_prop*C_prop*K_motor^2)...
    + Vastar^2/(K_motor^2));

Jxz = -J(3,1);
Jx = J(1,1);
Jy = J(2,2);
Jz = J(3,3);
GAMMA = Jx*Jz - Jxz^2;
GAMMA1 = (Jxz*(Jx - Jy + Jz))/GAMMA;
GAMMA2 = (Jz*(Jz-Jy)+Jxz^2)/GAMMA;
GAMMA3 = Jz/GAMMA;
GAMMA4 = Jxz/GAMMA;
GAMMA5 = (Jz-Jx)/Jy;
GAMMA6 = Jxz/Jy;
GAMMA7 = ((Jx-Jy)*Jx+Jxz^2)/GAMMA;
GAMMA8 = Jx/GAMMA;

C_p_0 = GAMMA3*C_l_0 + GAMMA4*C_n_0;
C_p_beta = GAMMA3*C_l_beta + GAMMA4*C_n_beta;
C_p_p = GAMMA3*C_l_p + GAMMA4*C_n_p;
C_p_r = GAMMA3*C_l_r + GAMMA4*C_n_r;
C_p_deltaa = GAMMA3*C_l_deltaa + GAMMA4*C_n_deltaa;
C_p_deltar = GAMMA3*C_l_deltar + GAMMA4*C_n_deltar;
C_r_0 = GAMMA4*C_l_0 + GAMMA8*C_n_0;
C_r_beta = GAMMA4*C_l_beta + GAMMA8*C_n_beta;
C_r_p = GAMMA4*C_l_p + GAMMA8*C_n_p;
C_r_r = GAMMA4*C_l_r + GAMMA8*C_n_r;
C_r_deltaa = GAMMA4*C_l_deltaa + GAMMA8*C_n_deltaa;
C_r_deltar = GAMMA4*C_l_deltar + GAMMA8*C_n_deltar;

deltaa_deltar_vector = zeros(2,1);
deltaa_deltar_vector(1,1) = (-GAMMA1*pstar*qstar + GAMMA2*qstar*rstar)/(0.5*rho*Vastar^2*S*b) - C_p_0 - C_p_beta*betastar...
    - C_p_p*b*pstar/(2*Vastar) - C_p_r*b*rstar/(2*Vastar);
deltaa_deltar_vector(2,1) = (-GAMMA7*pstar*qstar + GAMMA1*qstar*rstar)/(0.5*rho*Vastar^2*S*b) - C_r_0 - C_r_beta*betastar...
    - C_r_p*b*pstar/(2*Vastar) - C_r_r*b*rstar/(2*Vastar);

deltaa_deltar = [C_p_deltaa C_p_deltar; C_r_deltaa C_r_deltar]\deltaa_deltar_vector;
deltaastar = deltaa_deltar(1,1);
deltarstar = deltaa_deltar(2,1);

%% Print out some useful information

uvwstar = [ustar; vstar; wstar]
pqrstar = [pstar; qstar; rstar]
psi_theta_phi_star_deg = [psistar; thetastar; phistar]*180/pi
alpha_beta_star_deg = [alphastar; betastar]*180/pi
delta_e_p_a_r_star_deg = [deltaestar; deltapstar*pi/180; deltaastar; deltarstar]*180/pi


%% Calculate A and B matricies for control design

% Longitudinal

X_u = ((ustar*rho*S)/(m))*(C_X + C_X_deltae*deltaestar) - (rho*S*wstar*C_X_alpha)/(2*m)...
    +(rho*S*c*C_X_q*ustar*qstar)/(4*m*Vastar) - (rho*S_prop*C_prop*ustar)/(m);
X_w = -qstar + ((wstar*rho*S)/(m))*(C_X + C_X_deltae*deltaestar) + (rho*S*c*C_X_q*wstar*qstar)/(4*m*Vastar)...
    + (rho*S*C_X_alpha*ustar)/(2*m) - (rho*S_prop*C_prop*wstar)/(m);
X_q = -wstar + (rho*Vastar*S*C_X_q*c)/(4*m);
X_deltae = (rho*Vastar^2*S*C_X_deltae)/(2*m);
X_deltap = (rho*S_prop*C_prop*K_motor^2*deltapstar)/(m);

Z_u = qstar + ((ustar*rho*S)/(m))*(C_Z + C_Z_deltae*deltaestar) - (rho*S*C_Z_alpha*wstar)/(2*m)...
    +(ustar*rho*S*C_Z_q*c*qstar)/(4*m*Vastar);
Z_w = ((wstar*rho*S)/(m))*(C_Z + C_Z_deltae*deltaestar) + (rho*S*C_Z_alpha*ustar)/(2*m)...
    +(rho*wstar*S*c*C_Z_q*qstar)/(4*m*Vastar);
Z_q = ustar + (rho*Vastar*S*C_Z_q*c)/(4*m);
Z_deltae = (rho*Vastar^2*S*C_Z_deltae)/(2*m);

M_u = ((ustar*rho*S*c)/(Jy))*(C_m_0 + C_m_alpha*alphastar + C_m_deltae*deltaestar) - (rho*S*c*C_m_alpha*wstar)/(2*Jy)...
    + (rho*S*c^2*C_m_q*qstar*ustar)/(4*Jy*Vastar);
M_w = ((wstar*rho*S*c)/(Jy))*(C_m_0 + C_m_alpha*alphastar + C_m_deltae*deltaestar) + (rho*S*c*C_m_alpha*ustar)/(2*Jy)...
    + (rho*S*c^2*C_m_q*qstar*wstar)/(4*Jy*Vastar);
M_q = (rho*Vastar*S*c^2*C_m_q)/(4*Jy);
M_deltae = (rho*Vastar^2*S*c*C_m_deltae)/(2*Jy);

A_long = zeros(5,5);

A_long(1,1) = X_u;
A_long(1,2) = X_w;
A_long(1,3) = X_q;
A_long(1,4) = -g*cos(thetastar);
A_long(1,5) = 0;
A_long(2,1) = Z_u;
A_long(2,2) = Z_w;
A_long(2,3) = Z_q;
A_long(2,4) = -g*sin(thetastar);
A_long(2,5) = 0;
A_long(3,1) = M_u;
A_long(3,2) = M_w;
A_long(3,3) = M_q;
A_long(3,4) = 0;
A_long(3,5) = 0;
A_long(4,1) = 0;
A_long(4,2) = 0;
A_long(4,3) = 1;
A_long(4,4) = 0;
A_long(4,5) = 0;
A_long(5,1) = -sin(thetastar);
A_long(5,2) = cos(thetastar);
A_long(5,3) = 0;
A_long(5,4) = -ustar*cos(thetastar)+wstar*sin(thetastar);
A_long(5,5) = 0;

B_long = zeros(5,2);

B_long(1,1) = X_deltae;
B_long(1,2) = X_deltap;
B_long(2,1) = Z_deltae;
B_long(2,2) = 0;
B_long(3,1) = M_deltae;
B_long(3,2) = 0;
B_long(4,1) = 0;
B_long(4,2) = 0;
B_long(5,1) = 0;
B_long(5,2) = 0;

A_long
B_long
eig_A_long = eig(A_long)

% Lateral

Y_v = ((rho*S*b*vstar)/(4*m*Vastar))*(C_Y_p*pstar + C_Y_r*rstar)...
    + ((rho*S*vstar)/m)*(C_Y_0 + C_Y_beta*betastar + C_Y_deltaa*deltaastar + C_Y_deltar*deltarstar)...
    + ((rho*S*C_Y_beta)/(2*m))*sqrt(ustar^2+wstar^2);
Y_p = wstar + ((rho*Vastar*S*b)/(4*m))*C_Y_p;
Y_r = -ustar + ((rho*Vastar*S*b)/(4*m))*C_Y_r;
Y_deltaa = ((rho*Vastar^2*S)/(2*m))*C_Y_deltaa;
Y_deltar = ((rho*Vastar^2*S)/(2*m))*C_Y_deltar;

L_v = ((rho*S*b^2*vstar)/(4*Vastar))*(C_p_p*pstar + C_p_r*rstar)...
    + (rho*S*b*vstar)*(C_p_0 + C_p_beta*betastar + C_p_deltaa*deltaastar + C_p_deltar*deltarstar)...
    + ((rho*S*b*C_p_beta)/(2))*sqrt(ustar^2+wstar^2);
L_p = GAMMA1*qstar + ((rho*Vastar*S*b^2)/(4))*C_p_p;
L_r = -GAMMA2*qstar + ((rho*Vastar*S*b^2)/(4))*C_p_r;
L_deltaa = ((rho*Vastar^2*S*b)/(2))*C_p_deltaa;
L_deltar = ((rho*Vastar^2*S*b)/(2))*C_p_deltar;

N_v = ((rho*S*b^2*vstar)/(4*Vastar))*(C_r_p*pstar + C_r_r*rstar)...
    + (rho*S*b*vstar)*(C_r_0 + C_r_beta*betastar + C_r_deltaa*deltaastar + C_r_deltar*deltarstar)...
    + ((rho*S*b*C_r_beta)/(2))*sqrt(ustar^2+wstar^2);
N_p = GAMMA7*qstar + ((rho*Vastar*S*b^2)/(4))*C_r_p;
N_r = -GAMMA1*qstar + ((rho*Vastar*S*b^2)/(4))*C_r_r;
N_deltaa = ((rho*Vastar^2*S*b)/(2))*C_r_deltaa;
N_deltar = ((rho*Vastar^2*S*b)/(2))*C_r_deltar;

A_lat = zeros(5,5);

A_lat(1,1) = Y_v;
A_lat(1,2) = Y_p;
A_lat(1,3) = Y_r;
A_lat(1,4) = g*cos(thetastar)*cos(phistar);
A_lat(1,5) = 0;
A_lat(2,1) = L_v;
A_lat(2,2) = L_p;
A_lat(2,3) = L_r;
A_lat(2,4) = 0;
A_lat(2,5) = 0;
A_lat(3,1) = N_v;
A_lat(3,2) = N_p;
A_lat(3,3) = N_r;
A_lat(3,4) = 0;
A_lat(3,5) = 0;
A_lat(4,1) = 0;
A_lat(4,2) = 1;
A_lat(4,3) = cos(phistar)*tan(thetastar);
A_lat(4,4) = qstar*cos(phistar)*tan(thetastar) - rstar*sin(phistar)*tan(thetastar);
A_lat(4,5) = 0;
A_lat(5,1) = 0;
A_lat(5,2) = 0;
A_lat(5,3) = cos(phistar)/cos(thetastar);
A_lat(5,4) = (pstar*cos(phistar)-rstar*sin(phistar))/cos(thetastar);
A_lat(5,5) = 0;

B_lat = zeros(5,2);

B_lat(1,1) = Y_deltaa;
B_lat(1,2) = Y_deltar;
B_lat(2,1) = L_deltaa;
B_lat(2,2) = L_deltar;
B_lat(3,1) = N_deltaa;
B_lat(3,2) = N_deltar;
B_lat(4,1) = 0;
B_lat(4,2) = 0;
B_lat(5,1) = 0;
B_lat(5,2) = 0;

A_lat
B_lat
eig_A_lat = eig(A_lat)


%% Control for Simulink - Lateral

delta_a_sat = 20;
roll_error_max = 15;
kp_phi = abs(delta_a_sat)/abs(roll_error_max);

roll_tf_num_const = B_lat(2,1);
roll_tf_denom_const = -A_lat(2,2);

omega_n_phi = sqrt(roll_tf_num_const*kp_phi);
zeta_phi = 0.8;

kd_phi = (2*zeta_phi*omega_n_phi - roll_tf_denom_const)/roll_tf_num_const;

omega_n_chi = omega_n_phi/10;

ki_chi = omega_n_chi^2/(g/Vastar);
zeta_chi = 0.8;
kp_chi = (2*zeta_chi*omega_n_chi)/(g/Vastar);

a_beta = A_lat(1,1);
b_beta = A_lat(1,3);
c_beta = A_lat(3,1);
d_beta = A_lat(3,3);
e_beta = B_lat(1,2);
f_beta = B_lat(3,2);

rudder_sat = 20;
max_sideslip = 10;
kp_beta = abs(rudder_sat)/abs(max_sideslip);
zeta_beta = 0.4;

solver_options = optimoptions('fsolve', 'Display', 'iter', ...
	'Algorithm', 'levenberg-marquardt', 'FunctionTolerance', 1e-5);
x = fsolve((@(x) findkd(x, a_beta, b_beta, c_beta, d_beta, e_beta, f_beta, Vastar, kp_beta, zeta_beta)), [12.7; 1.194], solver_options);

omega_n_beta = x(1);
kd_beta = x(2);

chi_ref = 0;
roll_des = phistar;
yawrate_des = rstar;


sideslip_tf_num_s = e_beta/Vastar;
sideslip_tf_num_const = (-e_beta*d_beta + b_beta*f_beta)/Vastar;
sideslip_tf_denom_s = -(a_beta + d_beta);
sideslip_tf_denom_const = a_beta*d_beta - b_beta*c_beta;


%% Control for simulink - Longitudinal

close all

deltapz_ref = 0;
deltatheta_ref = 0;
deltau_ref = 0;

deltaxlong0 = [10; 5; 1; 25*pi/180; 100];


Q_long = blkdiag(1E-4,1E-4,1E-5,1E-3,1E-6);
R_long = blkdiag(1,1);
C_long = eye(5);
D_long = 0;

A_long = A_long(1:4, 1:4);
B_long = B_long(1:4, :);
Q_long = Q_long(1:4, 1:4);
C_long = eye(4);
deltaxlong0 = deltaxlong0(1:4);

t_long = 0:0.01:20;

[Klong, Slong, Elong] = lqr(A_long, B_long, Q_long, R_long);
syslong = ss(A_long-B_long*Klong,B_long, C_long, D_long);
[ylong, tlong, slong] = lsim(syslong, zeros(length(t_long),2), t_long,deltaxlong0);
ulong = Klong*ylong';
ulong = ulong';

subplot(4,1,1)
plot(tlong, ylong(:,1),'linewidth',2)
ylabel('u (m/s)')
hold on
plot(tlong, ylong(:,1)+ustar,'r','linewidth',2)
legend('\Delta u', 'u')
subplot(4,1,2)
plot(tlong, ylong(:,2),'linewidth',2)
ylabel('w (m/s)')
hold on
plot(tlong, ylong(:,2)+wstar,'r','linewidth',2)
legend('\Delta w', 'w')
subplot(4,1,3)
plot(tlong, ylong(:,3),'linewidth',2)
ylabel('q (rad/s)')
hold on
plot(tlong, ylong(:,3)+qstar,'r','linewidth',2)
legend('\Delta q', 'q')
subplot(4,1,4)
plot(tlong, ylong(:,4)*180/pi,'linewidth',2)
ylabel('\theta (deg)')
hold on
plot(tlong, (ylong(:,4)+thetastar)*180/pi,'r','linewidth',2)
legend('\Delta \theta', '\theta')
xlabel('t (s)')
print(strcat(unique_identifier_string,'/Long_States'),'-dpng')
figure;


% subplot(3,1,1)
% plot(tlong, ylong(:,5),'linewidth',2)
% ylabel('p_z (m)')
% hold on
% plot(tlong, ylong(:,5)-100,'r','linewidth',2)
% legend('\Delta p_z','p_z')
subplot(2,1,1)
plot(tlong, ulong(:,1)*180/pi,'linewidth',2)
hold on
plot(tlong, (ulong(:,1)+deltaestar)*180/pi,'r','linewidth',2)
ylabel('\delta e (deg)')
legend('\Delta \delta e', '\delta e')
subplot(2,1,2)
plot(tlong, ulong(:,2),'linewidth',2)
hold on
plot(tlong, (ulong(:,2)+deltapstar),'r','linewidth',2)
ylabel('\delta p')
legend('\Delta \delta p', '\delta p')
xlabel('t (s)')
print(strcat(unique_identifier_string,'/Long_Controls'),'-dpng')

function settozero = findkd(x, a, b, c, d, e, f, Va, kp, zeta)
    omega_n = x(1);
    kd = x(2);
    settozero(1) = -2*zeta*omega_n + ((kd*(b*f - d*e)/Va) - d - a + e*kp/Va)/(e*kd/Va + 1);
    settozero(2) = -omega_n^2 + ((a*d - b*c + (kp*(b*f - d*e)/Va)))/(e*kd/Va + 1);
end
