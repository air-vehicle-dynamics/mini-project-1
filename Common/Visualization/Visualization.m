%% Displaying Flight Data 

% Load simulation parameters and results
dataloc = 'Simulation_With_Sensors_Data.mat';
sd = load(dataloc);
tdata = sd.to_sensor;

% Convert .mat to .csv
% csv_name = 'Sim_Sensors_Data.csv';
% csvwrite(csv_name, sd.to_sensor)
% 
% % Load sensor data 
% tdata = dlmread(csv_name,',',1,0);

% Organize
time   = tdata(:,1); % s
Px     = tdata(:,2); % m
Py     = tdata(:,3); % m
alt    = tdata(:,4); % m


yaw    = tdata(:,14); % rad
pitch  = tdata(:,15); % rad
roll   = tdata(:,16); % rad

% convert position to deg lat and long
% lets start at the seal
href = 168.0; % m above sea level
loc_start = [42.27385, -71.80981];
deg_clc_N = 0; % Angular direction of flat Earth x-axis (degrees clockwise from north)

pos_deg = zeros(length(time), 3);
for i = 1:length(time)
    pos_deg(i,:) = flat2lla([Px(i) Py(i) alt(i)+href], loc_start, deg_clc_N, href);    
end

ts = timeseries([pos_deg roll pitch yaw], time);

%% Open New FlightGear Animation Object 

h = Aero.FlightGearAnimation;

% Set flightgear object properties 
h.TimeseriesSourceType = 'Timeseries';
h.TimeseriesSource     = ts;

h.FlightGearBaseDirectory = 'C:\Program Files\FlightGear 2019.1.1';
h.FlightGearVersion = '2018.3';
h.GeometryModelName = 'c172p';
h.DestinationIpAddress = '127.0.0.1';
h.DestinationPort = '5502';

%% Initial Conditions


% instrument panel on
% load scenery/ground
h.InitialAltitude = -alt(1) + href;
h.InitialHeading  = deg_clc_N + yaw(1);
% h.InstallScenery = true;

get(h)


%% Create Run Script

GenerateRunScript(h)


%% Start FlightGear Simulator

system('runfg.bat &')


%% Play Back Flight Trajectory

play(h)


