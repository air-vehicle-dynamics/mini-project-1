function pathlength = DubinsCCC(p1, p2, psi1, psi2, Rmin, type, makeplots)


switch type
    case 'RLR'
        
        c1 = p1 + Rmin*[cos(psi1+pi/2); sin(psi1+pi/2);0];
        c2 = p2 + Rmin*[cos(psi2+pi/2); sin(psi2+pi/2);0];
        
        ell = c2 - c1;
        
        costheta = norm(ell)/(4*Rmin);
        if costheta > 1
            pathlength = 1E9;
            return
        end
        
        theta = acos(costheta);
        
        %ellhat = ell/norm(ell);
        
        c3 = c1 + (2*Rmin/norm(ell))*[cos(theta) sin(theta) 0; -sin(theta) cos(theta) 0; 0 0 1]*ell;
        
        c2p2 = p2 - c2;
        c2c3 = c3 - c2;
        theta3 = mod(atan2(c2c3(1)*c2p2(2)-c2c3(2)*c2p2(1),c2c3(1)*c2p2(1)+c2c3(2)*c2p2(2)),2*pi);
        theta2 = pi - 2*theta;
        c1p1 = p1 - c1;
        c1c3 = c3 - c1;
        theta1 = mod(atan2(c1p1(1)*c1c3(2)-c1p1(2)*c1c3(1),c1p1(1)*c1c3(1)+c1p1(2)*c1c3(2)),2*pi);
              
        %theta1deg = theta1*180/pi;
        %theta2deg = theta2*180/pi;
        %theta3deg = theta3*180/pi;
        pathlength = Rmin*(theta1 + theta2 + theta3);
        
    case 'RLR2' 
        
        c1 = p1 + Rmin*[cos(psi1+pi/2); sin(psi1+pi/2);0];
        c2 = p2 + Rmin*[cos(psi2+pi/2); sin(psi2+pi/2);0];
        
        ell = c2 - c1;
        
        costheta = norm(ell)/(4*Rmin);
        if costheta > 1
            pathlength = 1E9;
            return
        end
        
        theta = acos(costheta);
        
        %ellhat = ell/norm(ell);
        
        c3 = c1 + (2*Rmin/norm(ell))*[cos(theta) -sin(theta) 0; sin(theta) cos(theta) 0; 0 0 1]*ell;
        
        c2p2 = p2 - c2;
        c2c3 = c3 - c2;
        theta3 = mod(atan2(c2c3(1)*c2p2(2)-c2c3(2)*c2p2(1),c2c3(1)*c2p2(1)+c2c3(2)*c2p2(2)),2*pi);
        theta2 = pi + 2*theta;
        c1p1 = p1 - c1;
        c1c3 = c3 - c1;
        theta1 = mod(atan2(c1p1(1)*c1c3(2)-c1p1(2)*c1c3(1),c1p1(1)*c1c3(1)+c1p1(2)*c1c3(2)),2*pi);
              
        %theta1deg = theta1*180/pi;
        %theta2deg = theta2*180/pi;
        %theta3deg = theta3*180/pi;
        pathlength = Rmin*(theta1 + theta2 + theta3);

       
    case 'LRL2'
        
        c1 = p1 + Rmin*[cos(psi1-pi/2); sin(psi1-pi/2);0];
        c2 = p2 + Rmin*[cos(psi2-pi/2); sin(psi2-pi/2);0];
        
        ell = c2 - c1;
        
        costheta = norm(ell)/(4*Rmin);
        if costheta > 1
            pathlength = 1E9;
            return
        end
        
        theta = acos(costheta);
        
        %ellhat = ell/norm(ell);
        
        c3 = c1 + (2*Rmin/norm(ell))*[cos(theta) sin(theta) 0; -sin(theta) cos(theta) 0; 0 0 1]*ell;
        
        c2p2 = p2 - c2;
        c2c3 = c3 - c2;
        theta3 = mod(atan2(c2p2(1)*c2c3(2)-c2p2(2)*c2c3(1),c2p2(1)*c2c3(1)+c2p2(2)*c2c3(2)),2*pi);
        theta2 = pi + 2*theta;
        c1p1 = p1 - c1;
        c1c3 = c3 - c1;
        theta1 = mod(atan2(c1c3(1)*c1p1(2)-c1c3(2)*c1p1(1),c1c3(1)*c1p1(1)+c1c3(2)*c1p1(2)),2*pi);
              
        %theta1deg = theta1*180/pi;
        %theta2deg = theta2*180/pi;
        %theta3deg = theta3*180/pi;
        pathlength = Rmin*(theta1 + theta2 + theta3);
        
    case 'LRL'
        
        c1 = p1 + Rmin*[cos(psi1-pi/2); sin(psi1-pi/2);0];
        c2 = p2 + Rmin*[cos(psi2-pi/2); sin(psi2-pi/2);0];
        
        ell = c2 - c1;
        
        costheta = norm(ell)/(4*Rmin);
        if costheta > 1
            pathlength = 1E9;
            return
        end
        
        theta = acos(costheta);
        
        %ellhat = ell/norm(ell);
        
        c3 = c1 + (2*Rmin/norm(ell))*[cos(theta) -sin(theta) 0; sin(theta) cos(theta) 0; 0 0 1]*ell;
        
        c2p2 = p2 - c2;
        c2c3 = c3 - c2;
        theta3 = mod(atan2(c2p2(1)*c2c3(2)-c2p2(2)*c2c3(1),c2p2(1)*c2c3(1)+c2p2(2)*c2c3(2)),2*pi);
        theta2 = pi - 2*theta;
        c1p1 = p1 - c1;
        c1c3 = c3 - c1;
        theta1 = mod(atan2(c1c3(1)*c1p1(2)-c1c3(2)*c1p1(1),c1c3(1)*c1p1(1)+c1c3(2)*c1p1(2)),2*pi);
              
        %theta1deg = theta1*180/pi;
        %theta2deg = theta2*180/pi;
        %theta3deg = theta3*180/pi;
        pathlength = Rmin*(theta1 + theta2 + theta3);
end


if makeplots == 1

    circlegeneratingtheta = 0:0.001:2*pi;

    if type(2) == 'L'
        initial1theta = psi1 - pi/2;
        final1theta = initial1theta + theta1;
        pathgeneratingtheta1 = initial1theta:0.01:final1theta;

        final2theta = psi2 - pi/2;
        initial2theta = final2theta - theta3;
        pathgeneratingtheta2 = initial2theta:0.01:final2theta;
    end
    
    if type(2) == 'R'
        initial1theta = psi1 + pi/2;
        final1theta = initial1theta - theta1;
        pathgeneratingtheta1 = initial1theta:-0.01:final1theta;
        
        final2theta = psi2 + pi/2;
        initial2theta = final2theta + theta3;
        pathgeneratingtheta2 = initial2theta:-0.01:final2theta;
    end

    c3x = c3(1) + Rmin*cos(circlegeneratingtheta);
    c3y = c3(2) + Rmin*sin(circlegeneratingtheta);

    tangency1x = c1(1) + Rmin*cos(final1theta);
    tangency1y = c1(2) + Rmin*sin(final1theta);

    tangency2x = c2(1) + Rmin*cos(initial2theta);
    tangency2y = c2(2) + Rmin*sin(initial2theta);

    [~, wheremin] = min(abs(vecnorm([c3x; c3y] - [tangency1x; tangency1y])));
    angletangency1 = circlegeneratingtheta(wheremin);

    [~, wheremin] = min(abs(vecnorm([c3x; c3y] - [tangency2x; tangency2y])));
    angletangency2 = circlegeneratingtheta(wheremin);

    if type(2) == 'L'
        if angletangency2 > angletangency1
            angletangency2 = angletangency2 - 2*pi;
        end

        pathgeneratingtheta3 = angletangency2:0.001:angletangency1;
    end
    
    if type(2) == 'R'
        if angletangency2 < angletangency1
            angletangency2 = angletangency2 + 2*pi;
        end

        pathgeneratingtheta3 = angletangency1:0.001:angletangency2;
    end

%    [themin, wheremin] = min(abs(c3x - tangency2x));
%    angletangency2 = circlegeneratingtheta(wheremin)
    c1x = c1(1) + Rmin*cos(circlegeneratingtheta);
    c1y = c1(2) + Rmin*sin(circlegeneratingtheta);
    c1xpath = c1(1) + Rmin*cos(pathgeneratingtheta1);
    c1ypath = c1(2) + Rmin*sin(pathgeneratingtheta1);

    c2x = c2(1) + Rmin*cos(circlegeneratingtheta);
    c2y = c2(2) + Rmin*sin(circlegeneratingtheta);
    c2xpath = c2(1) + Rmin*cos(pathgeneratingtheta2);
    c2ypath = c2(2) + Rmin*sin(pathgeneratingtheta2);


    c3xpath = c3(1) + Rmin*cos(pathgeneratingtheta3);
    c3ypath = c3(2) + Rmin*sin(pathgeneratingtheta3);



    plot(c1y, c1x,'k','linewidth',2,'DisplayName','Circle 1')
    hold on
    plot(c2y, c2x,'r','linewidth',2,'DisplayName','Circle 2')
    plot(c3y, c3x,'g','linewidth',2,'DisplayName','Circle 3')
    plot(c1ypath, c1xpath, 'b','linewidth',2,'DisplayName','Path')
    secondpath = plot(c2ypath, c2xpath, 'b','linewidth',2);
    thirdpath = plot(c3ypath, c3xpath, 'b','linewidth',2);
    hasbehavior(secondpath, 'legend', false)
    hasbehavior(thirdpath, 'legend', false)

    plot(p1(2), p1(1),'k.','markersize',25,'DisplayName','Start')
    plot(p2(2), p2(1),'r.','markersize',25,'DisplayName','End')

    axis equal
    xlabel('y')
    ylabel('x')
    legend('-dynamiclegend', 'location', 'EastOutside')
    title(strcat(type, ' Dubin''s Path  '))
end

end
