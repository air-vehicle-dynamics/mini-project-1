classdef ekf 
    
    % Sensors
    % 3 axis accelerometer
    % 3 axis rate gyro
    % GPS receiver
    % airspeed sensor
    % three axis magnetometer
    
    
    % y (measurement)
    % [GPS x pos
    %  GPS y pos
    %  GPS z pos
    %  GPS x vel
    %  GPS y vel
    %  GPS z vel
    %  MAG psi
    %  MAG theta
    %  MAG phi
    %  PITOT dP]
    
    % x (state)
    % [X pos
    %  Y pos
    %  Z pos
    %  X vel
    %  Y vel
    %  Z vel
    %  psi
    %  theta
    %  phi
    %  bias psi
    %  bias theta 
    %  bias phi
    %  bias accelerometer x
    %  bias accelerometer y 
    %  bias accelerometer z
    %  bias rate gyro x
    %  bias rate gyro y
    %  bias rate gyro z
    %  bias airspeed]
    
    properties
        P
        Q
        R
        xhat
        noise
        bias_est
        wind_x
        wind_y
        wind_z
        
        
    end
    
    methods 
        
        function f_ = f(self, x, state)
            
            e_thta = self.xhat(8);
            e_phi = self.xhat(9);
            
            Hinv	= (1 / cos(e_thta) ) * [0 sin(e_phi) cos(e_phi); ...
                        0 cos(e_phi)*cos(e_thta) -sin(e_phi)*cos(e_thta); ...
                        cos(e_thta) sin(e_phi)*sin(e_thta) cos(e_phi)*sin(e_thta)];
    
            f_ = [V_t;
                  R_b_t * (a_b - b_a - n_a) + g_t;
                  Hinv * (w_b - b_w - n_w);
                  zeros([9 1])];
        end
        
        
        function h_ = h(self, x, state)
            est_alt = self.xhat(3);
            
            v_x_gt = self.xhat(4);
            v_y_gt = self.xhat(5);
            v_z_gt = self.xhat(6);
            
            
            rho = atmosphere(est_alt);
            
            dP_rel = (rho/2) * ((v_x_gt - self.wind_x)^2 + (v_y_gt - self.wind_y)^2 + (v_z_gt - self.wind_z)^2);
            
            h_ = blkdiag(eye(9), dP_rel);
            
        end
        
        
        function self = ekf(sensor, sim, initial_state)
            % initialize sim, sensor parameters
            run(sensor_characteristics.m)
            % run(sim characteristics)
            
            self.Q = blkdiag(accel.noise_stdev^2 * eye(3), rate_gyro.noise_stdev^2 * eye(3));
            self.R = blkdiag(GPS.noise_stdev_pos_NE^2 * eye(2), ...
                             GPS.noise_stdev_pos_Z^2, ...
                             GPS.noise_stdev_vel_NE^2 * eye(2), ...
                             GPS.noise_stdev_vel_Z^2, ...
                             dig_comp.noise_stdev^2 * eye(3), ...
                             pressure.noise_stdev^2);
            self.P = blkdiag(self.R, 0*eye(9)); % zeros are for 9 biases
            
            self.xhat = initial_state;
            self.noise = zeros(7,1); % noise for accelerometers, rate gyros
            self.bias_est = zeros(7,1); % three accelerometers, three rate gyros, airspeed sensor
        end
        
        function state = update(self, state, measurement)
            self.model_step(state);                      % get the next timestep of the system model
            self.measurement_update(state, measurement); % update the model with the "new" measurement
            % literature updates phi and theta directly (ekf attitude
            % estimation) here
        end
        
        % get the next expected state based on the system model
        function self = model_step(self, state)
            % number of prediction steps per sample
            for i = 1:N
                % get next step
                self.xhat = self.xhat + (T_out/N) * self.f(state); % FIX
                
                % compute Jacobian
                A = self.jacobian(@self.f, self.xhat, state);
                B2 = self.jacobian(@self.f, self.noise, state);
                
                % update P matrix
                self.P = self.P + (T_out/N) * (A*self.P + self.P*A' + B2 * self.Q * B2');
            end
        end
        
        % measurement update
        function self = measurement_update(self, state, measurement)
            H = self.h(self.xhat, state);
            C = jacobian(@self.h, self.xhat, state);
            y = [measurement.GPSxpos; 
                 measurement.GPSypos;
                 measurement.GPSzpos;
                 measurement.GPSxvel;
                 measurement.GPSyvel;
                 measurement.GPSzvel;
                 measurement.MAGpsi;
                 measurement.MAGtheta;
                 measurement.MAGphi;
                 measurement.PITOTdP];
            
             for i = 1:length(y)
                 Ci = C(i);
                 Li = self.P*Ci' * inv(Ri + Ci*self.P*Ci');
                 self.P = (eye(size(self.P)) - Li*Ci)*self.P;
                 self.xhat = self.xhat + Li * (y(i) - H);
             end
        end
        
             
             
        function J = jacobian(self, fun, x, state)
            % compute jacobian of fun with respect to x
            f = fun(x, state);
            m = size(f, 1);
            n = size(x, 1);
            eps = 0.01;  % deviation
            J = zeros(m, n);
            for i=1:n
                x_eps = x;
                x_eps(i) = x_eps(i) + eps;
                f_eps = fun(x_eps, state);
                df = (f_eps - f) / eps;
                J(:, i) = df;
            end
        end

            
        
        function rho = atmosphere(alt)
            % alt in m
            h = alt;
            a1 = -6.5e-3; % K/m
            g = 9.81; % m/s/s
            R = 287; % J/(kg*K)
            
            T_sl = 288.16; % K
            rho_sl = 1.225; % kg/m^3
            
            
            if h <= 11000
                T = T_sl + a1 * h;
                rho = rho_sl * (T/T_sl)^(-1 - g/(a1*R));
            elseif h > 11000
                T11 = 216.66; % K
                rho11 = 0.3642; % kg / m^3
                
                rho = rho11*exp(-g*(h - 11000)/(R*T11));
            end
               
            
            
            
        end
        
            
            
    
    
    
    
    
end