function pathlength = DubinsCSC(p1, p2, psi1, psi2, Rmin, type, makeplots)

switch type
    case 'RSR'
        c1 = p1 + Rmin*[cos(psi1+pi/2); sin(psi1+pi/2);0];
        c2 = p2 + Rmin*[cos(psi2+pi/2); sin(psi2+pi/2);0];
        ell = c2 - c1;

        theta3 = atan2(ell(2), ell(1));

        theta1 = mod(2*pi + mod(theta3 - pi/2,2*pi) - mod(psi1-pi/2, 2*pi),2*pi);
        theta2 = mod(2*pi + mod(psi2 - pi/2, 2*pi) - mod(theta3 - pi/2,2*pi),2*pi);

        pathlength = norm(ell) + Rmin*(theta1 + theta2);
        
        initial_angle = mod(psi1 - pi/2,2*pi);
        leavecircle1angle = mod(theta3 - pi/2, 2*pi);
        if leavecircle1angle < initial_angle
            leavecircle1angle = leavecircle1angle + 2*pi;
        end
        entercircle2angle = mod(theta3 - pi/2, 2*pi);
        end_angle = mod(psi2 - pi/2,2*pi);
        if end_angle < entercircle2angle
            end_angle = end_angle + 2*pi;
        end
        
        pathoncircle1theta = initial_angle:0.01:leavecircle1angle;
        pathoncircle2theta = entercircle2angle:0.01:end_angle;

        
    case 'RSL'
        c1 = p1 + Rmin*[cos(psi1+pi/2); sin(psi1+pi/2);0];
        c2 = p2 + Rmin*[cos(psi2-pi/2); sin(psi2-pi/2);0];
        ell = c2 - c1;
        
        theta3prime = atan2(ell(2), ell(1));
        if 2*Rmin/norm(ell) > 1
            pathlength = 1E9;
            return
        end
        theta3 = theta3prime - pi/2 + asin(2*Rmin/norm(ell));
        
        theta1 = mod(2*pi + mod(theta3, 2*pi) - mod(psi1-pi/2,2*pi),2*pi);
        theta2 = mod(2*pi + mod(theta3+pi, 2*pi) - mod(psi2+pi/2,2*pi),2*pi);
        
        pathlength = sqrt(norm(ell)^2-4*Rmin^2) + Rmin*(theta1 + theta2);

        initial_angle = mod(psi1 - pi/2, 2*pi);
        leavecircle1angle = mod(theta3, 2*pi);
        if leavecircle1angle < initial_angle
            leavecircle1angle = leavecircle1angle + 2*pi;
        end
        entercircle2angle = mod(theta3 + pi, 2*pi);
        end_angle = mod(psi2 + pi/2,2*pi);
        if entercircle2angle < end_angle
            entercircle2angle = entercircle2angle + 2*pi;
        end
        
        pathoncircle1theta = initial_angle:0.01:leavecircle1angle;
        pathoncircle2theta = end_angle:0.01:entercircle2angle;        
        
    case 'LSR'
        
        c1 = p1 + Rmin*[cos(psi1-pi/2); sin(psi1-pi/2);0];
        c2 = p2 + Rmin*[cos(psi2+pi/2); sin(psi2+pi/2);0];
        ell = c2 - c1;
        
        theta3prime = atan2(ell(2), ell(1));
        if 2*Rmin/norm(ell) > 1
            pathlength = 1E9;
            return
        end
        theta3 = acos(2*Rmin/norm(ell));
        
        theta1 = mod(2*pi + mod(psi1+pi/2,2*pi) - mod(theta3prime + theta3,2*pi),2*pi);
        theta2 = mod(2*pi + mod(psi2-pi/2,2*pi) - mod(theta3prime + theta3 - pi,2*pi),2*pi);
        
        pathlength = sqrt(norm(ell)^2 - 4*Rmin^2) + Rmin*(theta1 + theta2);
        
        initial_angle = mod(psi1 + pi/2,2*pi);
        leavecircle1angle = mod(theta3prime + theta3,2*pi);
        if leavecircle1angle > initial_angle
            initial_angle = initial_angle + 2*pi;
        end
        entercircle2angle = mod(theta3prime + theta3 - pi , 2*pi);
        end_angle = mod(psi2 - pi/2, 2*pi);
        if end_angle < entercircle2angle
            end_angle = end_angle + 2*pi;
        end
        pathoncircle1theta = leavecircle1angle:0.01:initial_angle;
        pathoncircle2theta = entercircle2angle:0.01:end_angle;
        
        
    case 'LSL'
        
        c1 = p1 + Rmin*[cos(psi1-pi/2); sin(psi1-pi/2);0];
        c2 = p2 + Rmin*[cos(psi2-pi/2); sin(psi2-pi/2);0];
        ell = c2 - c1;
        
        theta3 = atan2(ell(2), ell(1));
        
        theta1 = mod(2*pi + mod(psi1 + pi/2, 2*pi) - mod(theta3 + pi/2, 2*pi),2*pi);
        theta2 = mod(2*pi + mod(theta3 + pi/2, 2*pi) - mod(psi2 + pi/2,2*pi),2*pi);
        
        pathlength = norm(ell) + Rmin*(theta1 + theta2);
        
        initial_angle = mod(psi1 + pi/2, 2*pi);
        leavecircle1angle = mod(theta3 + pi/2,2*pi);
        if leavecircle1angle > initial_angle
            initial_angle = initial_angle + 2*pi;
        end
        entercircle2angle = mod(theta3 + pi/2,2*pi);
        end_angle = mod(psi2 + pi/2, 2*pi);
        if entercircle2angle < end_angle
            entercircle2angle = entercircle2angle + 2*pi;
        end
        
        pathoncircle1theta = leavecircle1angle:0.01:initial_angle;
        pathoncircle2theta = end_angle:0.01:entercircle2angle;


end

if makeplots == 1
   anglearray = 0:0.01:2*pi;
   
   circle1x = c1(1) + Rmin*cos(anglearray);
   circle1y = c1(2) + Rmin*sin(anglearray);
   plot(p1(2), p1(1), 'k.', 'DisplayName', 'Starting Point','markersize',25)
   hold on
   plot(circle1y, circle1x, 'k-', 'DisplayName', 'Starting Circle','linewidth',2)
   
   circle2x = c2(1) + Rmin*cos(anglearray);
   circle2y = c2(2) + Rmin*sin(anglearray);
   
   plot(p2(2), p2(1), 'r.', 'DisplayName', 'Ending Point','markersize',25)
   plot(circle2y, circle2x, 'r-', 'Displayname', 'Ending Circle','linewidth',2)
   
   
   leavecircle1x = c1(1) + Rmin*cos(leavecircle1angle);
   leavecircle1y = c1(2) + Rmin*sin(leavecircle1angle);
   
   
   entercircle2x = c2(1) + Rmin*cos(entercircle2angle);
   entercircle2y = c2(2) + Rmin*sin(entercircle2angle);
   
   
   plot([leavecircle1y; entercircle2y], [leavecircle1x; entercircle2x], 'blue-o', 'DisplayName', 'Path','linewidth',2,'markersize',5)
   
   
   pathoncircle1x = c1(1) + Rmin*cos(pathoncircle1theta);
   pathoncircle1y = c1(2) + Rmin*sin(pathoncircle1theta);
   firstcirclepath = plot(pathoncircle1y, pathoncircle1x, 'blue-', 'linewidth', 2);
   hasbehavior(firstcirclepath, 'legend', false)
   
   pathoncircle2x = c2(1) + Rmin*cos(pathoncircle2theta);
   pathoncircle2y = c2(2) + Rmin*sin(pathoncircle2theta);
   secondcirclepath = plot(pathoncircle2y, pathoncircle2x, 'blue-', 'linewidth', 2, 'DisplayName', '');
   hasbehavior(secondcirclepath, 'legend', false)
   
   axis equal
   xlabel('y')
   ylabel('x')
   legend('-dynamiclegend', 'location', 'EastOutside')
   title(strcat(type, ' Dubin''s Path  '))
end
        
end
