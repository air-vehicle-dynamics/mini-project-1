function [pos, posdot, posddot, posdddot, posddddot] = gen_traj_dubins()

t1 = 0:0.005:1.605;
t2 = 1.610:0.005:69.425;
t3 = 69.430:0.005:71.035;

pos_x_1 = 20*sin(0.5*t1);
pos_x_dot_1 = 10*cos(0.5*t1);
pos_x_ddot_1 = -5*sin(0.5*t1);
pos_x_dddot_1 = -2.5*cos(0.5*t1);
pos_x_ddddot_1 = 1.25*sin(0.5*t1);

pos_y_1 = 20+20*cos(0.5*t1 - pi);
pos_y_dot_1 = -10*sin(0.5*t1 - pi);
pos_y_ddot_1 = -5*cos(0.5*t1 - pi);
pos_y_dddot_1 = 2.5*sin(0.5*t1 - pi);
pos_y_ddddot_1 = 1.25*cos(0.5*t1 - pi);

pos_z_1 = -100*ones(size(t1));
pos_z_dot_1 = 0*ones(size(t1));
pos_z_ddot_1 = 0*ones(size(t1));
pos_z_dddot_1 = 0*ones(size(t1));
pos_z_ddddot_1 = 0*ones(size(t1));

pos_x_2 = 14.3842 + 6.94793*(t2-1.60533);
pos_x_dot_2 = 6.94793*ones(size(t2));
pos_x_ddot_2 = 0*ones(size(t2));
pos_x_dddot_2 = 0*ones(size(t2));
pos_x_ddddot_2 = 0*ones(size(t2));

pos_y_2 = 6.1041 + 7.192095*(t2 - 1.60533);
pos_y_dot_2 = 7.192095*ones(size(t2));
pos_y_ddot_2 = 0*ones(size(t2));
pos_y_dddot_2 = 0*ones(size(t2));
pos_y_ddddot_2 = 0*ones(size(t2));

pos_z_2 = -100*ones(size(t2));
pos_z_dot_2 = 0*ones(size(t2));
pos_z_ddot_2 = 0*ones(size(t2));
pos_z_dddot_2 = 0*ones(size(t2));
pos_z_ddddot_2 = 0*ones(size(t2));

pos_x_3 = 500 + 20*sin(0.5*(t3 - 71.03396));
pos_x_dot_3 = 10*cos(0.5*(t3 - 71.03396));
pos_x_ddot_3 = -5*sin(0.5*(t3 - 71.03396));
pos_x_dddot_3 = -2.5*cos(0.5*(t3 - 71.03396));
pos_x_ddddot_3 = 1.25*sin(0.5*(t3 - 71.03396));

pos_y_3 = 480 + 20*cos(0.5*(t3 - 71.03396));
pos_y_dot_3 = -10*sin(0.5*(t3 - 71.03396));
pos_y_ddot_3 = -5*cos(0.5*(t3 - 71.03396));
pos_y_dddot_3 = 2.5*sin(0.5*(t3 - 71.03396));
pos_y_ddddot_3 = 1.25*cos(0.5*(t3 - 71.03396));

pos_z_3 = -100*ones(size((t3)));
pos_z_dot_3 = 0*ones(size((t3)));
pos_z_ddot_3 = 0*ones(size((t3)));
pos_z_dddot_3 = 0*ones(size((t3)));
pos_z_ddddot_3 = 0*ones(size((t3)));

pos = [pos_x_1 pos_x_2 pos_x_3; pos_y_1 pos_y_2 pos_y_3; pos_z_1 pos_z_2 pos_z_3];
posdot = [pos_x_dot_1 pos_x_dot_2 pos_x_dot_3; pos_y_dot_1 pos_y_dot_2 pos_y_dot_3; pos_z_dot_1 pos_z_dot_2 pos_z_dot_3];
posddot = [pos_x_ddot_1 pos_x_ddot_2 pos_x_ddot_3; pos_y_ddot_1 pos_y_ddot_2 pos_y_ddot_3; pos_z_ddot_1 pos_z_ddot_2 pos_z_ddot_3];
posdddot = [pos_x_dddot_1 pos_x_dddot_2 pos_x_dddot_3; pos_y_dddot_1 pos_y_dddot_2 pos_y_dddot_3; pos_z_dddot_1 pos_z_dddot_2 pos_z_dddot_3];
posddddot = [pos_x_ddddot_1 pos_x_ddddot_2 pos_x_ddddot_3; pos_y_ddddot_1 pos_y_ddddot_2 pos_y_ddddot_3; pos_z_ddddot_1 pos_z_ddddot_2 pos_z_ddddot_3];

end