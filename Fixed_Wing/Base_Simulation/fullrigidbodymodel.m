function xdot = fullrigidbodymodel(~,x,control)

global J m W g GAMMA GAMMA1 GAMMA2 GAMMA3 GAMMA4 GAMMA5 GAMMA6 GAMMA7 GAMMA8
global S b c S_prop rho K_motor K_TP K_omega e_oswald Aspect
global C_L_0 C_D_0 C_m_0 C_L_alpha C_D_alpha C_m_alpha C_L_q C_D_q C_m_q C_L_deltae C_D_deltae C_m_deltae C_prop 
global M_Stall alpha_0 epsilon C_D_p
global C_Y_0 C_l_0 C_n_0 C_Y_beta C_l_beta C_n_beta C_Y_p C_l_p C_n_p C_Y_r C_l_r C_n_r 
global C_Y_deltaa C_l_deltaa C_n_deltaa C_Y_deltar C_l_deltar C_n_deltar


% Moment of inertia terms
Jxz = -J(3,1);
Jx = J(1,1);
Jy = J(2,2);
Jz = J(3,3);
GAMMA = Jx*Jz - Jxz^2;
GAMMA1 = (Jxz*(Jx - Jy + Jz))/GAMMA;
GAMMA2 = (Jz*(Jz-Jy)+Jxz^2)/GAMMA;
GAMMA3 = Jz/GAMMA;
GAMMA4 = Jxz/GAMMA;
GAMMA5 = (Jz-Jx)/Jy;
GAMMA6 = Jxz/Jy;
GAMMA7 = ((Jx-Jy)*Jx+Jxz^2)/GAMMA;
GAMMA8 = Jx/GAMMA;

%{
State Definition
x1-x3 p^t - position in tangent frame
x4-x6 v_g^t - ground-relative velocity in tangent frame
x7-x9 v^b - air-relative velocity in body frame
x10 V - airspeed
x11 alpha - Angle of attack
x12 beta - Angle of sideslip
x13 psi - Yaw angle
x14 theta - Pitch angle
x15 phi - Roll angle
x16-x19 e - Quaternions
x20-22 omega_tb^b Angluar velocity in body coordinates
%}


xdot = zeros(22,1);

p_x = x(1); py = x(2); pz = x(3);
v_gx = x(4); v_gy = x(5); v_gz = x(6);
u = x(7); v = x(8); w = x(9);
Va = x(10); alpha = x(11); beta = x(12);
psi = x(13); theta = x(14); phi = x(15);
e0 = x(16); e1 = x(17); e2 = x(18); e3 = x(19);
p = x(20); q = x(21); r = x(22);


deltae = control(1,1); 
deltap = control(2,1); 
deltaa = control(3,1); 
deltar = control(4,1);
% Control saturation
if abs(deltae) > 20
    deltae = 20*sign(deltae);
end
if abs(deltaa) > 20
    deltaa = 20*sign(deltaa);
end
if abs(deltar) > 20
    deltar = 20*sign(deltar);
end
if deltap < 0
    deltap = 0;
end
if deltap > 1
    deltap = 1;
end

% Body to tangent rotation
R_b_t = zeros(3,3);
R_b_t(1,1) = (cos(theta)*cos(psi));
R_b_t(1,2) = (sin(phi)*sin(theta)*cos(psi) - cos(phi)*sin(psi));
R_b_t(1,3) = (cos(phi)*sin(theta)*cos(psi) + sin(phi)*sin(psi));
R_b_t(2,1) = (cos(theta)*sin(psi));
R_b_t(2,2) = (sin(phi)*sin(theta)*sin(psi) + cos(phi)*cos(psi));
R_b_t(2,3) = (cos(phi)*sin(theta)*sin(psi) - sin(phi)*cos(psi));
R_b_t(3,1) = -(sin(theta));
R_b_t(3,2) = (sin(phi)*cos(theta));
R_b_t(3,3) = (cos(phi)*cos(theta));

% H 3-2-1 in body coordinates
H_321 = zeros(3,3);
H(1,1) = -sin(theta);
H(1,2) = 0;
H(1,3) = 1;
H(2,1) = sin(phi)*cos(theta);
H(2,2) = cos(phi);
H(2,3) = 0;
H(3,1) = cos(phi)*cos(theta);
H(3,2) = -sin(phi);
H(3,3) = 0;

% Body to wind rotation
R_b_w = zeros(3,3);
R_b_w(1,1) = cos(beta)*cos(alpha);
R_b_w(1,2) = sin(beta);
R_b_w(1,3) = cos(beta)*sin(alpha);
R_b_w(2,1) = -sin(beta)*cos(alpha);
R_b_w(2,2) = cos(beta);
R_b_w(2,3) = -sin(beta)*sin(alpha);
R_b_w(3,1) = -sin(alpha);
R_b_w(3,2) = 0;
R_b_w(3,3) = cos(alpha);

% Lift and Drag Coefficients in x and z form
C_L = C_L_0 + C_L_alpha*alpha;
C_D = C_D_p + C_L^2/(pi*Aspect*e_oswald);
C_X = -C_D*cos(alpha) + C_L*sin(alpha);
C_X_q = -C_D_q*cos(alpha) + C_L_q*sin(alpha);
C_X_deltae = -C_D_deltae*cos(alpha) + C_L_deltae*sin(alpha);
C_Z = -C_D*sin(alpha)-C_L*cos(alpha);
C_Z_q = -C_D_q*sin(alpha)-C_L_q*cos(alpha);
C_Z_deltae = -C_D_deltae*sin(alpha) - C_L_deltae*cos(alpha);

% Moment coefficients in p and r form
C_p_0 = GAMMA3*C_l_0 + GAMMA4*C_n_0;
C_p_beta = GAMMA3*C_l_beta + GAMMA4*C_n_beta;
C_p_p = GAMMA3*C_l_p + GAMMA4*C_n_p;
C_p_r = GAMMA3*C_l_r + GAMMA4*C_n_r;
C_p_deltaa = GAMMA3*C_l_deltaa + GAMMA4*C_n_deltaa;
C_p_deltar = GAMMA3*C_l_deltar + GAMMA4*C_n_deltar;
C_r_0 = GAMMA4*C_l_0 + GAMMA8*C_n_0;
C_r_beta = GAMMA4*C_l_beta + GAMMA8*C_n_beta;
C_r_p = GAMMA4*C_l_p + GAMMA8*C_n_p;
C_r_r = GAMMA4*C_l_r + GAMMA8*C_n_r;
C_r_deltaa = GAMMA4*C_l_deltaa + GAMMA8*C_n_deltaa;
C_r_deltar = GAMMA4*C_l_deltar + GAMMA8*C_n_deltar;


% Pdot
xdot(1,1) = u*cos(theta)*cos(psi) + v*(sin(phi)*sin(theta)*cos(psi) - cos(phi)*sin(psi))...
    + w*(cos(phi)*sin(theta)*cos(psi) + sin(phi)*sin(psi)) + W(1,1);
xdot(2,1) = u*cos(theta)*sin(psi) + v*(sin(phi)*sin(theta)*sin(psi) + cos(phi)*cos(psi))...
    + w*(cos(phi)*sin(theta)*sin(psi) - sin(phi)*cos(psi)) + W(2,1);
xdot(3,1) = -u*sin(theta) + v*sin(phi)*cos(theta) + w*cos(phi)*cos(theta) + W(3,1);


% Define fores in body frame
Xoverm = (rho*Va^2*S/(2*m))*(C_X + C_X_q*c*q/(2*Va)...
    + C_X_deltae*deltae) + (rho*S_prop*C_prop/(2*m))*((K_motor*deltap)^2 - Va^2);
Yoverm = (rho*Va^2*S/(2*m))*(C_Y_0 + C_Y_beta*beta...
    + C_Y_p*b*p/(2*Va) + C_Y_r*b*r/(2*Va) + C_Y_deltaa*deltaa + C_Y_deltar*deltar);
Zoverm = (rho*Va^2*S/(2*m))*(C_Z...
    + C_Z_q*c*q/(2*Va) + C_Z_deltae*deltae);

% VGdot = g + (1/m)*R_b_t*XYZ

xdot(4:6,1) = [0; 0; g] + R_b_t*[Xoverm; Yoverm; Zoverm];

% Not sure about this one, need to check with Cowlagi


% V^bdot
xdot(7,1) = r*(v + W(2)) - q*(w + W(3)) - g*sin(theta) + (rho*Va^2*S/(2*m))*(C_X + C_X_q*c*q/(2*Va)...
    + C_X_deltae*deltae) + (rho*S_prop*C_prop/(2*m))*((K_motor*deltap)^2 - Va^2);
xdot(8,1) = p*(w + W(3)) - r*(u + W(1)) + g*cos(theta)*sin(phi) + (rho*Va^2*S/(2*m))*(C_Y_0 + C_Y_beta*beta...
    + C_Y_p*b*p/(2*Va) + C_Y_r*b*r/(2*Va) + C_Y_deltaa*deltaa + C_Y_deltar*deltar);
xdot(9,1) = q*(u + W(1)) - p*(v + W(2)) + g*cos(theta)*cos(phi) + (rho*Va^2*S/(2*m))*(C_Z...
    + C_Z_q*c*q/(2*Va) + C_Z_deltae*deltae);

% Airspeed, alpha, beta dot (chain rule)
xdot(10,1) = (u*xdot(7,1) + v*xdot(8,1) + w*xdot(9,1))/Va;
xdot(11,1) = (u*xdot(9,1) - xdot(7,1)*w)/(u^2+w^2);
xdot(12,1) = (Va*xdot(8,1) - v*xdot(10,1))/(Va^2*sqrt(1-(v/Va)^2));
% Not sure about this one, need to check with Cowlagi (chain rule?)

% Euler angle dot
xdot(13,1) = (sin(phi)*q + cos(phi)*r)/cos(theta);
xdot(14,1) = cos(phi)*q - sin(phi)*r;
xdot(15,1) = p + sin(phi)*tan(theta)*q + cos(phi)*tan(theta)*r;

% Quaternions dot
xdot(16,1) = 0.5*(-p*e1 - q*e2 - r*e3);
xdot(17,1) = 0.5*(p*e0 + r*e2 - q*e3);
xdot(18,1) = 0.5*(q*e0 - r*e1 + p*e3);
xdot(19,1) = 0.5*(r*e0 + q*e1 - p*e2);


% Angular velocity dot
Jinv = inv(J);
xdot(20,1) = GAMMA1*p*q - GAMMA2*q*r + 0.5*rho*Va^2*S*b*(C_p_0 + C_p_beta*beta + C_p_p*b*p/(2*Va)...
    + C_p_r*b*r/(2*Va) + C_p_deltaa*deltaa + C_p_deltar*deltar);
xdot(21,1) = GAMMA5*p*r - GAMMA6*(p^2-r^2) + (rho*Va^2*S*c/(2*Jy))*(C_m_0 + C_m_alpha*alpha...
    + C_m_q*c*q/(2*Va) + C_m_deltae*deltae);
xdot(22,1) = GAMMA7*p*q - GAMMA1*q*r + 0.5*rho*Va^2*S*b*(C_r_0 + C_r_beta*beta + C_r_p*b*p/(2*Va)...
    + C_r_r*b*r/(2*Va) + C_r_deltaa*deltaa + C_r_deltar*deltar);

end



