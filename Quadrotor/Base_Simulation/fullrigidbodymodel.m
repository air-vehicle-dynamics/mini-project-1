function xdot = fullrigidbodymodel(~,x,control)

global J m K_F K_M K_m W g L rho CD0_S


% Moment of inertia terms
Jx = J(1,1);
Jy = J(2,2);
Jz = J(3,3);
GAMMA = Jx*Jz;
GAMMA1 = 0;
GAMMA2 = (Jz*(Jz-Jy))/GAMMA;
GAMMA3 = Jz/GAMMA;
GAMMA4 = 0;
GAMMA5 = (Jz-Jx)/Jy;
GAMMA6 = 0;
GAMMA7 = ((Jx-Jy)*Jx)/GAMMA;
GAMMA8 = Jx/GAMMA;

%{
State Definition
x1-x3 p^t - position in tangent frame
x4-x6 v_g^t - ground-relative velocity in tangent frame
x7-x9 v^b - air-relative velocity in body frame
x10 V - airspeed
x11 alpha - Angle of attack
x12 beta - Angle of sideslip
x13 psi - Yaw angle
x14 theta - Pitch angle
x15 phi - Roll angle
x16-x19 e - Quaternions
x20-22 omega_tb^b Angluar velocity in body coordinates
%}


xdot = zeros(22,1);

p_x = x(1); py = x(2); pz = x(3);
v_gx = x(4); v_gy = x(5); v_gz = x(6);
u = x(7); v = x(8); w = x(9);
psi = x(10); theta = x(11); phi = x(12);
e0 = x(13); e1 = x(14); e2 = x(15); e3 = x(16);
p = x(17); q = x(18); r = x(19);

Va = norm([u; v; w]);
alpha = atan2(w,u);
beta = asin(v/Va);
if(Va) == 0
    beta = 0;
end

omega1 = control(1,1); 
omega2 = control(2,1); 
omega3 = control(3,1); 
omega4 = control(4,1);

% Control saturation
if abs(omega1) > 9000
    omega1 = 9000;
end
if abs(omega2) > 9000
    omega2 = 9000;
end
if abs(omega3) > 9000
    omega3 = 9000;
end
if abs(omega4) > 9000
    omega4 = 9000;
end

% Body to tangent rotation
R_b_t = zeros(3,3);
R_b_t(1,1) = (cos(theta)*cos(psi));
R_b_t(1,2) = (sin(phi)*sin(theta)*cos(psi) - cos(phi)*sin(psi));
R_b_t(1,3) = (cos(phi)*sin(theta)*cos(psi) + sin(phi)*sin(psi));
R_b_t(2,1) = (cos(theta)*sin(psi));
R_b_t(2,2) = (sin(phi)*sin(theta)*sin(psi) + cos(phi)*cos(psi));
R_b_t(2,3) = (cos(phi)*sin(theta)*sin(psi) - sin(phi)*cos(psi));
R_b_t(3,1) = -(sin(theta));
R_b_t(3,2) = (sin(phi)*cos(theta));
R_b_t(3,3) = (cos(phi)*cos(theta));

% H 3-2-1 in body coordinates
H_321 = zeros(3,3);
H(1,1) = -sin(theta);
H(1,2) = 0;
H(1,3) = 1;
H(2,1) = sin(phi)*cos(theta);
H(2,2) = cos(phi);
H(2,3) = 0;
H(3,1) = cos(phi)*cos(theta);
H(3,2) = -sin(phi);
H(3,3) = 0;

% Body to wind rotation
R_b_w = zeros(3,3);
R_b_w(1,1) = cos(beta)*cos(alpha);
R_b_w(1,2) = sin(beta);
R_b_w(1,3) = cos(beta)*sin(alpha);
R_b_w(2,1) = -sin(beta)*cos(alpha);
R_b_w(2,2) = cos(beta);
R_b_w(2,3) = -sin(beta)*sin(alpha);
R_b_w(3,1) = -sin(alpha);
R_b_w(3,2) = 0;
R_b_w(3,3) = cos(alpha);


% Pdot
xdot(1,1) = u*cos(theta)*cos(psi) + v*(sin(phi)*sin(theta)*cos(psi) - cos(phi)*sin(psi))...
    + w*(cos(phi)*sin(theta)*cos(psi) + sin(phi)*sin(psi)) + W(1,1);
xdot(2,1) = u*cos(theta)*sin(psi) + v*(sin(phi)*sin(theta)*sin(psi) + cos(phi)*cos(psi))...
    + w*(cos(phi)*sin(theta)*sin(psi) - sin(phi)*cos(psi)) + W(2,1);
xdot(3,1) = -u*sin(theta) + v*sin(phi)*cos(theta) + w*cos(phi)*cos(theta) + W(3,1);


% Define fores in body frame
f_d_b = R_b_w'*[-CD0_S*0.5*rho*Va^2;0;0];
Xoverm = 0 + (1/m)*f_d_b(1);
Yoverm = 0 + (1/m)*f_d_b(2);
Zoverm = -(K_F/m)*(omega1^2 + omega2^2 + omega3^2 + omega4^2) + (1/m)*f_d_b(3);

% VGdot = g + (1/m)*R_b_t*XYZ

xdot(4:6,1) = [0; 0; g] + R_b_t*[Xoverm; Yoverm; Zoverm];

% V^bdot

xdot(7,1) = r*(v + W(2)) - q*(w + W(3)) - g*sin(theta) + Xoverm;
xdot(8,1) = p*(w + W(3)) - r*(u + W(1)) + g*cos(theta)*sin(phi) + Yoverm;
xdot(9,1) = q*(u + W(1)) - p*(v + W(2)) + g*cos(theta)*cos(phi) + Zoverm;

% Euler angle dot
xdot(10,1) = (sin(phi)*q + cos(phi)*r)/cos(theta);
xdot(11,1) = cos(phi)*q - sin(phi)*r;
xdot(12,1) = p + sin(phi)*tan(theta)*q + cos(phi)*tan(theta)*r;

% Quaternions dot
xdot(13,1) = 0.5*(-p*e1 - q*e2 - r*e3);
xdot(14,1) = 0.5*(p*e0 + r*e2 - q*e3);
xdot(15,1) = 0.5*(q*e0 - r*e1 + p*e3);
xdot(16,1) = 0.5*(r*e0 + q*e1 - p*e2);


% Angular velocity dot
Jinv = inv(J);
xdot(17,1) = -GAMMA2*q*r + K_F*L*(omega2^2 - omega4^2)/Jx;
xdot(18,1) = GAMMA5*p*r + K_F*L*(omega3^2 - omega1^2)/Jy;
xdot(19,1) = GAMMA7*p*q + K_F*(K_M/K_F)*(omega1^2 - omega2^2 + omega3^2 - omega4^2)/Jz;

end



