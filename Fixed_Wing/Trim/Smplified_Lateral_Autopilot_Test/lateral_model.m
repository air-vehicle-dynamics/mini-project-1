function xlatdot = lateral_model(~,xlat)

global chiref Kichi Kpchi Kdphi Kpphi Kpbeta Kdbeta ulat deltaastar deltarstar Rstar Vastar gammastar phistar


g = 9.81;


xlatdot = zeros(6,1);

deltav = xlat(1);
deltap = xlat(2);
deltar = xlat(3);
deltaphi = xlat(4);
chi = xlat(5);
deltaphic = xlat(6);

A_lat = zeros(4,4);
A_lat(1,1) = -0.7336;
A_lat(1,2) = 1.1133;
A_lat(1,3) = -29.9793;
A_lat(1,4) = 9.8032;
A_lat(2,1) = -3.6891;
A_lat(2,2) = -13.4188;
A_lat(2,3) = 6.0240;
A_lat(2,4) = 0;
A_lat(3,1) = 3.9066;
A_lat(3,2) = -0.3886;
A_lat(3,3) = -8.0179;
A_lat(3,4) = 0;
A_lat(4,1) = 0;
A_lat(4,2) = 1;
A_lat(4,3) = 0.0371;
A_lat(4,4) = 0;

B_lat = zeros(4,2);
B_lat(1,1) = 0;
B_lat(1,2) = -3.82;
B_lat(2,1) = 90.47;
B_lat(2,2) = 110.59;
B_lat(3,1) = 36.14;
B_lat(3,2) = -8.40;
B_lat(4,1) = 0;
B_lat(4,2) = 0;

xlatdot(5,1) = (g/Vastar)*tan(deltaphi + phistar);

echi = chiref - chi;

xlatdot(6,1) =  Kichi*echi + Kpchi*((Vastar*cos(gammastar)/Rstar) - xlatdot(5,1));

ulat(1,1) = Kdphi*deltap + Kpphi*(deltaphic - deltaphi);
ulat(2,1) = Kpbeta*deltav/Vastar + Kdbeta*xlatdot(1,1)/Vastar;

% Saturation
if abs(ulat(1,1) + deltaastar) > 20*pi/180
    ulat(1,1) = 20*(pi/180)*sign(ulat(1,1) + deltaastar) - deltaastar;
end
if abs(ulat(2,1) + deltarstar) > 20*pi/180
    ulat(2,1) = 20*(pi/180)*sign(ulat(2,1) + deltarstar) - deltarstar;
end

xlatdot(1:4,1) = A_lat*xlat(1:4,1) + B_lat*ulat;


end
