global J m W g GAMMA GAMMA1 GAMMA2 GAMMA3 GAMMA4 GAMMA5 GAMMA6 GAMMA7 GAMMA8
global S b c S_prop rho K_motor K_TP K_omega e_oswald Aspect
global C_L_0 C_D_0 C_m_0 C_L_alpha C_D_alpha C_m_alpha C_L_q C_D_q C_m_q C_L_deltae C_D_deltae C_m_deltae C_prop 
global M_Stall alpha_0 epsilon C_D_p
global C_Y_0 C_l_0 C_n_0 C_Y_beta C_l_beta C_n_beta C_Y_p C_l_p C_n_p C_Y_r C_l_r C_n_r 
global C_Y_deltaa C_l_deltaa C_n_deltaa C_Y_deltar C_l_deltar C_n_deltar


m = 13.5;
J = [0.8244 0 -0.1204; 0 1.135 0; -0.1204 0 1.759];
S = 0.55;
b = 2.8956;
c = 0.18994;
S_prop = 0.2027;
K_motor = 80;
K_TP = 0;
K_omega = 0;
e_oswald = 0.9;
Aspect = b^2/S;
C_L_0 = 0.28;
C_D_0 = 0.03;
C_m_0 = -0.02338;
C_L_alpha = 3.45;
C_D_alpha = 0.30;
C_m_alpha = -0.38;
C_L_q = 0;
C_D_q = 0;
C_m_q = -3.6;
C_L_deltae = -0.36;
C_D_deltae = 0;
C_m_deltae = -0.5;
C_prop = 1.0;
M_Stall = 50;
alpha_0 = 0.4712;
epsilon = 0.1592;
C_D_p = 0.0437;
C_Y_0 = 0;
C_l_0 = 0;
C_n_0 = 0;
C_Y_beta = -0.98;
C_l_beta = -0.12;
C_n_beta = 0.25;
C_Y_p = 0;
C_l_p = -0.26;
C_n_p = 0.022;
C_Y_r = 0;
C_l_r = 0.14;
C_n_r = -0.35;
C_Y_deltaa = 0;
C_l_deltaa = 0.08;
C_n_deltaa = 0.06;
C_Y_deltar = -0.17;
C_l_deltar = 0.105;
C_n_deltar = -0.032;