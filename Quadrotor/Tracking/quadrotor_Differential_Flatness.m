function quadrotor_Differential_Flatness()

close all

unique_identifier_string = 'Dubins_1';
mkdir(unique_identifier_string)

%----- Inertia
mass	= 0.5; % kg
moi_J	= diag([2.32E-3 2.32E-3 4E-3]); % kg.m2
g		= 9.81;
K_M = 1.5E-9;
K_F = 6.11E-8;
K_m = 20;
L = 0.175;

omega1star = 4746.16017;
omega2star = 4746.99719;
omega3star = 4746.16017;
omega4star = 4745.32300;

ustar = K_F*[1 1 1 1; 0 L 0 -L; -L 0 L 0; K_M/K_F -K_M/K_F K_M/K_F -K_M/K_F]*[omega1star^2; omega2star^2; omega3star^2; omega4star^2];

phistar  = 27.00721129079*pi/180;
thetastar = 0*pi/180;
pstar = 0;
qstar = 0.22705131945;
rstar = 0.44547468877;

% phistar = 0;
% thetastar = 0;
% pstar = 0;
% qstar = 0;
% rstar = 0;

n_pts	= 10001;
time_pts= linspace(0, 7.25, n_pts);	% s



%----- Initial conditions
posn0	= [0; 9; -17.0];
yaw0	= 15*pi/180;
gamma0  = -3*pi/180;
Va0     = 15;
vel0    = Va0*[cos(yaw0)*cos(gamma0); sin(yaw0)*cos(gamma0); sin(gamma0)];
vel0    = [14.469; 3.8769; 0.78507];
ptc0	= 0*thetastar;
roll0	= 0*phistar;
e_quat0	= euler2quat([yaw0; ptc0; roll0]);
omga0	= 0*[pstar; qstar; rstar];


% For Dubins

% First Dubins Path
time_pts = 0:0.005:71.035;
n_pts = length(time_pts);

posn0 = [5; -5; -110];
vel0 = [10; 0; 2];
yaw0 = 4*pi/180;



%----- Reference
V_star	= 10;	% m/s
R_star	= 20;	% m



% Circle
% px_ref	= R_star * cos( (V_star/R_star)* time_pts );
% py_ref	= R_star * sin( (V_star/R_star)* time_pts );
% pz_ref	= -100*ones(1, n_pts);
% px_ref_dot = -V_star * sin( (V_star/R_star)* time_pts );
% py_ref_dot = V_star * cos( (V_star/R_star)* time_pts );
% pz_ref_dot = 0*ones(1,n_pts);
% px_ref_2dot = -(V_star^2 / R_star) * cos( (V_star/R_star)* time_pts );
% py_ref_2dot = -(V_star^2 / R_star) * sin( (V_star/R_star)* time_pts );
% pz_ref_2dot = 0*ones(1,n_pts);
% px_ref_3dot = (V_star^3 / (R_star^2)) * sin( (V_star/R_star)* time_pts );
% py_ref_3dot = -(V_star^3 / (R_star^2)) * cos( (V_star/R_star)* time_pts );
% pz_ref_3dot = 0*ones(1,n_pts);
% px_ref_4dot = (V_star^4/ (R_star^3)) * cos( (V_star/R_star)* time_pts );
% py_ref_4dot = (V_star^4/ (R_star^3)) * sin( (V_star/R_star)* time_pts );
% pz_ref_4dot = 0*ones(1,n_pts);
% yaw_ref	= (V_star/R_star)* time_pts + pi/2;
% yaw_ref_dot = (V_star/R_star)*ones(1,n_pts);
% yaw_ref_2dot = 0*ones(1,n_pts);



% Straight Line Path

% px_ref = V_star*time_pts;
% py_ref = 0*ones(1,n_pts);
% pz_ref	= -100*ones(1, n_pts);
% px_ref_dot = V_star*ones(1,n_pts);
% py_ref_dot = 0*ones(1,n_pts);
% pz_ref_dot = 0*ones(1,n_pts);
% px_ref_2dot = 0*ones(1,n_pts);
% py_ref_2dot = 0*ones(1,n_pts);
% pz_ref_2dot = 0*ones(1,n_pts);
% px_ref_3dot = 0*ones(1,n_pts);
% py_ref_3dot = 0*ones(1,n_pts);
% pz_ref_3dot = 0*ones(1,n_pts);
% px_ref_4dot = 0*ones(1,n_pts);
% py_ref_4dot = 0*ones(1,n_pts);
% pz_ref_4dot = 0*ones(1,n_pts);
% yaw_ref = 0*ones(1,n_pts);
% yaw_ref_dot = 0*ones(1,n_pts);
% yaw_ref_2dot = 0*ones(1,n_pts);


% Hover 

% px_ref = 0*ones(1,n_pts);
% py_ref = 0*ones(1,n_pts);
% pz_ref	= -100*ones(1, n_pts);
% px_ref_dot = 0*ones(1,n_pts);
% py_ref_dot = 0*ones(1,n_pts);
% pz_ref_dot = 0*ones(1,n_pts);
% px_ref_2dot = 0*ones(1,n_pts);
% py_ref_2dot = 0*ones(1,n_pts);
% pz_ref_2dot = 0*ones(1,n_pts);
% px_ref_3dot = 0*ones(1,n_pts);
% py_ref_3dot = 0*ones(1,n_pts);
% pz_ref_3dot = 0*ones(1,n_pts);
% px_ref_4dot = 0*ones(1,n_pts);
% py_ref_4dot = 0*ones(1,n_pts);
% pz_ref_4dot = 0*ones(1,n_pts);
% yaw_ref = 0*ones(1,n_pts);
% yaw_ref_dot = 0*ones(1,n_pts);
% yaw_ref_2dot = 0*ones(1,n_pts);


% Polynomial
% ak = [0; 14.469; -1.0127E-1; -1.1841E-4; -7.6013E-4; -3.4094E-3; 2.9767E-4];
% bk = [9.1440; 3.8769; -4.1309; 1.1727; -1.6510E-1; 1.2217E-2; -3.8280E-4];
% ck = [-16.764; 7.8507E-1; 3.1597; -1.0059; 1.2597E-1; -6.9039E-3; 1.2789E-4];
% px_ref = zeros(1,n_pts);
% py_ref = zeros(1,n_pts);
% pz_ref = zeros(1,n_pts);
% for k = 0:6
%     px_ref = px_ref + ak(k+1)*time_pts.^k;
%     py_ref = py_ref + bk(k+1)*time_pts.^k;
%     pz_ref = pz_ref + ck(k+1)*time_pts.^k;
% end
% px_ref_dot = zeros(1,n_pts);
% py_ref_dot = zeros(1,n_pts);
% pz_ref_dot = zeros(1,n_pts);
% for k = 1:6
%     px_ref_dot = px_ref_dot + k*ak(k+1)*time_pts.^(k-1);
%     py_ref_dot = py_ref_dot + k*bk(k+1)*time_pts.^(k-1);
%     pz_ref_dot = pz_ref_dot + k*ck(k+1)*time_pts.^(k-1);
% end
% px_ref_2dot = zeros(1,n_pts);
% py_ref_2dot = zeros(1,n_pts);
% pz_ref_2dot = zeros(1,n_pts);
% for k = 2:6
%     px_ref_2dot = px_ref_2dot + k*(k-1)*ak(k+1)*time_pts.^(k-2);
%     py_ref_2dot = py_ref_2dot + k*(k-1)*bk(k+1)*time_pts.^(k-2);
%     pz_ref_2dot = pz_ref_2dot + k*(k-1)*ck(k+1)*time_pts.^(k-2);
% end
% px_ref_3dot = zeros(1,n_pts);
% py_ref_3dot = zeros(1,n_pts);
% pz_ref_3dot = zeros(1,n_pts);
% for k = 3:6
%     px_ref_3dot = px_ref_3dot + k*(k-1)*(k-2)*ak(k+1)*time_pts.^(k-3);
%     py_ref_3dot = py_ref_3dot + k*(k-1)*(k-2)*bk(k+1)*time_pts.^(k-3);
%     pz_ref_3dot = pz_ref_3dot + k*(k-1)*(k-2)*ck(k+1)*time_pts.^(k-3);
% end
% px_ref_4dot = zeros(1,n_pts);
% py_ref_4dot = zeros(1,n_pts);
% pz_ref_4dot = zeros(1,n_pts);
% for k = 4:6
%     px_ref_4dot = px_ref_4dot + k*(k-1)*(k-2)*(k-3)*ak(k+1)*time_pts.^(k-4);
%     py_ref_4dot = py_ref_4dot + k*(k-1)*(k-2)*(k-3)*bk(k+1)*time_pts.^(k-4);
%     pz_ref_4dot = pz_ref_4dot + k*(k-1)*(k-2)*(k-3)*ck(k+1)*time_pts.^(k-4);
% end
% 
% yaw_ref = 15*(pi/180)*ones(1,n_pts);
% yaw_ref_dot = 0*ones(1,n_pts);
% yaw_ref_2dot = 0*ones(1,n_pts);
% 
% pos_ref = [px_ref; py_ref; pz_ref];
% pos_ref_dot = [px_ref_dot; py_ref_dot; pz_ref_dot];
% pos_ref_2dot = [px_ref_2dot; py_ref_2dot; pz_ref_2dot];
% pos_ref_3dot = [px_ref_3dot; py_ref_3dot; pz_ref_3dot];
% pos_ref_4dot = [px_ref_4dot; py_ref_4dot; pz_ref_4dot];


% For Dubins
[pos_ref, pos_ref_dot, pos_ref_2dot, pos_ref_3dot, pos_ref_4dot] = gen_traj_dubins();
px_ref = pos_ref(1,:);
py_ref = pos_ref(2,:);
pz_ref = pos_ref(3,:);

px_ref_dot = pos_ref_dot(1,:);
py_ref_dot = pos_ref_dot(2,:);
pz_ref_dot = pos_ref_dot(3,:);

px_ref_2dot = pos_ref_2dot(1,:);
py_ref_2dot = pos_ref_2dot(2,:);
pz_ref_2dot = pos_ref_2dot(3,:);

px_ref_3dot = pos_ref_3dot(1,:);
py_ref_3dot = pos_ref_3dot(2,:);
pz_ref_3dot = pos_ref_3dot(3,:);

px_ref_4dot = pos_ref_4dot(1,:);
py_ref_4dot = pos_ref_4dot(2,:);
pz_ref_4dot = pos_ref_4dot(3,:);

yaw_ref = 0*ones(1,n_pts);
yaw_ref_dot = 0*ones(1,n_pts);
yaw_ref_2dot = 0*ones(1,n_pts);





x		= [posn0; vel0; yaw0; ptc0; roll0; omga0; e_quat0];
x_true	= zeros(16, n_pts);
u_true	= zeros(4, n_pts);
pos_tilde_2dot_true = zeros(3, n_pts);
pos_tilde_3dot_true = zeros(3, n_pts);
pos_tilde_4dot_true = zeros(3, n_pts);
yaw_tilde_2dot = zeros(1,n_pts);

% Gains for Polynomial
K_pos_p = 1.5*[1; 1; 10];
K_pos_d = 1.5*[1; 1; 1];
K_angles = 1*[1; 1; 1];
K_angular_rates = 0.1*[1; 1; 1];

% Gains for Dubins
K_pos_p = 0.3*[1; 1; 1];
K_pos_d = 0.3*[1; 1; 1];
K_angles = 0.5*[1; 1; 1];
K_angular_rates = 0.1*[1; 1; 1];



x_true(:, 1)	= x;
pqr_2minus = [0; 0; 0];
pqr_minus = [0; 0; 0];
pqr = [0; 0; 0];
for m1 = 2:n_pts
    t = time_pts(m1);
    m1
	dt		= time_pts(m1) - time_pts(m1 - 1);
	
    yaw_tilde_2dot(m1-1) = yaw_ref_2dot(m1-1);
    
   
    
    pos_tilde_2dot = [0;0;0];
    for m2 = 1:3
        pos_tilde_2dot(m2) = pos_ref_2dot(m2, m1-1) + ...
            K_pos_d(m2)*(pos_ref_dot(m2, m1-1) - x(m2+3)) + ...
            K_pos_p(m2)*(pos_ref(m2, m1-1) - x(m2));
    end
    pos_tilde_2dot_true(:,m1-1) = pos_tilde_2dot';
    
    pos_tilde_3dot_true(:,m1-1) = pos_ref_3dot(:,m1-1)';
    pos_tilde_4dot_true(:,m1-1) = pos_ref_4dot(:,m1-1)';
    
    
    if m1 > 3
        pos_tilde_3dot_true(:,m1-1) = (pos_tilde_2dot_true(:,m1-3) - 4*pos_tilde_2dot_true(:,m1-2) + 3*pos_tilde_2dot_true(:,m1-1))/(2*dt);
        if m1 > 6
            pos_tilde_4dot_true(:,m1-1) = (pos_tilde_3dot_true(:,m1-3) - 4*pos_tilde_3dot_true(:,m1-2) + 3*pos_tilde_3dot_true(:,m1-1))/(2*dt);
        end
    end
    
    
    %disp(pos_tilde_2dot_true(:,m1-1)-pos_ref_2dot(:,m1-1))
    %disp(pos_tilde_3dot_true(:,m1-1)-pos_ref_3dot(:,m1-1))
    %disp(pos_tilde_4dot_true(:,m1-1)-pos_ref_4dot(:,m1-1))
    
    %F_t = mass*pos_ref_2dot(:,m1-1) - [0; 0; mass*g];
    F_t = mass*pos_tilde_2dot_true(:,m1-1) - [0; 0; mass*g];
    
    input_u1 = norm(F_t);
    u1 = input_u1;
    
    k_b_t = -F_t/norm(F_t);
    theta = asin(k_b_t(1));
    phi =-sign(k_b_t(2))*acos(norm(cross([-sin(yaw_ref(m1-1)); cos(yaw_ref(m1-1)); 0], k_b_t)));
    j_a_t = [-sin(yaw_ref(m1-1)); cos(yaw_ref(m1-1)); 0];
    j_e_t = j_a_t;
    
    i_b_t = cross([-sin(yaw_ref(m1-1)); cos(yaw_ref(m1-1)); 0], k_b_t);
    i_b_t = i_b_t/norm(i_b_t);
    
    j_b_t = cross(k_b_t, i_b_t);

    R_b_t = [i_b_t j_b_t k_b_t];
    R_t_b = R_b_t';

    R_t_b_real = [cos(x(8))*cos(x(7)) cos(x(8))*sin(x(7)) -sin(x(8));
        (-cos(x(9))*sin(x(7))+sin(x(9))*sin(x(8))*cos(x(7))) (cos(x(9))*cos(x(7))+sin(x(9))*sin(x(8))*sin(x(7))) (sin(x(9))*cos(x(8)));
        (sin(x(9))*sin(x(7))+cos(x(9))*sin(x(8))*cos(x(7))) (-sin(x(9))*cos(x(7))+cos(x(9))*sin(x(8))*sin(x(7))) (cos(x(9))*cos(x(8)))];
    
    %S_b = mass*R_t_b*(pos_ref_4dot(:,m1-1));
    %J_b = mass*R_t_b*(pos_ref_3dot(:,m1-1));
    
    S_b = mass*R_t_b*(pos_tilde_4dot_true(:,m1-1));
    J_b = mass*R_t_b*(pos_tilde_3dot_true(:,m1-1));
    
    p = J_b(2)/u1;
    q = -J_b(1)/u1;
    
    u1dot = J_b(3);
    %norm(k_b_t)
    %norm(i_b_t)
    %norm(j_e_t)
    %norm(j_b_t)
    
    r_phidot_thetadot = (inv([-k_b_t i_b_t j_e_t]))*(R_b_t*[p; q; 0] - [0; 0; yaw_ref_dot(m1-1)]);
    r = r_phidot_thetadot(1);
    phidot = r_phidot_thetadot(2);
    thetadot = r_phidot_thetadot(3);
    
    pqr_2minus = pqr_minus;
    pqr_minus = pqr;
    pqr = [p; q; r];
    
    qdot = -(S_b(1) + 2*q*u1dot + p*r*u1)/u1;
    pdot = (S_b(2) - 2*p*u1dot + q*r*u1)/u1;
    
    %rdot_thetaddot_phiddot = (inv([-k_b_t j_e_t i_b_t]))*(R_b_t*[pdot; qdot; 0] + [thetadot*yaw_ref_dot(m1-1); 0; 0]...
    %    - phidot*[0; r; -q] - [0; 0; yaw_ref_2dot(m1-1)]);
    rdot_thetaddot_phiddot = (inv([-k_b_t j_e_t i_b_t]))*(R_b_t*[pdot; qdot; 0] + [thetadot*yaw_ref_dot(m1-1); 0; 0]...
        - phidot*[0; r; -q] - [0; 0; yaw_tilde_2dot(m1-1)]);
    rdot = rdot_thetaddot_phiddot(1);
    %rdot = 0;
    
    
    if m1 > 3
        pqr_dot = (pqr_2minus - 4*pqr_minus + 3*pqr)/(2*dt);
        pdot = pqr_dot(1);
        qdot = pqr_dot(2);
        rdot = pqr_dot(3);
    end
    
    u2 = moi_J*[pdot; qdot; rdot] + cross([p; q; r], (moi_J*[p; q; r]));
    u2 = u2 + K_angles.*[phi-x(9); theta-x(8); yaw_ref(m1)-x(7)] + K_angular_rates.*[p-x(10); q-x(11); r-x(12)];
    input_u2 = u2;
  
    u_t = [input_u1; input_u2];
    u_t_dt = u_t;
    
	%u_t		= [mass*g; 0; 0; 0];	% Hover
	%u_t_dt	= [mass*g; 0; 0; 0];	% Hover
	u_t_half= u_t;
	
	k1	= dt*quadrotor_dynamics(x,				u_t);
	k2	= dt*quadrotor_dynamics((x + 0.5*k1),	u_t_half);
	k3	= dt*quadrotor_dynamics((x + 0.5*k2),	u_t_half);
	k4	= dt*quadrotor_dynamics((x + k3),		u_t_dt);
	
	x	= x + (1/6)*k1 + (1/3)*k2 + (1/3)*k3 + (1/6)*k4;
	x_true(:, m1)	= x;
	u_true(:, m1)	= u_t;
end

figure('Name', 'Position');
plot3(x_true(1, :), x_true(2, :), -x_true(3, :), 'LineWidth', 2', 'Marker', '.', 'MarkerSize', 10); axis equal; grid on; hold on;
plot3(px_ref, py_ref, -pz_ref, 'LineWidth', 2', 'Marker', '.', 'MarkerSize', 10);
print(strcat(unique_identifier_string,'/3DPosition'),'-dpng')

figure('Name', 'Position Coordinates');
subplot(311); plot(time_pts, x_true(1, :), time_pts, px_ref, 'LineWidth', 2); xlabel('Time (s)'); ylabel('p_x (m)'); grid on;
subplot(312); plot(time_pts, x_true(2, :), time_pts, py_ref, 'LineWidth', 2); xlabel('Time (s)'); ylabel('p_y (m)'); grid on;
subplot(313); plot(time_pts, -x_true(3, :), time_pts, -pz_ref, 'LineWidth', 2); xlabel('Time (s)'); ylabel('-p_z (m)'); grid on;
print(strcat(unique_identifier_string,'/Position'),'-dpng')
	

figure('Name', 'Velocity');
subplot(311); plot(time_pts, x_true(4, :), 'LineWidth', 2); xlabel('Time (s)'); ylabel('v_x (m/s)'); grid on;
subplot(312); plot(time_pts, x_true(5, :), 'LineWidth', 2); xlabel('Time (s)'); ylabel('v_y (m/s)'); grid on;
subplot(313); plot(time_pts, x_true(6, :), 'LineWidth', 2); xlabel('Time (s)'); ylabel('-v_z (m/s)'); grid on;
print(strcat(unique_identifier_string,'/Velocity'),'-dpng')


figure('Name', 'Euler angles');
subplot(311); plot(time_pts, wrapTo180(x_true(7, :)*180/pi), time_pts, wrapTo180(yaw_ref*180/pi), 'LineWidth', 2); xlabel('Time (s)'); ylabel('\psi (deg)'); grid on;
subplot(312); plot(time_pts, x_true(8, :)*180/pi, 'LineWidth', 2); xlabel('Time (s)'); ylabel('\theta (deg)'); grid on;
subplot(313); plot(time_pts, x_true(9, :)*180/pi, 'LineWidth', 2); xlabel('Time (s)'); ylabel('\phi (deg)'); grid on;
print(strcat(unique_identifier_string,'/Euler'),'-dpng')


figure('Name', 'Angular velocity');
subplot(311); plot(time_pts, x_true(10, :)*180/pi, 'LineWidth', 2); xlabel('Time (s)'); ylabel('p (deg/s)'); grid on;
subplot(312); plot(time_pts, x_true(11, :)*180/pi, 'LineWidth', 2); xlabel('Time (s)'); ylabel('q (deg/s)'); grid on;
subplot(313); plot(time_pts, x_true(12, :)*180/pi, 'LineWidth', 2); xlabel('Time (s)'); ylabel('r (deg/s)'); grid on;
print(strcat(unique_identifier_string,'/Angular'),'-dpng')


figure('Name', 'Control');
subplot(411); plot(time_pts, u_true(2, :)*180/pi, 'LineWidth', 2); xlabel('Time (s)'); ylabel('Roll Input'); grid on;
subplot(412); plot(time_pts, u_true(3, :)*180/pi, 'LineWidth', 2); xlabel('Time (s)'); ylabel('Pitch Input'); grid on;
subplot(413); plot(time_pts, u_true(4, :)*180/pi, 'LineWidth', 2); xlabel('Time (s)'); ylabel('Yaw Input'); grid on;
subplot(414); plot(time_pts, u_true(1, :), 'Linewidth', 2); xlabel('Time (s)'); ylabel('Force Input'); grid on;
print(strcat(unique_identifier_string,'/Controls'),'-dpng')


	function x_dot_ = quadrotor_dynamics(x_, u_)
		vel		= x_(4:6);
		yaw		= x_(7);
		ptc_	= x_(8);
		roll_	= x_(9);
		omga_	= x_(10:12);
		e_quat	= x_(13:16)';
		
		Rbt_		= [...
			cos(ptc_)*cos(yaw)	sin(roll_)*sin(ptc_)*cos(yaw) - cos(roll_)*sin(yaw)	cos(roll_)*sin(ptc_)*cos(yaw) + sin(roll_)*sin(yaw); ...
			cos(ptc_)*sin(yaw)	sin(roll_)*sin(ptc_)*sin(yaw) + cos(roll_)*cos(yaw)	cos(roll_)*sin(ptc_)*sin(yaw) - sin(roll_)*cos(yaw); ...
			-sin(ptc_)			sin(roll_)*cos(ptc_)									cos(roll_)*cos(ptc_)];
		Fb		= [0; 0; -u_(1)];
		Mb		= u_(2:4);
		
		posn_dot	= vel;
		vel_dot_e	= Rbt_*Fb/mass + [0; 0; g];
		e1			= quatmultiply(e_quat, (quatmultiply([0;Fb]', quatconj(e_quat))) )';
		vel_dot_q	= (1/mass) * e1(2:4) + [0; 0; g];
		
		%disp(norm( vel_dot_e - vel_dot_q));
		
		euler_dot	= [...
			-sin(ptc_)			0			1;
			sin(roll_)*cos(ptc_)	cos(roll_)	0;
			cos(roll_)*cos(ptc_)	-sin(roll_)	0] \ omga_;
		e_quat_dot	= 0.5*quatmultiply(e_quat, [0; omga_]');
		omga_dot	= moi_J \ (Mb - cross(omga_, moi_J*omga_));
		
		x_dot_		= [posn_dot; vel_dot_q; euler_dot; omga_dot; e_quat_dot'];
	end

	function quat_ = euler2quat(euler_angles_)
		e_psi_	= euler_angles_(1);
		e_thta_	= euler_angles_(2);
		e_phi_	= euler_angles_(3);
		quat_	=	[ ...
			cos(e_psi_/2)*cos(e_thta_/2)*cos(e_phi_/2) + sin(e_psi_/2)*sin(e_thta_/2)*sin(e_phi_/2); ...
			cos(e_psi_/2)*cos(e_thta_/2)*sin(e_phi_/2) - sin(e_psi_/2)*sin(e_thta_/2)*cos(e_phi_/2); ...
			cos(e_psi_/2)*sin(e_thta_/2)*cos(e_phi_/2) + sin(e_psi_/2)*cos(e_thta_/2)*sin(e_phi_/2); ...
			sin(e_psi_/2)*cos(e_thta_/2)*cos(e_phi_/2) - cos(e_psi_/2)*sin(e_thta_/2)*sin(e_phi_/2)];

	end
end
