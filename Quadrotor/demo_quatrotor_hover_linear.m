
%% Sim Setup

n_pts = 1001;
time_pts = linspace(0, 300, n_pts); % [s]

%% Initial Conditions

global mass g J

mass = 0.5; % kg
g = 9.81;   % m/s/s
J = [2.32E-3 0 0; 0 2.32E-3 0; 0 0 4E-3];


posn0 = [10; 0; -100];
yaw0 = 5*pi/180;
vel0 = 3*[cos(yaw0); sin(yaw0); 0];
ptc0 = 0;
roll0 = 0;
e_quat0 = euler2quat([yaw0; ptc0; roll0]);
omega0 = [0; 0; 0];

%% Reference
% V_star = 15;  % [m/s]
% R_star = 100; % [m]



% Hovering
yaw_ref = 0*ones(1,n_pts);     % can pick any angle
yaw_ref_dot = 0*ones(1,n_pts);

px_ref = 0*ones(1,n_pts);    % Hover at origin
py_ref = 0*ones(1,n_pts);    % Hover at origin
pz_ref = -100*ones(1,n_pts); % Hover at 100m
pos_ref = [px_ref; py_ref; pz_ref];

px_ref_dot = 0*ones(1,n_pts); % Hover at origin
py_ref_dot = 0*ones(1,n_pts); % Hover at origin
pz_ref_dot = 0*ones(1,n_pts); % Hover at 100m
pos_ref_dot = [px_ref_dot; py_ref_dot; pz_ref_dot];

px_ref_ddot = 0*ones(1,n_pts);
py_ref_ddot = 0*ones(1,n_pts);
pz_ref_ddot = 0*ones(1,n_pts);
pos_ref_ddot = [px_ref_ddot; py_ref_ddot; pz_ref_ddot];

x = [posn0; vel0; yaw0; ptc0; roll0; omega0; e_quat0];
x_true = zeros(16, n_pts);
u_true = zeros(4,  n_pts);

x_true(:,1) = x;

for i = 2:n_pts
    t  = time_pts(i);
    dt = time_pts(i) - time_pts(i-1);
    
    
    K_pos_p = 0.01*[1; 1; 200];
    K_pos_d = 0.01*[1; 1; 50];
    K_roll_p = 0.3;  K_roll_d = 0.05;
    K_ptc_p  = 0.3;  K_ptc_d  = 0.05;
    K_yaw_p  = 0.3;  K_yaw_d  = 0.05;
    
    pos_tilde_ddot = [0; 0; 0];
    
    for j = 1:3
        pos_tilde_ddot(j) = pos_ref_ddot(j,i)  + ...
                            K_pos_d(j)*(pos_ref_dot(j,i) - x(j+3)) +...
                            K_pos_p(j)*(    pos_ref(j,i) - x(j));
    end
    
    roll_ptc_tilde = (1/g) * [-cos(yaw_ref(i)) -sin(yaw_ref(i));
                              -sin(yaw_ref(i))  cos(yaw_ref(i))] \ pos_tilde_ddot(1:2);
                          
    input_u1 = mass * (g - pos_tilde_ddot(3));
    input_u2 = [K_roll_p*(roll_ptc_tilde(2) - x(9)) + K_roll_d*(0              - x(10));
                K_ptc_p *(roll_ptc_tilde(1) - x(8)) + K_ptc_d *(0              - x(11));
                K_yaw_p *(yaw_ref(i)        - x(7)) + K_yaw_d *(yaw_ref_dot(i) - x(12))];
            
    ut    = [input_u1; input_u2]; % linearized controller
    ut_dt = [input_u1; input_u2]; % linearized controller
    

%     ut      = [mass*g; 0; 0; 0]; % Open Loop Hover
%     ut_dt   = [mass*g; 0; 0; 0]; % Open Loop Hover

    ut_half = 0.5*(ut + ut_dt);
    
    k1 = dt*quadrotor_dynamics(x,            ut);
    k2 = dt*quadrotor_dynamics((x + 0.5*k1), ut_half);
    k3 = dt*quadrotor_dynamics((x + 0.5*k2), ut_half);
    k4 = dt*quadrotor_dynamics((x + k3)    , ut_dt);
    
    x = x + ((1/6)*k1 + (1/3)*k2 + (1/3)*k3 + (1/6)*k4);
    x_true(:,i) = x;
    u_true(:,i) = ut;
end


%% Plot
figure('Name', 'Position')
plot3(x_true(1,:), x_true(2,:) -x_true(3,:), 'LineWidth', 2, 'Marker', '.', 'MarkerSize', 20);
axis equal; grid on; hold on;
plot3(px_ref, py_ref, -pz_ref, 'LineWidth', 2, 'Marker', '.', 'MarkerSize', 20);

%% Functions 

% 16:13
function x_dot = quadrotor_dynamics(x, u)
global mass g J

vel = x(4:6);
yaw = x(7);
ptc = x(8);
roll = x(9);
omega = x(10:12);
e_quat = x(13:16)';


R_bt = [cos(ptc)*cos(yaw) sin(roll)*sin(ptc)*cos(yaw)-cos(roll)*sin(yaw) cos(roll)*sin(ptc)*cos(yaw)+sin(roll)*sin(yaw);
        cos(ptc)*sin(yaw) sin(roll)*sin(ptc)*sin(yaw)+cos(roll)*cos(yaw) cos(roll)*sin(ptc)*sin(yaw)-sin(roll)*cos(yaw);
        -sin(ptc)         sin(roll)*cos(ptc)                             cos(roll)*cos(ptc)                           ];
    
Fb = [0; 0; -u(1)];
Mb = u(2:4);

pos_dot   = vel;
vel_dot_e = R_bt * Fb/mass + [0; 0; g];
e1        = quatmultiply(e_quat, (quatmultiply([0;Fb]', quatconj(e_quat))))';
vel_dot_q = (1/mass) * e1(2:4) + [0; 0; g];

disp(norm(vel_dot_e - vel_dot_q))

euler_dot = [ -sin(ptc)            0         1;
               sin(roll)*cos(ptc)  cos(roll) 0;
               cos(roll)*cos(ptc) -sin(roll) 0] \ omega;
           
e_quat_dot = 0.5 * quatmultiply(e_quat, [0; omega]');
omega_dot  = J \ (Mb - cross(omega, J*omega));

x_dot = [pos_dot; vel_dot_q; euler_dot; omega_dot; e_quat_dot'];
end


function quat = euler2quat(euler_angles)
e_psi = euler_angles(1);
e_th  = euler_angles(2);
e_phi = euler_angles(3);

quat = [cos(e_psi/2)*cos(e_th/2)*cos(e_phi/2) + sin(e_psi/2)*sin(e_th/2)*sin(e_phi/2);
        cos(e_psi/2)*cos(e_th/2)*sin(e_phi/2) - sin(e_psi/2)*sin(e_th/2)*cos(e_phi/2);
        cos(e_psi/2)*sin(e_th/2)*cos(e_phi/2) + sin(e_psi/2)*cos(e_th/2)*sin(e_phi/2);
        sin(e_psi/2)*cos(e_th/2)*cos(e_phi/2) - cos(e_psi/2)*sin(e_th/2)*sin(e_phi/2)];
end