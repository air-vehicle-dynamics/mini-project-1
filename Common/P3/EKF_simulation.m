%% Input Simulation Results
clear all; clc;close all;

dbstop if error

unique_identifier_string = 'run02';
mkdir(unique_identifier_string);

% Load simulation parameters and results
sd = load('Simulation_With_Sensors_Data.mat');

% Load sensor parameters
% [rate_gyro,accel,pressure,dig_comp,gps] = run('..\sensor_characteristics.m');
run 'sensor_characteristics.m'

% collect sensor data from sim
time_arr  = sd.to_sensor(:,1);  % SIM time (s)
GPS_pos_x = sd.to_sensor(:,2);  % GPS pos, X (m)
GPS_pos_y = sd.to_sensor(:,3);  % GPS pos, Y (m) 
GPS_pos_z = sd.to_sensor(:,4);  % GPS pos, Z (m)
GPS_vel_x = sd.to_sensor(:,5);  % GPS vel, X (m/s)
GPS_vel_y = sd.to_sensor(:,6);  % GPS vel, Y (m/s)
GPS_vel_z = sd.to_sensor(:,7);  % GPS vel, Z (m/s)
MAG_psi   = sd.to_sensor(:,14); % MAG yaw (rad)
MAG_theta = sd.to_sensor(:,15); % MAG pitch (rad)
MAG_phi   = sd.to_sensor(:,16); % MAG roll (rad)
airspeed  = sd.to_sensor(:,11); % airspeed (m/s)
ACCEL_x   = sd.to_sensor(:,24); % acceleration, X (m/s/s)
ACCEL_y   = sd.to_sensor(:,25); % acceleration, Y (m/s/s)
ACCEL_z   = sd.to_sensor(:,26); % acceleration, Z (m/s/s)
GYRO_p    = sd.to_sensor(:,21); % rate gyro, p (deg/s)
GYRO_q    = sd.to_sensor(:,22); % rate gyro, q (deg/s)
GYRO_r    = sd.to_sensor(:,23); % rate gyro, r (deg/s)


%% Generate Noisy Sensors
% from sensor_characteristics and Base_Simulation
bias_mag	= 20*pi/180 * randn(3, 1);	% Magnetometer bias (rad)
bias_gyro	= 0*pi/180 * randn(3, 1);	% Rate gyro bias (rad/s)
bias_GPS_p  = 0;                        % GPS position bias (m), assume 0
bias_GPS_v  = 0;                        % GPS velocity bias (m/s), assume 0
bias_airspeed = 300*randn(1);               % Airspeed bias (m/s), assume 0
bias_accel  = 2*randn(3,1);

% sensor grouping
time_arr       = time_arr';

GPS_pos        = [GPS_pos_x'; GPS_pos_y'; GPS_pos_z']; 
GPS_pos_bias   = [bias_GPS_p; bias_GPS_p; bias_GPS_p];
GPS_pos_covar  = [gps.noise_stdev_pos_NE; gps.noise_stdev_pos_NE; gps.noise_stdev_pos_Z];

GPS_vel        = [GPS_vel_x'; GPS_vel_y'; GPS_vel_z'];
GPS_vel_bias   = [bias_GPS_v; bias_GPS_v; bias_GPS_v];
GPS_vel_covar  = [gps.noise_stdev_vel_NE; gps.noise_stdev_vel_NE; gps.noise_stdev_vel_Z];

MAG            = [MAG_psi'; MAG_theta'; MAG_phi'];
MAG_bias       = [bias_mag(1); bias_mag(2); bias_mag(3)];
MAG_covar      = [dig_comp.noise_stdev; dig_comp.noise_stdev; dig_comp.noise_stdev];

AIRSPEED       = airspeed';
AIRSPEED_bias  = bias_airspeed;
AIRSPEED_covar = pressure.noise_stdev;

ACCEL          = [ACCEL_x'; ACCEL_y'; ACCEL_z'];
ACCEL_bias     = [bias_accel(1); bias_accel(2); bias_accel(3)];
ACCEL_covar    = [accel.noise_stdev; accel.noise_stdev; accel.noise_stdev];

GYRO           = [GYRO_p'; GYRO_q'; GYRO_r'];
GYRO_bias      = [bias_gyro(1); bias_gyro(2); bias_gyro(3)];
GYRO_covar     = [rate_gyro.noise_stdev; rate_gyro.noise_stdev; rate_gyro.noise_stdev];

% noisy signals
GPS_pos_noise  = noisy_signal( GPS_pos,  GPS_pos_bias,  GPS_pos_covar);
GPS_vel_noise  = noisy_signal( GPS_vel,  GPS_vel_bias,  GPS_vel_covar);
MAG_noise      = noisy_signal(     MAG,      MAG_bias,      MAG_covar);
AIRSPEED_noise = noisy_signal(AIRSPEED, AIRSPEED_bias, AIRSPEED_covar);
ACCEL_noise    = noisy_signal(   ACCEL,    ACCEL_bias,    ACCEL_covar);
GYRO_noise     = noisy_signal(    GYRO,     GYRO_bias,     GYRO_covar);


g_12 = zeros(2,length(time_arr));
g_3  = 9.81 * ones(1, length(time_arr));
g = [g_12; g_3];

%convert airspeed to deltaP values     
for i = 1:length(time_arr)
    rho = atmosphere(-GPS_pos_z(i));
    rho = 1.225;
    deltaP(i) = AIRSPEED(i)^2 * (rho/2);
end

deltaP_noise = noisy_signal(deltaP, AIRSPEED_bias, AIRSPEED_covar);

% full sensor stream
% sensor_data = [GPS_pos_noise; GPS_vel_noise; MAG_noise; AIRSPEED_noise];
sensor_data = [GPS_pos_noise; GPS_vel_noise; MAG_noise; deltaP_noise];
predict_sensor_data = [ACCEL_noise; GYRO_noise; g];
wind = [0; 0; 0];


%% Extended Kalman Filter State Estimation

% set up ekf
[x_hat] = EKF_ac(time_arr, sensor_data, predict_sensor_data, wind,rate_gyro,accel,pressure,dig_comp,gps);

%% Plot 

% Position
figure

subplot(3,1,1)
plot(time_arr, x_hat(1,:),'linewidth',2)
hold on
plot(time_arr, GPS_pos_x,'r--','linewidth',2)
plot(time_arr, GPS_pos_noise(1,:),'k--','linewidth',2)
ylabel('P_x^t (m)')

title('Positions','fontsize',12)
Lgnd = legend('Estimated','True','GPS - Noisy','location','east');
Lgnd.Position(1) = .8;
Lgnd.Position(2) = 0.9;

subplot(3,1,2)
plot(time_arr, x_hat(2,:),'linewidth',2)
hold on
plot(time_arr, GPS_pos_y,'r--','linewidth',2)
plot(time_arr, GPS_pos_noise(2,:),'k--','linewidth',2)
ylabel('P_y^t (m)')

subplot(3,1,3)
plot(time_arr, x_hat(3,:),'linewidth',2)
hold on
plot(time_arr, GPS_pos_z,'r--','linewidth',2)
plot(time_arr, GPS_pos_noise(3,:),'k--','linewidth',2)
ylabel('P_z^t (m)')
xlabel('t')


print(strcat(unique_identifier_string,'/Positions'),'-dpng')
% Ground Velocities
figure
subplot(3,1,1)
plot(time_arr, x_hat(4,:),'linewidth',2)
hold on
plot(time_arr, GPS_vel_x,'r--','linewidth',2)
plot(time_arr, GPS_vel_noise(1,:),'k--', 'linewidth',2)
ylabel('V_x^t (m/s)')

title('Velocities','fontsize',12)
Lgnd = legend('Estimated','True','GPS - Noisy','location','east');
Lgnd.Position(1) = .8;
Lgnd.Position(2) = 0.9;

subplot(3,1,2)
plot(time_arr, x_hat(5,:),'linewidth',2)
hold on
plot(time_arr, GPS_vel_y,'r--','linewidth',2)
plot(time_arr, GPS_vel_noise(2,:),'k--', 'linewidth',2)
ylabel('V_y^t (m/s)')

subplot(3,1,3)
plot(time_arr, x_hat(6,:),'linewidth',2)
hold on
plot(time_arr, GPS_vel_z,'r--','linewidth',2)
plot(time_arr, GPS_vel_noise(3,:),'k--', 'linewidth',2)
ylabel('V_z^t (m/s)')
xlabel('t')

print(strcat(unique_identifier_string,'/Velocities'),'-dpng')

% Euler Angles
figure
subplot(3,1,1)
plot(time_arr, x_hat(7,:)*180/pi,'linewidth',2)
hold on
plot(time_arr, MAG_psi*180/pi,'r--','linewidth',2)
plot(time_arr, MAG_noise(1,:)*180/pi,'k--','linewidth',2)
ylabel('\psi (deg)')

title('Euler Angles','fontsize',12)
Lgnd = legend('Estimated','True','Magnetometer - Noisy','location','east');
Lgnd.Position(1) = .8;
Lgnd.Position(2) = 0.9;

subplot(3,1,2)
plot(time_arr, x_hat(8,:)*180/pi,'linewidth',2)
hold on
plot(time_arr, MAG_theta*180/pi,'r--','linewidth',2)
plot(time_arr, MAG_noise(2,:)*180/pi,'k--','linewidth',2)
ylabel('\theta (deg)')

subplot(3,1,3)
plot(time_arr, x_hat(9,:)*180/pi,'linewidth',2)
hold on
plot(time_arr, MAG_phi*180/pi,'r--','linewidth',2)
plot(time_arr, MAG_noise(3,:)*180/pi,'k--','linewidth',2)
ylabel('\phi  (deg)')
xlabel('t')
print(strcat(unique_identifier_string,'/EulerAngles'),'-dpng')

% Euler Biases
figure
subplot(3,1,1)
plot(time_arr, x_hat(10,:)*180/pi,'linewidth',2)
hold on
plot(time_arr, (bias_mag(1)*ones(size(time_arr)))*180/pi,'r--','linewidth',2)
ylabel('\psi Bias (deg)')

title('Euler Angle (Magnetometer) Biases','fontsize',12)
Lgnd = legend('Estimated','True','location','east');
Lgnd.Position(1) = .8;
Lgnd.Position(2) = 0.9;

subplot(3,1,2)
plot(time_arr, x_hat(11,:)*180/pi,'linewidth',2)
hold on
plot(time_arr, (bias_mag(2)*ones(size(time_arr)))*180/pi,'r--','linewidth',2)
ylabel('\theta Bias (deg)')

subplot(3,1,3)
plot(time_arr, x_hat(12,:)*180/pi,'linewidth',2)
ylabel('\phi Bias (deg)')
hold on
plot(time_arr, (bias_mag(3)*ones(size(time_arr)))*180/pi,'r--','linewidth',2)
xlabel('t')

print(strcat(unique_identifier_string,'/EulerBiases'),'-dpng')

% Accelerometer Biases
figure
subplot(3,1,1)
plot(time_arr, x_hat(13,:),'linewidth',2)
hold on
plot(time_arr, (bias_accel(1)*ones(size(time_arr))),'r--','linewidth',2)
ylabel('Accel_x Bias (m/s/s)')

title('Acceleration (Accelerometer) Biases','fontsize',12)
Lgnd = legend('Estimated','True','location','east');
Lgnd.Position(1) = .8;
Lgnd.Position(2) = 0.9;

subplot(3,1,2)
plot(time_arr, x_hat(14,:),'linewidth',2)
hold on
plot(time_arr, (bias_accel(2)*ones(size(time_arr))),'r--','linewidth',2)
ylabel('Accel_y Bias (m/s/s)')

subplot(3,1,3)
plot(time_arr, x_hat(15,:),'linewidth',2)
ylabel('Accel_z Bias (m/s/s)')
hold on
plot(time_arr, (bias_accel(3)*ones(size(time_arr))),'r--','linewidth',2)
xlabel('t')

print(strcat(unique_identifier_string,'/AccelBiases'),'-dpng')

% Rate Gyro Biases
figure
subplot(3,1,1)
plot(time_arr, x_hat(16,:)*180/pi,'linewidth',2)
ylabel('p Bias (deg/s)')
hold on
plot(time_arr, (bias_gyro(1)*ones(size(time_arr)))*180/pi,'r--','linewidth',2)

title('Angular Velocity (Rate Gyro) Biases','fontsize',12)
Lgnd = legend('Estimated','True','location','east');
Lgnd.Position(1) = .8;
Lgnd.Position(2) = 0.9;

subplot(3,1,2)
plot(time_arr, x_hat(17,:)*180/pi,'linewidth',2)
ylabel('q Bias (deg/s)')
hold on
plot(time_arr, (bias_gyro(2)*ones(size(time_arr)))*180/pi,'r--','linewidth',2)

subplot(3,1,3)
plot(time_arr, x_hat(18,:)*180/pi,'linewidth',2)
ylabel('r Bias (deg/s)')
hold on
plot(time_arr, (bias_gyro(3)*ones(size(time_arr)))*180/pi,'r--','linewidth',2)
xlabel('t')

print(strcat(unique_identifier_string,'/GyroBias'),'-dpng')

% Airspeed Bias
figure
plot(time_arr, x_hat(19,:),'linewidth',2)
ylabel('Airspeed DeltaP Bias (Pa)')
hold on
plot(time_arr, (bias_airspeed(1)*ones(size(time_arr))),'r--','linewidth',2)
xlabel('t')

title('Airspeed (Pressure Sensor) Bias','fontsize',12)
Lgnd = legend('Estimated','True','location','east');
Lgnd.Position(1) = .8;
Lgnd.Position(2) = 0.6;

print(strcat(unique_identifier_string,'/AirspeedPressureBias'),'-dpng')

















%% Helpful Function(s)
function noisy_signal1 = noisy_signal(true_signal, bias, noise_covar)
    % generate noise array
    [rows,cols] = size(true_signal);
    
    for i = 1:rows
        noise = noise_covar(i) .* randn(1,cols);
        noisy_signal1(i,:) = true_signal(i,:) + bias(i) + noise;
    end
end
