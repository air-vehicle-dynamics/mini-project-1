function fixed_wing_tracking_circle()
close all; clc;

t_span	= 30;
n_pts	= t_span*100 + 1;
time_pts= linspace(0, t_span, n_pts);	% s

%----- Initial conditions
posn0		= [300; 100];

%----- Reference
V_star	      = 30;	% m/s
heading_infty = pi/4;   % Reference heading at infinity
K_p           = 5;   % Heading reference gain

% Define Circle
C_t = [100; 100]; % center of circle 
R   = 150;        % circle turn radius (min, from Dubin's calculations)
CW  = 1;          % 1 for CW, -1 for CCW

x		= [posn0];
x_true	= zeros(2, n_pts);
u_true	= zeros(1, n_pts);

x_true(:, 1)	= x;
for m1 = 2:n_pts
	dt = time_pts(m1) - time_pts(m1 - 1);
    t  = time_pts(m1);
    
    % Position on the circle at time t (will have to schedule this with
    % start and end thetas)
    P_t = C_t + CW * R*[cos((V_star/R)*t); sin((V_star/R)*t)];
    
    d = norm(x - C_t);
    X_p = atan2(P_t(2) - C_t(2), P_t(1) - C_t(1));
    
    % Commanded heading
	u_t = X_p + CW * ((pi/2) + atan(K_p * ((d - R)/R)));	
    u_t_dt	= u_t;
	u_t_half= 0.5*(u_t + u_t_dt);
	
	k1	= dt*aircraft_kinematics(x,				u_t);
	k2	= dt*aircraft_kinematics((x + 0.5*k1),	u_t_half);
	k3	= dt*aircraft_kinematics((x + 0.5*k2),	u_t_half);
	k4	= dt*aircraft_kinematics((x + k3),		u_t_dt);
	
	x	= x + (1/6)*k1 + (1/3)*k2 + (1/3)*k3 + (1/6)*k4;
	x_true(:, m1)	= x;
	u_true(:, m1)	= u_t;
end


n_plot = 20;
theta_start = 0;
theta_stop  = 2*pi;
theta = theta_start:0.001:theta_stop;

pos_ref = C_t + R*[cos(theta); sin(theta)];



% [x_plot, y_plot] = meshgrid( linspace(slp0(1), slp0(1) + V_star*cos(heading_ref)*t_span, n_plot), ...
% 	linspace(slp0(2), slp0(2) + V_star*sin(heading_ref)*t_span, n_plot ) );
% vec_field = zeros(n_plot);
% for m1= 1:n_plot
% 	for m2 = 1:n_plot
% 		xm1 = x_plot(m1,m2);
% 		ym2	= y_plot(m1,m2);
% 		
% 		e_posn_p	= Rtp*( [xm1; ym2] - slp0 );
% 		heading_cmd	= heading_ref - heading_infty*(2/pi)*atan(K_p*e_posn_p(2));
% 		vec_field(m1, m2) = heading_cmd;
% 	end
% end
% posn_ref= slp0 + V_star*[cos(heading_ref)*time_pts; sin(heading_ref)*time_pts];

figure('Name', 'Position');
% quiver(x_plot, y_plot, cos(vec_field), sin(vec_field), 0.5); 
axis equal; grid on; hold on;
plot(pos_ref(1, :), pos_ref(2, :), 'LineWidth', 2', 'Marker', '.', 'MarkerSize', 5,'DisplayName','Reference'); 
plot( x_true(1, :),  x_true(2, :), 'LineWidth', 2', 'Marker', '.', 'MarkerSize', 5,'DisplayName','True'); 
hold off; legend;

	function x_dot_ = aircraft_kinematics(x_, u_)
		
% 		u_ = max( min(u_, (V_star / R_min)), (V_star / R_min));
		
		
		heading_chi	= u_; %x_(3);
		x_dot_		= [V_star*cos(heading_chi); V_star*sin(heading_chi)]; %; u_];
    end






end