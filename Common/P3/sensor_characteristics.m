% define sensors and typical noise characteristics 

% rate gyros
rate_gyro.read_rate = 80; % [Hz]
rate_gyro.noise_stdev = 0.13*pi/180; % [rad/s]

% accelerometers
accel.read_rate = 100; % [Hz] has range from 0.5 to 550, 100 is typical
accel.noise_stdev = 0.0025*9.81; % [G]

% pressure sensors (absolute pressure)
pressure.read_rate = 100; % [Hz] assumed
pressure.noise_stdev = 0.002*1000; % [Pa]

% digital compass / magnetometer
dig_comp.read_rate = 8; % [Hz]
dig_comp.noise_stdev = 0.3*pi/180; % [deg]

% gps
gps.read_rate = 1; % [Hz]
gps.noise_stdev_pos_NE = 0.21; % [m]
gps.noise_stdev_pos_Z = 0.4; % [m]
gps.noise_stdev_vel_NE = 0.02;
gps.noise_stdev_vel_Z = 0.04; % same as above for now
