clc;
clear all;
close all;

global J m W g GAMMA GAMMA1 GAMMA2 GAMMA3 GAMMA4 GAMMA5 GAMMA6 GAMMA7 GAMMA8
global S b c S_prop rho K_motor K_TP K_omega e_oswald Aspect
global C_L_0 C_D_0 C_m_0 C_L_alpha C_D_alpha C_m_alpha C_L_q C_D_q C_m_q C_L_deltae C_D_deltae C_m_deltae C_prop 
global M_Stall alpha_0 epsilon C_D_p
global C_Y_0 C_l_0 C_n_0 C_Y_beta C_l_beta C_n_beta C_Y_p C_l_p C_n_p C_Y_r C_l_r C_n_r 
global C_Y_deltaa C_l_deltaa C_n_deltaa C_Y_deltar C_l_deltar C_n_deltar

unique_identifier_string = 'trimcond3';
mkdir(unique_identifier_string)

W = [0;0;0];
g = 9.81;
rho = 1.225;

% Define parameters for UAV

Define_Aerosonde_Parameters;


% Generate time arrays

t_out = 0:1:100;
t_generate_control = 0:(max(t_out)/10):max(t_out);


% Generate Random Control Inputs Near Trim


rand1_generate_control = (1 + 0*rand(length(t_generate_control),1));
rand1_generate_control = interp1(t_generate_control, rand1_generate_control, t_out);

rand2_generate_control = (1 + 0*rand(length(t_generate_control),1));
rand2_generate_control = interp1(t_generate_control, rand2_generate_control, t_out);

rand3_generate_control = (1 + 0*rand(length(t_generate_control),1));
rand3_generate_control = interp1(t_generate_control, rand3_generate_control, t_out);

% rand1_generate_control = (0.8 + .4*rand(length(t_generate_control),1));
% rand1_generate_control = interp1(t_generate_control, rand1_generate_control, t_out);
% 
% rand2_generate_control = (-0.2 + .4*rand(length(t_generate_control),1));
% rand2_generate_control = interp1(t_generate_control, rand2_generate_control, t_out);
% 
% rand3_generate_control = (0.8 + .4*rand(length(t_generate_control),1));
% rand3_generate_control = interp1(t_generate_control, rand3_generate_control, t_out);


% Constant if desired
all_deltar = 1.008344*pi*ones(length(t_out),1)/180;
all_deltae = rand1_generate_control'.*(-5.26958).*pi.*ones(length(t_out),1)/180;
all_deltaa = rand2_generate_control'.*(-0.84311)*pi.*ones(length(t_out),1)/180;
all_deltap = rand3_generate_control'.*0.4101.*ones(length(t_out),1);


% Initial States
x0 = zeros(22,1);
x0(1,1) = 0;     % Px
x0(2,1) = 0;     % Py
x0(3,1) = -100;  % Pz
x0(7,1) = 29.94834;    % u
x0(8,1) = 0.511315;     % v
x0(9,1) = 1.683843;     % w
x0(13,1) = 0*pi/180;  % Psi
x0(14,1) = 6.218063*pi/180;  % Theta
x0(15,1) = 33.80849*pi/180;  % Phi
x0(20,1) = -0.02166;  % p
x0(21,1) = 0.110629;  % q
x0(22,1) = 0.165203;  % r

psi0 = x0(13,1);
theta0 = x0(14,1);
phi0 = x0(15,1);

% Body to tangent rotation
R_b_t_0 = zeros(3,3);
R_b_t_0(1,1) = (cos(theta0)*cos(psi0));
R_b_t_0(1,2) = (sin(phi0)*sin(theta0)*cos(psi0) - cos(phi0)*sin(psi0));
R_b_t_0(1,3) = (cos(phi0)*sin(theta0)*cos(psi0) + sin(phi0)*sin(psi0));
R_b_t_0(2,1) = (cos(theta0)*sin(psi0));
R_b_t_0(2,2) = (sin(phi0)*sin(theta0)*sin(psi0) + cos(phi0)*cos(psi0));
R_b_t_0(2,3) = (cos(phi0)*sin(theta0)*sin(psi0) - sin(phi0)*cos(psi0));
R_b_t_0(3,1) = -(sin(theta0));
R_b_t_0(3,2) = (sin(phi0)*cos(theta0));
R_b_t_0(3,3) = (cos(phi0)*cos(theta0));

% Other Initial States
x0(4:6,1) = R_b_t_0*x0(7:9,1) + W;
x0(10,1) = sqrt(x0(7,1)^2 + x0(8,1)^2 + x0(9,1)^2);
x0(11,1) = atan2(x0(9,1),x0(7,1));
x0(12,1) = asin(x0(8,1)/x0(10,1));
x0(16:19,1) = [0;0;0;0];%eul2quat(x0(13:15,1)');

% Define output matrix
x_store = zeros(length(t_out),length(x0));
x_store(1,:) = x0';
x_prev = x0;

for the_step = 2:length(t_out)
   
    if mod(the_step, 10) == 0    
        fprintf('Step %i of %i\n\n',the_step, length(t_out))
    end
    
    t_prev = t_out(the_step - 1);
    t_current = t_out(the_step);
    
    the_deltae = all_deltae(the_step - 1);
    the_deltar = all_deltar(the_step - 1);
    the_deltaa = all_deltaa(the_step - 1);
    the_deltap = all_deltap(the_step - 1);
    
    the_control = [the_deltae; the_deltap; the_deltaa; the_deltar];
    
    the_output = ode45((@(t,x) fullrigidbodymodel(0,x,the_control)),[t_prev t_current], x_prev);
    
    x_store(the_step,:) = the_output.y(:,end)';
    x_prev = the_output.y(:,end);
    
end



%% Plotting

close all;

% Plot Positions

subplot(3,1,1)
plot(t_out, x_store(:,1),'linewidth',2)
ylabel('P_x^t')
ylim([-1.5*max(abs(x_store(:,1))) - 0.01, 1.5*max(abs(x_store(:,1))) + 0.01])
subplot(3,1,2)
plot(t_out, x_store(:,2),'linewidth',2)
ylim([-1.5*max(abs(x_store(:,2))) - 0.01, 1.5*max(abs(x_store(:,2))) + 0.01])
ylabel('P_y^t')
subplot(3,1,3)
plot(t_out, x_store(:,3),'linewidth',2)
ylim([-1.5*max(abs(x_store(:,3))) - 0.01, 1.5*max(abs(x_store(:,3))) + 0.01])
ylabel('P_z^t')
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Positions'),'-dpng')

% Plot Ground Velocities

figure;

subplot(3,1,1)
plot(t_out, x_store(:,4),'linewidth',2)
ylabel('V_{Gx}^t')
ylim([-1.5*max(abs(x_store(:,4))) - 0.01, 1.5*max(abs(x_store(:,4))) + 0.01])
subplot(3,1,2)
plot(t_out, x_store(:,5),'linewidth',2)
ylabel('V_{Gy}^t')
ylim([-1.5*max(abs(x_store(:,5))) - 0.01, 1.5*max(abs(x_store(:,5))) + 0.01])
subplot(3,1,3)
plot(t_out, x_store(:,6),'linewidth',2)
ylim([-1.5*max(abs(x_store(:,6))) - 0.01, 1.5*max(abs(x_store(:,6))) + 0.01])
ylabel('V_{Gz}^t')
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Ground_Velocities'),'-dpng')

% Plot Air Velocities

figure;

subplot(3,1,1)
plot(t_out, x_store(:,7),'linewidth',2)
ylabel('u')
ylim([-1.5*max(abs(x_store(:,7))) - 0.01, 1.5*max(abs(x_store(:,7))) + 0.01])
subplot(3,1,2)
plot(t_out, x_store(:,8),'linewidth',2)
ylabel('v')
ylim([-1.5*max(abs(x_store(:,8))) - 0.01, 1.5*max(abs(x_store(:,8))) + 0.01])
subplot(3,1,3)
plot(t_out, x_store(:,9),'linewidth',2)
ylim([-1.5*max(abs(x_store(:,9))) - 0.01, 1.5*max(abs(x_store(:,9))) + 0.01])
ylabel('w')
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Air_Velocities'),'-dpng')

% Plot Airspeed, alpha, beta

figure;

subplot(3,1,1)
plot(t_out, x_store(:,10),'linewidth',2)
ylim([-1.5*max(abs(x_store(:,10))) - 0.01, 1.5*max(abs(x_store(:,10))) + 0.01])
ylabel('V_a')
subplot(3,1,2)
plot(t_out, 180*x_store(:,11)/pi,'linewidth',2)
ylabel('\alpha (deg)')
ylim([-10 10])
subplot(3,1,3)
plot(t_out, 180*x_store(:,12)/pi,'linewidth',2)
ylabel('\beta (deg)')
ylim([-10 10])
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Airspeed_and_Air_Angles'),'-dpng')

% Plot Euler Angles

figure;

subplot(3,1,1)
plot(t_out, 180*x_store(:,13)/pi,'linewidth',2)
ylabel('\psi (deg)')
%ylim([-90 90])
subplot(3,1,2)
plot(t_out, 180*x_store(:,14)/pi,'linewidth',2)
ylabel('\theta (deg)')
ylim([-90 90])
subplot(3,1,3)
plot(t_out, 180*x_store(:,15)/pi,'linewidth',2)
ylabel('\phi (deg)')
ylim([-90 90])
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Euler_Angles'),'-dpng')

% Plot Quaternions and Norm

figure;

subplot(5,1,1)
plot(t_out, x_store(:,16),'linewidth',2)
ylabel('e_0')
ylim([-2 2])
subplot(5,1,2)
plot(t_out, x_store(:,17),'linewidth',2)
ylabel('e_1')
ylim([-2 2])
subplot(5,1,3)
plot(t_out, x_store(:,18),'linewidth',2)
ylabel('e_2')
ylim([-2 2])
subplot(5,1,4)
plot(t_out, x_store(:,19),'linewidth',2)
ylabel('e_3')
ylim([-2 2])
subplot(5,1,5)
plot(t_out, vecnorm(x_store(:,16:19)')','linewidth',2)
ylim([0 2])
ylabel('||e||')
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Quaternions'),'-dpng')

% Plot Angular Rates

figure;

subplot(3,1,1)
plot(t_out, 180*x_store(:,20)/pi,'linewidth',2)
ylabel('p (deg/sec)')
ylim([-1.5*max(abs(x_store(:,20)*180/pi)) - 0.01, 1.5*max(abs(x_store(:,20)*180/pi)) + 0.01])
subplot(3,1,2)
plot(t_out, 180*x_store(:,21)/pi,'linewidth',2)
ylabel('q (deg/sec)')
ylim([-1.5*max(abs(x_store(:,21)*180/pi)) - 0.01, 1.5*max(abs(x_store(:,21)*180/pi)) + 0.01])
subplot(3,1,3)
plot(t_out, 180*x_store(:,22)/pi,'linewidth',2)
ylim([-1.5*max(abs(x_store(:,22)*180/pi)) - 0.01, 1.5*max(abs(x_store(:,22)*180/pi)) + 0.01])
ylabel('r (deg/sec)')
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Angular_Rates'),'-dpng')

% Plot Control Inputs

figure;

subplot(4,1,1)
plot(t_out, all_deltap,'linewidth',2)
ylabel('\delta p')
ylim([0 1])
subplot(4,1,2)
plot(t_out, 180*all_deltae/pi,'linewidth',2)
ylabel('\delta e')
ylim([-20 20])
subplot(4,1,3)
plot(t_out, 180*all_deltar/pi,'linewidth',2)
ylabel('\delta r')
ylim([-20 20])
subplot(4,1,4)
plot(t_out, 180*all_deltaa/pi,'linewidth',2)
ylabel('\delta a')
ylim([-20 20])
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Controls'),'-dpng')


% Plot 3D

figure;
plot3(x_store(:,1), x_store(:,2), -x_store(:,3),'linewidth',2)
legend(["Trajectory"]);
hold on
plot3(x_store(1,1), x_store(1,2), -x_store(1,3),'rx', 'DisplayName','Start','linewidth',5)
plot3(x_store(end,1), x_store(end,2), -x_store(end,3),'ko','DisplayName','End','linewidth',5)
xlabel('P_x (North)')
ylabel('P_y (East)')
zlabel('-P_z (Altitide)')
axis equal
print(strcat(unique_identifier_string,'/',unique_identifier_string,'3D_Trajectory'),'-dpng')

%% Output fake sensor readings

sensor_rate = 50;  % Hz
t_out_sensor = min(t_out):(1/sensor_rate):max(t_out);
to_sensor_output = zeros(size(x_store,1),size(x_store,2)+5);
to_sensor_output(:,2:size(x_store,2)+1) = x_store;
to_sensor_output(:,1) = t_out';
p_out = x_store(:,20);
q_out = x_store(:,21);
r_out = x_store(:,22);
alpha_out = x_store(:,11);
Va_out = x_store(:,10);
beta_out = x_store(:,12);

% Lift and Drag Coefficients in x and z form
C_L = C_L_0 + C_L_alpha*alpha_out;
C_D = C_D_p + C_L.^2/(pi*Aspect*e_oswald);
C_X = -C_D.*cos(alpha_out) + C_L.*sin(alpha_out);
C_X_q = -C_D_q*cos(alpha_out) + C_L_q*sin(alpha_out);
C_X_deltae = -C_D_deltae*cos(alpha_out) + C_L_deltae*sin(alpha_out);
C_Z = -C_D.*sin(alpha_out)-C_L.*cos(alpha_out);
C_Z_q = -C_D_q*sin(alpha_out)-C_L_q*cos(alpha_out);
C_Z_deltae = -C_D_deltae*sin(alpha_out) - C_L_deltae*cos(alpha_out);


Xoverm = (rho*Va_out.^2*S./(2*m)).*(C_X + C_X_q.*c.*q_out./(2*Va_out)...
    + C_X_deltae.*all_deltae) + (rho*S_prop*C_prop./(2*m)).*((K_motor*all_deltap).^2 - Va_out.^2);
Yoverm = (rho*Va_out.^2*S/(2*m)).*(C_Y_0 + C_Y_beta*beta_out...
    + C_Y_p*b*p_out./(2*Va_out) + C_Y_r*b*r_out./(2*Va_out) + C_Y_deltaa*all_deltaa + C_Y_deltar*all_deltar);
Zoverm = (rho*Va_out.^2*S/(2*m)).*(C_Z...
    + C_Z_q.*c.*q_out./(2*Va_out) + C_Z_deltae.*all_deltae);

to_sensor_output(:,24:26) = [Xoverm Yoverm Zoverm];
to_sensor_output(:,27) = Va_out.^2*rho/2;
to_sensor = interp1(t_out, to_sensor_output, t_out_sensor);

save(strcat(unique_identifier_string,'/',unique_identifier_string,"_Simulation_Data.mat"))

% to_sensor has:  t, 22 states, accel_x, accel_y, accel_z, deltaP
% 22 states are px, py, pz, V_gx, V_gy, V_gz, u, v, w, Va, , , psi, theta,
% phi