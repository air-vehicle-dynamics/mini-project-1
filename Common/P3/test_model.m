fprintf('Model class test suite: \n')

syms a b c
symbolic_vars = [a b c];

% define working variables
pdot = a + b + c;
qdot = a * b * c;
rdot = 3 + 2*a + c;

% check Model definition 
m = Model;
m.p_dot = pdot;
m.q_dot = qdot;
m.r_dot = rdot;

pdot_diff = m.p_dot - pdot;
qdot_diff = m.q_dot - qdot;
rdot_diff = m.r_dot - rdot;

assert(double(pdot_diff) == 0)
assert(double(qdot_diff) == 0)
assert(double(rdot_diff) == 0)
assert(m.g == 9.81)
assert(m.m == 10)


% check Model functions
    % create_state
    model_state = create_state(m);
    expected_state = [pdot; qdot; rdot];
    assert(isequal(model_state, expected_state))

    % linearize equations
    J = linearize(m,symbolic_vars);
    jacobian_expected = [1 1 1;
                         b*c a*c a*b;
                         2 0 1];
    assert(isequal(J,jacobian_expected))