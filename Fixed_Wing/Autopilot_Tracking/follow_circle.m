function [x_true] = follow_circle(path,x_start,C,R,CW,V_star,npts)

% tstart = 0;
% tend   = 30;
% time_pts = linspace(tstart,tend,npts);

% stop_cond = 1;
% stop_tol = [10; 10]; % stopping tolerance [m]
% counter = 0;
tol = [1;1];
i_tol = 1e6;


npts = length(path);


dt = 0.01;

K_p           = 1000;   % Heading reference gain

C_t = [C(1); C(2)];
x = x_start;
% x_true	= zeros(2, npts);
u_true	= zeros(1, npts);

x_true(:, 1)	= x;

i = 1;

while i > 0
        
    t = dt*i;   
    
    P_t = C_t + CW * R*[cos((V_star/R)*t); sin((V_star/R)*t)];
%     P_t = path(:,i);
    
    d = norm(x - C_t);
    X_p = atan2(P_t(2) - C_t(2), P_t(1) - C_t(1));
    
    % Commanded heading
    % CIRCLES ARE TURNING THE WRONG WAY
	u_t = X_p + CW * ((pi/2) + atan(K_p * ((d - R)/R)));	
    u_t_dt	= u_t;
	u_t_half= 0.5*(u_t + u_t_dt);
	
	k1	= dt*aircraft_kinematics(x,				u_t);
	k2	= dt*aircraft_kinematics((x + 0.5*k1),	u_t_half);
	k3	= dt*aircraft_kinematics((x + 0.5*k2),	u_t_half);
	k4	= dt*aircraft_kinematics((x + k3),		u_t_dt);
	
	x	= x + (1/6)*k1 + (1/3)*k2 + (1/3)*k3 + (1/6)*k4;
	x_true(:, i)	= x;
	u_true(:, i)	= u_t;
    
    i = i + 1;
    
    % check for stop condition
    closeness = abs(path(:,end) - x);
    
    if closeness(1) <= tol(1)
        fprintf("X within spec, %d\n", i)
        break
    elseif closeness(2) <= tol(2)
        fprintf("Y within spec, %d\n", i)
        break
    elseif i >= i_tol 
        break
    end
    
end



function x_dot_ = aircraft_kinematics(x_, u_)

% 	u_ = max( min(u_, (V_star / R_min)), (V_star / R_min));


    heading_chi	= u_; %x_(3);
    x_dot_		= [V_star*cos(heading_chi); V_star*sin(heading_chi)]; %; u_];
end


end