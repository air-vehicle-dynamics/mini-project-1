clc;
clear all;
close all;

p1 = [0;0;0];
p2 = [1000; 1000; 0];
psi1 = 0*pi/180;
psi2 = 20*pi/180;
Rmin = 500;
the_type = 'LSL';
makeplots = 1;

minlength = DubinsCSC(p1, p2, psi1, psi2, Rmin, the_type, makeplots)