clc;
clear all;
close all;

global J m K_F K_M K_m W g L rho CD0_S
global GAMMA GAMMA1 GAMMA2 GAMMA3 GAMMA4 GAMMA5 GAMMA6 GAMMA7 GAMMA8

unique_identifier_string = "test1";
mkdir(unique_identifier_string);

rho = 1.225;
W = [0;0;0];
g = 9.81;
m = 0.5;
J = [2.32E-3 0 0; 0 2.32E-3 0; 0 0 4E-3];
K_M = 1.5E-9;
K_F = 6.11E-8;
K_m = 20;
L = 0.175;
CD0_S = 0.005;


t_out = 0:1:100;

% Constant if desired
all_omega1 = 4484.266*ones(length(t_out),1);
all_omega2 = 4484.266*ones(length(t_out),1);
all_omega3 = 4484.266*ones(length(t_out),1);
all_omega4 = 4484.266*ones(length(t_out),1);

x0 = zeros(22,1);


% Initial States
x0(1,1) = 0;     % Px
x0(2,1) = 0;     % Py
x0(3,1) = -100;  % Pz
x0(7,1) = 9.980565 + -.5;     % u
x0(8,1) = 0 + .01;     % v
x0(9,1) = -0.62315;     % w
x0(10,1) = 0*pi/180;  % Psi
x0(11,1) = -3.5727*pi/180;  % Theta
x0(12,1) = 0*pi/180;  % Phi
x0(17,1) = 0;  % p
x0(18,1) = 0;  % q
x0(19,1) = 0;  % r

psi0 = x0(10,1);
theta0 = x0(11,1);
phi0 = x0(12,1);

% Body to tangent rotation
R_b_t_0 = zeros(3,3);
R_b_t_0(1,1) = (cos(theta0)*cos(psi0));
R_b_t_0(1,2) = (sin(phi0)*sin(theta0)*cos(psi0) - cos(phi0)*sin(psi0));
R_b_t_0(1,3) = (cos(phi0)*sin(theta0)*cos(psi0) + sin(phi0)*sin(psi0));
R_b_t_0(2,1) = (cos(theta0)*sin(psi0));
R_b_t_0(2,2) = (sin(phi0)*sin(theta0)*sin(psi0) + cos(phi0)*cos(psi0));
R_b_t_0(2,3) = (cos(phi0)*sin(theta0)*sin(psi0) - sin(phi0)*cos(psi0));
R_b_t_0(3,1) = -(sin(theta0));
R_b_t_0(3,2) = (sin(phi0)*cos(theta0));
R_b_t_0(3,3) = (cos(phi0)*cos(theta0));

x0(4:6,1) = R_b_t_0*x0(7:9,1) + W;

x0(13:16,1) = eul2quat(x0(10:12,1)');

x_store = zeros(length(t_out),length(x0));
x_store(1,:) = x0';

x_prev = x0;

for the_step = 2:length(t_out)
   
    if mod(the_step, 10) == 0    
        fprintf('Step %i of %i\n\n',the_step, length(t_out))
    end
    
    t_prev = t_out(the_step - 1);
    t_current = t_out(the_step);
    
    the_omega1 = all_omega1(the_step - 1);
    the_omega2 = all_omega2(the_step - 1);
    the_omega3 = all_omega3(the_step - 1);
    the_omega4 = all_omega4(the_step - 1);
    
    the_control = [the_omega1; the_omega2; the_omega3; the_omega4];
    
    the_output = ode45((@(t,x) fullrigidbodymodel(0,x,the_control)),[t_prev t_current], x_prev);
    
    x_store(the_step,:) = the_output.y(:,end)';
    x_prev = the_output.y(:,end);
end



%% Plotting

close all;

% Plot Positions

subplot(3,1,1)
plot(t_out, x_store(:,1),'linewidth',2)
ylabel('P_x^t')
ylim([-1.5*max(abs(x_store(:,1))) - 0.01, 1.5*max(abs(x_store(:,1))) + 0.01])
subplot(3,1,2)
plot(t_out, x_store(:,2),'linewidth',2)
ylim([-1.5*max(abs(x_store(:,2))) - 0.01, 1.5*max(abs(x_store(:,2))) + 0.01])
ylabel('P_y^t')
subplot(3,1,3)
plot(t_out, x_store(:,3),'linewidth',2)
ylim([-1.5*max(abs(x_store(:,3))) - 0.01, 1.5*max(abs(x_store(:,3))) + 0.01])
ylabel('P_z^t')
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Positions'),'-dpng')

% Plot Ground Velocities

figure;

subplot(3,1,1)
plot(t_out, x_store(:,4),'linewidth',2)
ylabel('V_{Gx}^t')
ylim([-1.5*max(abs(x_store(:,4))) - 0.01, 1.5*max(abs(x_store(:,4))) + 0.01])
subplot(3,1,2)
plot(t_out, x_store(:,5),'linewidth',2)
ylabel('V_{Gy}^t')
ylim([-1.5*max(abs(x_store(:,5))) - 0.01, 1.5*max(abs(x_store(:,5))) + 0.01])
subplot(3,1,3)
plot(t_out, x_store(:,6),'linewidth',2)
ylim([-1.5*max(abs(x_store(:,6))) - 0.01, 1.5*max(abs(x_store(:,6))) + 0.01])
ylabel('V_{Gz}^t')
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Ground_Velocities'),'-dpng')

% Plot Air Velocities

figure;

subplot(3,1,1)
plot(t_out, x_store(:,7),'linewidth',2)
ylabel('u')
ylim([-1.5*max(abs(x_store(:,7))) - 0.01, 1.5*max(abs(x_store(:,7))) + 0.01])
subplot(3,1,2)
plot(t_out, x_store(:,8),'linewidth',2)
ylabel('v')
ylim([-1.5*max(abs(x_store(:,8))) - 0.01, 1.5*max(abs(x_store(:,8))) + 0.01])
subplot(3,1,3)
plot(t_out, x_store(:,9),'linewidth',2)
ylim([-1.5*max(abs(x_store(:,9))) - 0.01, 1.5*max(abs(x_store(:,9))) + 0.01])
ylabel('w')
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Air_Velocities'),'-dpng')

% Plot Euler Angles

figure;

subplot(3,1,1)
plot(t_out, 180*x_store(:,10)/pi,'linewidth',2)
ylabel('\psi (deg)')
%ylim([-90 90])
subplot(3,1,2)
plot(t_out, 180*x_store(:,11)/pi,'linewidth',2)
ylabel('\theta (deg)')
ylim([-90 90])
subplot(3,1,3)
plot(t_out, 180*x_store(:,12)/pi,'linewidth',2)
ylabel('\phi (deg)')
ylim([-90 90])
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Euler_Angles'),'-dpng')

% Plot Quaternions and Norm

figure;

subplot(5,1,1)
plot(t_out, x_store(:,13),'linewidth',2)
ylabel('e_0')
ylim([-2 2])
subplot(5,1,2)
plot(t_out, x_store(:,14),'linewidth',2)
ylabel('e_1')
ylim([-2 2])
subplot(5,1,3)
plot(t_out, x_store(:,15),'linewidth',2)
ylabel('e_2')
ylim([-2 2])
subplot(5,1,4)
plot(t_out, x_store(:,16),'linewidth',2)
ylabel('e_3')
ylim([-2 2])
subplot(5,1,5)
plot(t_out, vecnorm(x_store(:,13:16)')','linewidth',2)
ylim([0 2])
ylabel('||e||')
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Quaternions'),'-dpng')

% Plot Angular Rates

figure;

subplot(3,1,1)
plot(t_out, 180*x_store(:,17)/pi,'linewidth',2)
ylabel('p (deg/sec)')
ylim([-1.5*max(abs(x_store(:,17)*180/pi)) - 0.01, 1.5*max(abs(x_store(:,17)*180/pi)) + 0.01])
subplot(3,1,2)
plot(t_out, 180*x_store(:,18)/pi,'linewidth',2)
ylabel('q (deg/sec)')
ylim([-1.5*max(abs(x_store(:,18)*180/pi)) - 0.01, 1.5*max(abs(x_store(:,18)*180/pi)) + 0.01])
subplot(3,1,3)
plot(t_out, 180*x_store(:,19)/pi,'linewidth',2)
ylim([-1.5*max(abs(x_store(:,19)*180/pi)) - 0.01, 1.5*max(abs(x_store(:,19)*180/pi)) + 0.01])
ylabel('r (deg/sec)')
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Angular_Rates'),'-dpng')


% Plot Control Inputs

figure;

subplot(4,1,1)
plot(t_out, all_omega1,'linewidth',2)
ylabel('\omega_1')
ylim([0 9000])
subplot(4,1,2)
plot(t_out, all_omega2,'linewidth',2)
ylabel('\omega_2')
ylim([0 9000])
subplot(4,1,3)
plot(t_out, all_omega3,'linewidth',2)
ylabel('\omega_3')
ylim([0 9000])
subplot(4,1,4)
plot(t_out, all_omega4,'linewidth',2)
ylabel('\omega_4')
ylim([0 9000])
xlabel('t')
print(strcat(unique_identifier_string,'/',unique_identifier_string,'Controls'),'-dpng')



% Plot 3D

figure;
plot3(x_store(:,1), x_store(:,2), -x_store(:,3),'linewidth',2)
legend(["Trajectory"]);
hold on
plot3(x_store(1,1), x_store(1,2), -x_store(1,3),'rx', 'DisplayName','Start','linewidth',5)
plot3(x_store(end,1), x_store(end,2), -x_store(end,3),'ko','DisplayName','End','linewidth',5)
xlabel('P_x (North)')
ylabel('P_y (East)')
zlabel('-P_z (Altitide)')
axis equal
print(strcat(unique_identifier_string,'/',unique_identifier_string,'3D_Trajectory'),'-dpng')

%% Output fake sensor readings

sensor_rate = 400;  % Hz
t_out_sensor = min(t_out):(1/sensor_rate):max(t_out);
to_sensor_output = zeros(size(x_store,1),size(x_store,2)+5);
to_sensor_output(:,2:size(x_store,2)+1) = x_store;
to_sensor_output(:,1) = t_out';
u_out = x_store(:,7);
v_out = x_store(:,8);
w_out = x_store(:,9);
p_out = x_store(:,17);
q_out = x_store(:,18);
r_out = x_store(:,19);

Va_out = sqrt(u_out.^2 + v_out.^2 + w_out.^2);
alpha_out = atan2(w_out,u_out);
beta_out = asin(v_out./Va_out);

the_drag = zeros(length(alpha_out),3);

for ii = 1:length(beta_out)
    the_beta = beta_out(ii);
    if Va_out == 0
        the_beta = 0;
    end
    the_alpha = alpha_out(ii);
    the_Va = Va_out(ii);
    
    R_b_w_i = [cos(the_beta)*cos(the_alpha) sin(the_beta) cos(the_beta)*sin(the_alpha);
        -sin(the_beta)*cos(the_alpha) cos(the_beta) -sin(the_beta)*sin(the_alpha);
        -sin(the_alpha) 0 cos(the_alpha)];
    the_drag(ii,:) = (R_b_w_i'*[-CD0_S*0.5*rho*the_Va^2; 0; 0])';
end

Xoverm = 0*all_omega1 + (1/m)*the_drag(:,1);
Yoverm = 0*all_omega1 + (1/m)*the_drag(:,2);
Zoverm = -(K_F/m)*(all_omega1.^2 + all_omega2.^2 + all_omega3.^2 + all_omega4.^2) + (1/m)*the_drag(:,3);

to_sensor_output(:,21:23) = [Xoverm Yoverm Zoverm];
Va_out = sqrt(u_out.^2 + v_out.^2 + w_out.^2);
to_sensor_output(:,24) = Va_out.^2*rho/2;
to_sensor = interp1(t_out, to_sensor_output, t_out_sensor);

save(strcat(unique_identifier_string,'/',unique_identifier_string,"_Simulation_Data.mat"))

% to_sensor has:  t, 19 states, accel_x, accel_y, accel_z, deltaP
